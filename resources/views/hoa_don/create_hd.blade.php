@extends('layout.master')

@section('title', 'Hóa đơn')

@section('navigation')
<a href="{{ route('hoa_don.index') }}">Hóa đơn</a> → Tạo hóa đơn
@endsection

@section('search')
@endsection

@section('content')
<div class="grid_1">
	<div>
		<form action="{{ route('hoa_don.store') }}" method="POST">
			{{ csrf_field() }}
			Tạo hóa đơn mới:
			<br>
			Khách hàng
			<br>
			<select name="ma_kh">
				<option disabled selected>Khách hàng</option>
				@foreach ($array_khach_hang as $each)
					<option value="{{ $each->ma_kh }}">{{ $each->ten_kh }}</option>
				@endforeach
			</select>
			@if ($errors->has('ma_kh'))
				<span class="validate_msg">{{ $errors->first('ma_kh') }}</span>
			@endif
			<br>
			Ghi chú
			<br>
			<textarea name="ghi_chu"></textarea>
			<br>
			<button class="form_button_success">Thêm</button>
			*Lưu ý: Thời gian tạo hóa đơn sẽ lấy thời gian hiện tại
		</form>
	</div>
</div>
@endsection