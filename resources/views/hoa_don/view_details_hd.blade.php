@extends('layout.master')

@section('title', 'Hóa đơn chi tiết')

@section('navigation')
	<a href="{{ route('hoa_don.index') }}">Hóa đơn</a> → Hóa đơn chi tiết
@endsection

@section('search')
@endsection

@push('css')
<style type="text/css">
	.tabs_button button{
		width: 49%;
		font-size: 16px;
		text-transform: uppercase;
	}
</style>
@endpush

@section('content')
<div class="grid_2">
	<div style="grid-area: 1 / 1 / 2 / 3" class="grid_content">
		<table width="70%" align="center">
			@foreach ($array_hoa_don as $each)
			<tr>
				<td>Hóa đơn: {{ $each->ten_kh }} #{{ $each->ma_hoa_don }}</td>
				<td>Tạo ngày: {{ $each->getThoiGianLapHoaDon() }}</td>
				<td>Trạng thái: {{ $each->ten_trang_thai_hoa_don }}</td>
				<td>
					Thành tiền: 
					@php
						$tong_tien_phong = 0
					@endphp
					@if (count($each->array_hoa_don_dich_vu))
						@if (count($each->array_hoa_don_phong))
							@foreach ($each->array_hoa_don_dich_vu as $hoa_don_dich_vu)
								@foreach ($each->array_hoa_don_phong as $hoa_don_phong)
									@php
										$tong_tien_phong += $hoa_don_phong->tong_tien_phong;
									@endphp
								@endforeach
							@endforeach
							{{ number_format($hoa_don_dich_vu->tong_tien_dich_vu + $tong_tien_phong) }}
						@else
							@foreach ($each->array_hoa_don_dich_vu as $hoa_don_dich_vu)
								{{ number_format($hoa_don_dich_vu->tong_tien_dich_vu) }}
							@endforeach
						@endif
					@else
						@if (count($each->array_hoa_don_phong))
							@foreach ($each->array_hoa_don_phong as $hoa_don_phong)
								@php
									$tong_tien_phong += $hoa_don_phong->tong_tien_phong;
								@endphp
							@endforeach
							{{ number_format($hoa_don_phong->tong_tien_phong) }}
						@else
							0
						@endif
					@endif
					đ
				</td>
			</tr>
			@endforeach
		</table>
	</div>
	<div class="grid_content tabs_button" style="grid-area: 2/1/3/3;">
		<button class="form_button_success" id="button_tong_quat">Tổng quát</button>
		<button class="form_button_alternative" id="button_chi_tiet">Chi tiết</button>
	</div>
	<div class="grid_content hoa_don_chi_tiet">
		Phòng:
		@foreach ($array_hoa_don as $each)
			@if (count($each->array_hoa_don_phong))
				@foreach ($each->array_hoa_don_phong as $hoa_don_phong)
				@endforeach
				{{ number_format($tong_tien_phong) }}
			@else
				0
			@endif
			đ
		@endforeach
		<table class="form_table">
			<tr>
				<th>Phòng</th>
				<th>Giá phòng (ngày)</th>
				<th>Số khách</th>
				<th>Thời gian đến</th>
				<th>Thời gian đi</th>
			</tr>
			@foreach ($array_hoa_don_phong as $each)
				<tr>
					<td>
						<p>
							{{ $each->ten_phong }}
						</p>
					</td>
					<td>
						<p>
							{{ number_format($each->gia_dat_phong) }} đồng
						</p>
					</td>
					<td>
						<p>
							{{ $each->so_luong_khach }}
						</p>
					</td>
					<td>
						{{ $each->getThoiGianDen() }}
						<br>
						@if ($each->thoi_gian_den > date('Y-m-d H:i:s'))
							<form action="{{ route('dat_phong.update', ['ma_hoa_don' => $each->ma_hoa_don]) }}" method="POST">
								{{ method_field('PUT') }}
								{{ csrf_field() }}
								<input type="hidden" name="ma_phong" value="{{ $each->ma_phong }}">
								<input type="hidden" name="new_thoi_gian_den" value="{{ date('Y-m-d H:i:s') }}">
								<input type="hidden" name="thoi_gian_den" value="{{ $each->thoi_gian_den }}">
								<input type="hidden" name="thoi_gian_di" value="{{ $each->thoi_gian_di }}">
								<button class="form_button_alternative">Lấy phòng sớm</button>
							</form>
						@endif
					</td>
					<td>
						{{ $each->getThoiGianDi() }}
						<br>
						@if ($each->thoi_gian_den < date('Y-m-d H:i:s') && $each->thoi_gian_di > date('Y-m-d H:i:s'))
							<form action="{{ route('dat_phong.update', ['ma_hoa_don' => $each->ma_hoa_don]) }}" method="POST">
								{{ method_field('PUT') }}
								{{ csrf_field() }}
								<input type="hidden" name="ma_phong" value="{{ $each->ma_phong }}">
								<input type="hidden" name="new_thoi_gian_di" value="{{ date('Y-m-d H:i:s') }}">
								<input type="hidden" name="thoi_gian_den" value="{{ $each->thoi_gian_den }}">
								<input type="hidden" name="thoi_gian_di" value="{{ $each->thoi_gian_di }}">
								<button class="form_button_danger">Trả phòng sớm</button>
							</form>
						@elseif ($each->thoi_gian_den > date('Y-m-d H:i:s') && $each->thoi_gian_di > date('Y-m-d H:i:s'))
							<form action="{{ route('dat_phong.destroy', ['ma_hoa_don' => $each->ma_hoa_don]) }}" method="POST">
								{{ method_field('DELETE') }}
								{{ csrf_field() }}
								<input type="hidden" name="ma_phong" value="{{ $each->ma_phong }}">
								<input type="hidden" name="thoi_gian_den" value="{{ $each->thoi_gian_den }}">
								<input type="hidden" name="thoi_gian_di" value="{{ $each->thoi_gian_di }}">
								<button class="form_button_danger">Hủy đặt phòng</button>
							</form>
						@endif
					</td>
				</tr>
			@endforeach
		</table>
	</div>
	<div class="grid_content hoa_don_chi_tiet">
		Dịch vụ:
		@foreach ($array_hoa_don as $each)
			@if (count($each->array_hoa_don_dich_vu))
				@foreach ($each->array_hoa_don_dich_vu as $hoa_don_dich_vu)
					{{ number_format($hoa_don_dich_vu->tong_tien_dich_vu) }}
				@endforeach
			@else
				0
			@endif
			đ
		@endforeach
		<table class="form_table">
			<tr>
				<th>Tên dịch vụ</th>
				<th>Đơn giá</th>
				<th>Số lượng</th>
				<th>Đơn vị</th>
				<th>Tổng giá</th>
				<th>Thời gian đặt</th>
			</tr>
			@foreach ($array_hoa_don_dich_vu as $each)
				<tr>
					<td>
						<p>
							{{ $each->ten_dich_vu }}
						</p>
					</td>
					<td>
						<p>
							{{ number_format($each->gia_sd_dich_vu) }} đồng
						</p>
					</td>
					<td>
						<p>
							{{ $each->so_luong_dich_vu }}
						</p>
					</td>
					<td>
						<p>
							{{ $each->don_vi_dich_vu }}
						</p>
					</td>
					<td>
						<p>
							{{ number_format($each->gia_sd_dich_vu * $each->so_luong_dich_vu) }} đồng
						</p>
					</td>
					<td>
						<p>
							{{ $each->getThoiGianSuDungDichVu() }}
						</p>
					</td>
				</tr>
			@endforeach
		</table>
	</div>
	<div class="invoice_container hoa_don_tong_quat" style="grid-area: 3/1/3/3">
		<div class="invoice">
			<header>
				@foreach ($array_hoa_don as $each)
				<section>
					<h1>{{ $each->ten_kh }}</h1>
					<span>{{ $each->getThoiGianLapHoaDon() }} | {{ $each->ten_trang_thai_hoa_don }}</span>
				</section>

				<section>
					<span>{{ $each->ma_hoa_don }}</span>
				</section>
				@endforeach
			</header>

			<main>
				<section>
					<span>Phòng</span>
				</section>

				<section>
					<span>Tên phòng</span>
					<span>Giá phòng / ngày</span>
					<span>Thời gian thuê</span>
					<span>Thành tiền</span>
				</section>

				<section>
					@foreach ($array_hoa_don_phong_group_by as $each)
					<figure>
						<span><strong>{{ $each->ten_phong }}</strong></span>
						<span>{{ number_format($each->gia_dat_phong) }}</span>
						<span>{{ $each->getThoiGianThuePhong() }}</span>
						<span>{{ number_format(round($each->gia_dat_phong * $each->he_so_thoi_gian_thue / 3, -2)) }}</span>
					</figure>
					@endforeach
				</section>

				<section>
					<span>Dịch vụ</span>
				</section>

				<section>
					<span>Tên dịch vụ</span>
					<span>Đơn giá</span>
					<span>Số lượng</span>
					<span>Thành tiền</span>
				</section>

				<section>
					@foreach ($array_hoa_don_dich_vu_group_by as $each)
					<figure>
						<span><strong>{{ $each->ten_dich_vu }}</strong> ({{ $each->don_vi_dich_vu }})</span>
						<span>{{ number_format($each->gia_sd_dich_vu) }}</span>
						<span>{{ $each->so_luong_dich_vu }}</span>
						<span>{{ number_format($each->tong_tien_dich_vu) }}</span>
					</figure>
					@endforeach
				</section>

				<section>
					<span>Tổng cộng</span>
					<span>
						@foreach ($array_hoa_don as $each)
							@if (count($each->array_hoa_don_dich_vu))
								@if (count($each->array_hoa_don_phong))
									@foreach ($each->array_hoa_don_dich_vu as $hoa_don_dich_vu)
										@foreach ($each->array_hoa_don_phong as $hoa_don_phong)
										@endforeach
										{{ number_format($hoa_don_dich_vu->tong_tien_dich_vu + $tong_tien_phong) }}
									@endforeach
								@else
									@foreach ($each->array_hoa_don_dich_vu as $hoa_don_dich_vu)
										{{ number_format($hoa_don_dich_vu->tong_tien_dich_vu) }}
									@endforeach
								@endif
							@else
								@if (count($each->array_hoa_don_phong))
									@foreach ($each->array_hoa_don_phong as $hoa_don_phong)
									@endforeach
									{{ number_format($tong_tien_phong) }}
								@else
									0
								@endif
							@endif
						@endforeach
					</span>
				</section>
			</main>

			<footer>
				@foreach ($array_hoa_don as $each)
					@if ($each->ma_trang_thai_hoa_don == 1)
						<a href="#0">Hủy hóa đơn</a>
						<a href="#0">Thanh toán</a>
					@endif
				@endforeach
			</footer>
		</div>
	</div>
</div>
@endsection

@push('script')
<script type="text/javascript">
	$(document).ready(function() {
		$('.hoa_don_chi_tiet').hide();
		$('#button_tong_quat').click(function(event) {
			$('.hoa_don_tong_quat').show();
			$('.hoa_don_chi_tiet').hide();
		});
		$('#button_chi_tiet').click(function(event) {
			$('.hoa_don_chi_tiet').show();
			$('.hoa_don_tong_quat').hide();
		});
	});
</script>
@endpush