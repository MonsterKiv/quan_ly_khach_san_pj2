@extends('layout.master')

@section('title', 'Hóa đơn')

@section('navigation')
Hóa đơn
@endsection

@section('search')
@endsection

@section('form_popup')
<div class="form_popup" id="create_hoa_don">
	<div class="form_popup_header">
		<span class="form_popup_close_button">&times;</span>
		<span class="form_popup_title">Thêm hóa đơn mới</span>
	</div>
	<div class="form_popup_content">
		<form action="{{ route('hoa_don.store') }}" method="POST">
		{{ csrf_field() }}
		<table class="form_popup_table">
			<tr>
				<td class="form_popup_td_label">Khách hàng</td>
				<td class="form_popup_td_input">
					<select name="ma_kh" id="ma_kh">
						<option></option>
						@foreach ($array_khach_hang as $each)
							<option value="{{ $each->ma_kh }}">{{ $each->ten_kh }}</option>
						@endforeach
					</select>
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Ghi chú</td>
				<td class="form_popup_td_input">
					<textarea name="ghi_chu" value="{{ old('ghi_chu') }}"></textarea>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<button class="form_button_success">Thêm</button>
				</td>
			</tr>
		</table>
		</form>
	</div>
</div>
<div class="form_popup" id="edit_hoa_don">
	<div class="form_popup_header">
		<span class="form_popup_close_button">&times;</span>
		<span class="form_popup_title">Chỉnh sửa ghi chú</span>
	</div>
	<div class="form_popup_content">
		<form method="POST">
		{{ method_field('PUT') }}
		{{ csrf_field() }}
		<table class="form_popup_table">
			<tr>
				<td class="form_popup_td_label">Khách hàng</td>
				<td class="form_popup_td_input">
					<input type="hidden" name="ma_kh" id="ma_kh_edit">
					<span id="ten_kh_edit"></span>
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Ghi chú</td>
				<td class="form_popup_td_input">
					<textarea name="ghi_chu" id="ghi_chu_edit"></textarea>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<button class="form_button_success">Sửa</button>
				</td>
			</tr>
		</table>
		</form>
	</div>
</div>
@endsection

@section('content')
<div class="grid_1">
	<div class="grid_content">
		<a class="form_button_success open_popup_form" id="button_create_hoa_don">Tạo hóa đơn mới</a>
		<table class="form_table">
			<tr>
				<th>Hóa đơn</th>
				<th>Thời gian lập hóa đơn</th>
				<th>Trạng thái hóa đơn</th>
				<th>Ghi chú</th>
				<th>Thành tiền (VND)</th>
				<th>Xem chi tiết</th>
				<th colspan="2">Thao tác</th>
			</tr>
			@foreach ($array_hoa_don as $each)
				<tr>
					<td>
						<p>
							{{ $each->ten_kh }} - #{{ $each->ma_hoa_don }}
						</p>
					</td>
					<td>
						<p>
							{{ $each->getThoiGianLapHoaDon() }}
						</p>
					</td>
					<td>
						<p>
							{{ $each->ten_trang_thai_hoa_don }}
						</p>
					</td>
					<td>
						<p>
							{{ $each->ghi_chu }}
						</p>
					</td>
					<td>
						<p>
							@php
								$tong_tien_phong = 0
							@endphp
							@if (count($each->array_hoa_don_dich_vu))
								@if (count($each->array_hoa_don_phong))
									@foreach ($each->array_hoa_don_dich_vu as $hoa_don_dich_vu)
										@foreach ($each->array_hoa_don_phong as $hoa_don_phong)
											@php
												$tong_tien_phong += $hoa_don_phong->tong_tien_phong;
											@endphp
										@endforeach
									@endforeach
									{{ number_format($hoa_don_dich_vu->tong_tien_dich_vu + $tong_tien_phong) }}
								@else
									@foreach ($each->array_hoa_don_dich_vu as $hoa_don_dich_vu)
										{{ number_format($hoa_don_dich_vu->tong_tien_dich_vu) }}
									@endforeach
								@endif
							@else
								@if (count($each->array_hoa_don_phong))
									@foreach ($each->array_hoa_don_phong as $hoa_don_phong)
										@php
											$tong_tien_phong += $hoa_don_phong->tong_tien_phong;
										@endphp
									@endforeach
									{{ number_format($hoa_don_phong->tong_tien_phong) }}
								@else
									0
								@endif
							@endif
							đ
						</p>
					</td>
					<td><a href="{{ route('hoa_don.show', ['ma_hoa_don' => $each->ma_hoa_don]) }}" class="form_button_redirect">Xem</a></td>
					<td><a class="form_button_warning open_popup_form button_edit_hoa_don" data-url="{{ route('hoa_don.edit', ['ma_hoa_don' => $each->ma_hoa_don]) }}" data-url_action="{{ route('hoa_don.update', ['ma_hoa_don' => $each->ma_hoa_don]) }}">Sửa ghi chú</a></td>
					<td>
						@if ($each->ma_trang_thai_hoa_don == 2)
							<button disabled class="form_button_alternative button_disabled">Đã thanh toán</button>
						@elseif ($each->ma_trang_thai_hoa_don == 3)
							<button disabled class="form_button_danger button_disabled">Đã hủy</button>
						@else
							@for ($i = 1; $i < 4; $i++)
								@if ($i != $each->ma_trang_thai_hoa_don)
									<form action="{{ route('hoa_don.update', ['ma_hoa_don' => $each->ma_hoa_don]) }}" method="POST">
										{{ method_field('PUT') }}
										{{ csrf_field() }}
										<input type="hidden" name="ma_trang_thai_hoa_don" value="{{ $i }}">
										@if ($i == 2)
											<button class="form_button_alternative">Thanh toán</button>
										@else
											<button class="form_button_danger">Hủy</button>
										@endif
									</form>
								@endif
							@endfor
						@endif
					</td>
				</tr>
			@endforeach
		</table>
	</div>
</div>
@endsection

@push('script')
<script type="text/javascript">
	$(document).ready(function() {
		/* Active Select2 */
    	$('.search_item').select2({
    		width: 'resolve'
    	});
    	$('#ma_kh').select2({
    		placeholder: 'Chọn khách hàng'
    	});

    	/* Open Form Modal */
    	$('#button_create_hoa_don').click(function(event) {
    		$('#create_hoa_don').show();
    	});
    	$('.button_edit_hoa_don').click(function(event) {
    		var url = $(this).data('url');
			var url_action = $(this).data('url_action');
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				$('#edit_hoa_don div form').attr('action', url_action);
				$('#ma_kh_edit').val(response.hoa_don.ma_kh);
				$('#ten_kh_edit').html(response.hoa_don.ten_kh);
				$('#ghi_chu_edit').val(response.hoa_don.ghi_chu);
				$('#edit_hoa_don').show();
			})
			.fail(function() {
				console.log("error");
			});
    	});
    });
</script>
@endpush