@extends('layout.master')

@section('title', 'Hóa đơn')

@section('navigation')
<a href="{{ route('hoa_don.index') }}">Hóa đơn</a> → Chỉnh sửa ghi chú
@endsection

@section('search')
@endsection

@section('content')
<div class="grid_1">
	<div>
		<form action="{{ route('hoa_don.update', ['ma_hoa_don' => $hoa_don->ma_hoa_don]) }}" method="POST">
			{{ method_field('PUT') }}
			{{ csrf_field() }}
			Hóa đơn: <input type="text" disabled value="{{ $hoa_don->ten_kh }} #{{ $hoa_don->ma_hoa_don }}">
			<br>
			Ghi chú
			<br>
			<textarea name="ghi_chu">{{ $hoa_don->ghi_chu }}</textarea>
			<br>
			<button class="form_button_success">Thay đổi</button>
		</form>
	</div>
</div>
@endsection