@extends('layout.master')

@section('title', 'Đặt phòng')

@section('navigation')
<a href="{{ route('phong.index') }}">Phòng</a> → Đặt phòng
@endsection

@section('search')
@endsection

@section('content')
<div class="grid_1">
	<div class="grid_content">
		<form action="{{ route('dat_phong.store') }}" method="POST">
			{{ csrf_field() }}
			Chọn hóa đơn
			<br>
			<select name="ma_hoa_don" id="select_ma_hoa_don">
				<option></option>
				@foreach ($array_hoa_don as $each)
					<option value="{{ $each->ma_hoa_don }}">{{ $each->ten_kh }} - #{{ $each->ma_hoa_don }}</option>
				@endforeach
			</select>
			@if ($errors->has('ma_hoa_don'))
				<span class="validate_msg">{{ $errors->first('ma_hoa_don') }}</span>
			@endif
			<br>
			Số lượng khách
			<br>
			<input type="number" name="so_luong_khach" value="{{ old('so_luong_khach') }}">
			@if ($errors->has('so_luong_khach'))
				<span class="validate_msg">{{ $errors->first('so_luong_khach') }}</span>
			@endif
			<br>
			Thời gian đến
			<br>
			<input type="datetime-local" name="thoi_gian_den" id="thoi_gian_den" value="{{ old('thoi_gian_den') }}">
			@if ($errors->has('thoi_gian_den'))
				<span class="validate_msg">{{ $errors->first('thoi_gian_den') }}</span>
			@endif
			<br>
			Thời gian đi
			<br>
			<input type="datetime-local" name="thoi_gian_di" id="thoi_gian_di" value="{{ old('thoi_gian_di') }}">
			@if ($errors->has('thoi_gian_di'))
				<span class="validate_msg">{{ $errors->first('thoi_gian_di') }}</span>
			@endif
			<div id="target_chon_phong" hidden>
				Chọn phòng
				<br>
				<select name="ma_phong" id="select_ma_phong">
				</select>
			</div>
			@if ($errors->has('ma_phong'))
				<span class="validate_msg">{{ $errors->first('ma_phong') }}</span>
			@endif
			<br>
			<button class="form_button_success">Đặt</button>
		</form>
	</div>
</div>
@endsection

@push('script')
<script type="text/javascript">
$(document).ready(function() {
	$("#select_ma_hoa_don").select2({
		placeholder: "Chọn hóa đơn"
	});
	$("#select_ma_phong").select2({
		placeholder: "Chọn phòng"
	});
	$("#thoi_gian_den").change(function () {
		var thoi_gian_den = $(this).val();
		var thoi_gian_di = $("#thoi_gian_di").val();
		$("#target_chon_phong").hide();
		$("#select_ma_phong").empty();
		$.ajax({
			url: '{{ route('ajax.get_phong') }}',
			type: 'GET',
			dataType: 'json',
			data: {thoi_gian_den: thoi_gian_den,
				thoi_gian_di: thoi_gian_di},
		})
		.done(function(response) {
			console.log(response);
			$('#select_ma_phong').append(`<option></option>`);
			$.each(response, function(index, val) {
				 $('#select_ma_phong').append(`
				 	<option value="${val.ma_phong}">${val.ten_phong}</option>
				 	`);
			});
			$('#target_chon_phong').show();
		})
		.fail(function() {
			console.log("error");
		})
	});
	$("#thoi_gian_di").change(function () {
		var thoi_gian_den = $("#thoi_gian_den").val();
		var thoi_gian_di = $(this).val();
		$("#target_chon_phong").hide();
		$("#select_ma_phong").empty();
		$.ajax({
			url: '{{ route('ajax.get_phong') }}',
			type: 'GET',
			dataType: 'json',
			data: {thoi_gian_den: thoi_gian_den,
				thoi_gian_di: thoi_gian_di},
		})
		.done(function(response) {
			$('#select_ma_phong').append(`<option></option>`);
			$.each(response, function(index, val) {
				 $('#select_ma_phong').append(`
				 	<option value="${val.ma_phong}">${val.ten_phong}</option>
				 	`);
			});
			$('#target_chon_phong').show();
		})
		.fail(function() {
			console.log("error");
		})
	});
});
</script>
@endpush