@extends('layout.master')

@section('title', 'Thêm phòng')

@section('navigation')
<a href="{{ route('phong.index') }}">Phòng</a> → Thêm phòng
@endsection

@section('search')
@endsection

@section('content')
<div class="grid_1">
	<div>
		<form action="{{ route('phong.store') }}" method="POST">
			{{ csrf_field() }}
			Loại phòng
			<br>
			<select name="ma_loai_phong">
				<option disabled selected>Loại phòng</option>
				@foreach ($array_phong as $each)
					<option value="{{ $each->ma_loai_phong }}">{{ $each->ten_loai_phong }}</option>
				@endforeach
			</select>
			@if ($errors->has('ma_loai_phong'))
				<span class="validate_msg">{{ $errors->first('ma_loai_phong') }}</span>
			@endif
			<br>
			Tên phòng
			<br>
			<input type="text" name="ten_phong" placeholder="(VD: 202)" value="{{ old('ten_phong') }}">
			@if ($errors->has('ten_phong'))
				<span class="validate_msg">{{ $errors->first('ten_phong') }}</span>
			@endif
			<br>
			Tầng
			<br>
			<input type="number" name="tang" placeholder="(VD: 2)" value="{{ old('tang') }}">
			@if ($errors->has('tang'))
				<span class="validate_msg">{{ $errors->first('tang') }}</span>
			@endif
			<br>
			View
			<br>
			<select name="ma_view">
				<option disabled selected>View</option>
				@foreach ($array_view as $each)
					<option value="{{ $each->ma_view }}">{{ $each->ten_view }}</option>
				@endforeach
			</select>
			@if ($errors->has('ma_view'))
				<span class="validate_msg">{{ $errors->first('ma_view') }}</span>
			@endif
			<br>
			Trạng thái phòng
			<br>
			<select name="ma_trang_thai_phong">
				<option disabled selected>Trạng thái</option>
				@foreach ($array_trang_thai_phong as $each)
					<option value="{{ $each->ma_trang_thai_phong }}">{{ $each->ten_trang_thai_phong }}</option>
				@endforeach
			</select>
			@if ($errors->has('ma_trang_thai_phong'))
				<span class="validate_msg">{{ $errors->first('ma_trang_thai_phong') }}</span>
			@endif
			<br>
			<button class="form_button_success">Thêm</button>
		</form>
	</div>
</div>
@endsection