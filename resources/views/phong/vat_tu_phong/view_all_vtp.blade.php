@extends('layout.master')

@section('title', 'Vật tư phòng')

@section('navigation')
<a href="{{ route('phong.index') }}">Phòng</a> → Vật tư phòng
@endsection

@section('search')
@endsection

@section('content')
<div class="grid_1">
	<div class="grid_content">
		<table width="40%" align="center">
			<tr>
				<td>Phòng: {{ $phong->ten_phong }} - {{ $loai_phong->ten_loai_phong }}</td>
				<td>Giá phòng: {{ number_format($loai_phong->gia_phong) }} đồng</td>
			</tr>
		</table>
	</div>
	<div class="grid_content">
		<table class="form_table">
			<tr>
				<th>Tên vật tư</th>
				<th>Số lượng theo loại phòng</th>
				<th>Số lượng thực tế</th>
				<th>Trạng thái của vật tư thiếu</th>
				<th colspan="2">Điều chỉnh</th>
			</tr>
			@foreach ($array_vat_tu_phong as $each)
				<tr>
					<td>{{ $each->ten_vat_tu }}</td>
					<td>{{ $each->so_luong_vat_tu_lp }}</td>
					<td>{{ $each->so_luong_vat_tu }}</td>
					<td>
						@if ($each->so_luong_vat_tu_lp - $each->so_luong_vat_tu == 0)
							Đủ vật tư
						@elseif ($each->so_luong_vat_tu_lp - $each->so_luong_vat_tu < 0)
							Thừa vật tư
						@else
							{{ $each->getTrangThaiVatTu() }}
						@endif
					</td>
					<td>
						@if ($each->so_luong_vat_tu_lp - $each->so_luong_vat_tu == 0)
							<button disabled class="form_button_alternative button_disabled">Đủ vật tư</button>
						@elseif ($each->so_luong_vat_tu_lp - $each->so_luong_vat_tu < 0)
							<button disabled class="form_button_alternative button_disabled">Thừa vật tư</button>
						@else
							@for ($i = 0; $i < 3; $i++)
								@if ($i != $each->trang_thai_vat_tu)
									<form action="{{ route('vat_tu_phong.update', ['ma_phong' => $ma_phong, 'ma_vat_tu' => $each->ma_vat_tu]) }}" method="POST">
										{{ method_field('PUT') }}
										{{ csrf_field() }}
										<input type="hidden" name="trang_thai_vat_tu" value="{{ $i }}">
										<button class="form_button_danger">{{ $each->getTrangThaiVatTuForm($i) }}</button>
									</form>
								@endif
							@endfor
						@endif
					</td>
					<td><a href="{{ route('vat_tu_phong.edit', [
								'ma_phong' => $ma_phong,
								'ma_vat_tu' => $each->ma_vat_tu
							]) }}" class="form_button_warning">Cập nhật số lượng</a></td>
				</tr>
			@endforeach
		</table>
	</div>
</div>
@endsection