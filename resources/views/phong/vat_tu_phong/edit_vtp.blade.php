@extends('layout.master')

@section('title', 'Cập nhật vật tư phòng')

@section('navigation')
<a href="{{ route('phong.index') }}">Phòng</a> → <a href="{{ route('vat_tu_phong.index',['ma_phong' => $ma_phong]) }}">Vật tư phòng</a> → Cập nhật vật tư phòng
@endsection

@section('search')
@endsection

@section('content')
<div class="grid_1">
	<div class="grid_content">
		<table width="40%" align="center">
			<tr>
				<td>Phòng: {{ $phong->ten_phong }} - {{ $loai_phong->ten_loai_phong }}</td>
				<td>Giá phòng: {{ $loai_phong->gia_phong }} đồng</td>
			</tr>
		</table>
	</div>
	<div class="grid_content">
		<form action="{{ route('vat_tu_phong.update', ['ma_phong' => $ma_phong, 'ma_vat_tu' => $vat_tu_phong->ma_vat_tu]) }}" method="POST">
			{{ method_field('PUT') }}
			{{ csrf_field() }}
			Tên vật tư:
			<input type="text" value="{{ $vat_tu_phong->ten_vat_tu }}" disabled>
			<br>
			Số lượng tồn kho:
			<input type="number" disabled value="{{ $kho->tong_so_luong_vat_tu - $kho->so_luong_vat_tu_hong }}">
			<br>
			Số lượng theo loại phòng:
			<input type="number" value="{{ $vat_tu_phong->so_luong_vat_tu_lp }}" disabled>
			<br>
			--------------------
			<br>
			Số lượng thực tế:
			<br>
			<input type="number" name="so_luong_vat_tu" value="{{ $vat_tu_phong->so_luong_vat_tu }}">
			@if ($errors->has('so_luong_vat_tu'))
				<span class="validate_msg">{{ $errors->first('so_luong_vat_tu') }}</span>
			@endif
			<br>
			<input type="hidden" name="so_luong_vat_tu_cu" value="{{ $vat_tu_phong->so_luong_vat_tu }}">
			<button class="form_button_success">Cập nhật</button>
		</form>
	</div>
</div>
@endsection