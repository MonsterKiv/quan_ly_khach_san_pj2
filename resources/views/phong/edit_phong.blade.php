@extends('layout.master')

@section('title', 'Sửa phòng')

@section('navigation')
<a href="{{ route('phong.index') }}">Phòng</a> → Sửa phòng
@endsection

@section('search')
@endsection

@section('content')
<div class="grid_1">
	<div>
		<form action="{{ route('phong.update', ['ma_phong' => $phong->ma_phong]) }}" method="POST">
			{{ method_field('PUT') }}
			{{ csrf_field() }}
			Loại phòng: 
			<input type="text" disabled value="{{ $loai_phong->ten_loai_phong }}">
			<br>
			Tên phòng
			<br>
			<input type="text" name="ten_phong" placeholder="(VD: 202)" value="{{ $phong->ten_phong }}">
			@if ($errors->has('ten_phong'))
				<span class="validate_msg">{{ $errors->first('ten_phong') }}</span>
			@endif
			<br>
			Tầng
			<br>
			<input type="number" name="tang" value="{{ $phong->tang }}">
			@if ($errors->has('tang'))
				<span class="validate_msg">{{ $errors->first('tang') }}</span>
			@endif
			<br>
			View
			<br>
			<select name="ma_view">
				@foreach ($array_view as $each)
					<option value="{{ $each->ma_view }}" @if ($each->ma_view == $phong->ma_view) selected @endif>
						{{ $each->ten_view }}
					</option>
				@endforeach
			</select>
			@if ($errors->has('ma_view'))
				<span class="validate_msg">{{ $errors->first('ma_view') }}</span>
			@endif
			<br>
			<button class="form_button_success">Sửa</button>
		</form>
	</div>
</div>
@endsection