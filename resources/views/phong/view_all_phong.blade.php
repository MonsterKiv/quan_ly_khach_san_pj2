@extends('layout.master')

@section('title', 'Phòng')

@section('navigation')
Phòng
@endsection

@section('form_popup')
<div class="form_popup" id="create_phong">
	<div class="form_popup_header">
		<span class="form_popup_close_button">&times;</span>
		<span class="form_popup_title">Thêm phòng mới</span>
	</div>
	<div class="form_popup_content">
		<form action="{{ route('phong.store') }}" method="POST">
		{{ csrf_field() }}
		<table class="form_popup_table">
			<tr>
				<td class="form_popup_td_label">Loại phòng</td>
				<td class="form_popup_td_input">
					<select name="ma_loai_phong" id="ma_loai_phong">
						<option></option>
						@foreach ($array_loai_phong as $each)
							<option value="{{ $each->ma_loai_phong }}">{{ $each->ten_loai_phong }}</option>
						@endforeach
					</select>
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Tên phòng</td>
				<td class="form_popup_td_input">
					<input type="text" name="ten_phong" placeholder="VD: 202" value="{{ old('ten_phong') }}">
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Tầng</td>
				<td class="form_popup_td_input">
					<input type="number" name="tang" placeholder="VD: 2" value="{{ old('tang') }}">
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">View</td>
				<td class="form_popup_td_input">
					<select name="ma_view" id="ma_view">
						<option></option>
						@foreach ($array_view as $each)
							<option value="{{ $each->ma_view }}">{{ $each->ten_view }}</option>
						@endforeach
					</select>
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Trạng thái phòng</td>
				<td class="form_popup_td_input">
					<select name="ma_trang_thai_phong" id="ma_trang_thai_phong">
						<option></option>
						@foreach ($array_trang_thai_phong as $each)
							<option value="{{ $each->ma_trang_thai_phong }}">{{ $each->ten_trang_thai_phong }}</option>
						@endforeach
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<button class="form_button_success">Thêm</button>
				</td>
			</tr>
		</table>
		</form>
	</div>
</div>

<div class="form_popup" id="edit_phong">
	<div class="form_popup_header">
		<span class="form_popup_close_button">&times;</span>
		<span class="form_popup_title">Sửa đổi phòng</span>
	</div>
	<div class="form_popup_content">
		<form method="POST">
			{{ method_field('PUT') }}
			{{ csrf_field() }}
		<table class="form_popup_table">
			<tr>
				<td class="form_popup_td_label">Loại phòng</td>
				<td class="form_popup_td_input">
					<span id="loai_phong_edit"></span>
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Tên phòng</td>
				<td class="form_popup_td_input">
					<input type="text" name="ten_phong" placeholder="VD: 202" id="ten_phong_edit">
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Tầng</td>
				<td class="form_popup_td_input">
					<input type="number" name="tang" placeholder="VD: 2" id="tang_edit">
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">View</td>
				<td class="form_popup_td_input">
					<select name="ma_view" id="ma_view_edit">
						<option></option>
						@foreach ($array_view as $each)
							<option value="{{ $each->ma_view }}">{{ $each->ten_view }}</option>
						@endforeach
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<button class="form_button_success">Sửa</button>
				</td>
			</tr>
		</table>
		</form>
	</div>
</div>
@endsection

@section('search')
<form action="{{ route('phong.index') }}" method="GET">
	<select class="search_item" name="loai_phong[]" multiple="multiple" data-placeholder="Chọn loại phòng" style="width: 120px">
		@foreach ($array_loai_phong as $each)
			@if (isset($loai_phong) && in_array($each->ma_loai_phong, $loai_phong))
				<option value="{{ $each->ma_loai_phong }}" selected>{{ $each->ten_loai_phong }}</option>
			@else
				<option value="{{ $each->ma_loai_phong }}">{{ $each->ten_loai_phong }}</option>
			@endif
		@endforeach
	</select>
	<select class="search_item" name="view[]" multiple="multiple" data-placeholder="Chọn view" style="width: 120px">
		@foreach ($array_view as $each)
			@if (isset($view) && in_array($each->ma_view, $view))
				<option value="{{ $each->ma_view }}" selected>{{ $each->ten_view }}</option>
			@else
				<option value="{{ $each->ma_view }}">{{ $each->ten_view }}</option>
			@endif
		@endforeach
	</select>
	Thời gian:
	<input type="text" id="from" name="ngay_den" placeholder="Ngày đến" value="{{ $ngay_den }}" style="width: 70px">
	tới
	<input type="text" id="to" name="ngay_di" placeholder="Ngày đi" value="{{ $ngay_di }}" style="width: 70px">
	<button class="form_button_success">Tìm kiếm</button>
</form>
@endsection

@section('content')
<div class="grid_1">
	<div class="grid_content">
		<a class="form_button_success open_popup_form" id="button_create_phong">Thêm phòng</a>
		<table class="form_table">
			<tr>
				<th>Tên phòng</th>
				<th>Loại phòng</th>
				<th>Giá phòng (VND)</th>
				<th>Vị trí</th>
				<th>Vật tư</th>
				<th>Trạng thái phòng</th>
				<th colspan="3">Điều chỉnh</th>
			</tr>
			@if ($array_phong->isEmpty())
				<tr>
					<td colspan="7">
						<p>Không có dữ liệu</p>
					</td>
				</tr>
			@else
			@foreach ($array_phong as $each)
				<tr data-key="{{ $each->ma_phong }}">
					<td>
						<p>{{ $each->ten_phong }}</p>
						<div>
							
						</div>
					</td>
					<td>
						<p>{{ $each->ten_loai_phong }}</p>
					</td>
					<td>
						<p>{{ number_format($each->gia_phong) }}</p>
					</td>
					<td>
						<p>Tầng {{ $each->tang }} - {{ $each->ten_view }}</p>
					</td>
					<td>
						<a href="{{ route('vat_tu_phong.index', ['ma_phong' => $each->ma_phong]) }}" class="form_button_redirect">Xem</a>
					</td>
					<td>
						@php
							$check_ordered = 0;
						@endphp
						@if (isset($array_phong_ordered))
							@foreach ($array_phong_ordered as $element)
								@if (in_array($each->ma_phong, $element))
									@php
										$check_ordered = 1;
									@endphp
									@break
								@endif
							@endforeach
						@endif
						{{ $each->ten_trang_thai_phong }}
					</td>
					<td>
						@if ($check_ordered == 1)
							<button disabled class="form_button_alternative button_disabled">Đang có khách</button>
						@else
							@for ($i = 1; $i < 3; $i++)
								@if ($i != $each->ma_trang_thai_phong)
									<form action="{{ route('phong.update', ['ma_phong' => $each->ma_phong]) }}" method="POST">
										{{ method_field('PUT') }}
										{{ csrf_field() }}
										<input type="hidden" name="trang_thai_phong" value="{{ $i }}">
										<input type="hidden" name="ten_phong" value="{{ $each->ten_phong }}">
										<input type="hidden" name="tang" value="{{ $each->tang }}">
										<input type="hidden" name="view" value="{{ $each->view }}">
										<button class="form_button_alternative">{{ $each->getTrangThaiPhongForm($i) }}</button>
									</form>
								@endif
							@endfor
						@endif
					</td>
					<td>
						@if ($check_ordered == 1)
							<a class="form_button_warning button_disabled">Sửa</a>
						@else
							<a class="form_button_warning open_popup_form button_edit_phong" data-url="{{ route('phong.edit', ['ma_phong' => $each->ma_phong]) }}" data-url_action="{{ route('phong.update', ['ma_phong' => $each->ma_phong]) }}">Sửa</a>
						@endif
					</td>
					<td>
						@if ($check_ordered == 1)
							<button class="form_button_danger button_disabled">Xóa</button>
						@else
							<button class="form_button_danger button_trigger">Xóa</button>
							<div style="display: none;">
								<form>
									{{ method_field('DELETE') }}
									{{ csrf_field() }}
									Bạn chắc chứ?
									<br>
									<button class="form_button_danger" formaction="{{ route('phong.destroy', ['ma_phong' => $each->ma_phong]) }}" formmethod="POST">Xóa</button>
									<button class="form_button_return button_return">Quay lại</button>
								</form>
							</div>
						@endif
					</td>
				</tr>
			@endforeach
			@endif
		</table>
		{{ $array_phong->appends(['loai_phong' => $loai_phong, 'ngay_den' => $ngay_den, 'ngay_di' => $ngay_di, 'view' => $view])->render('layout.custom_paginate') }}
	</div>
	<div class="grid_content">
		<div id="calendar"></div>
	</div>
</div>
@endsection

@push('script')
<script type="text/javascript">
	$(document).ready(function() {
		/* Active Select2 */
    	$('.search_item').select2({
    		width: 'resolve'
    	});
    	$('#ma_loai_phong').select2({
    		placeholder: "Chọn loại phòng"
    	});
    	$('#ma_view').select2({
    		placeholder: "Chọn view"
    	});
    	$('#ma_trang_thai_phong').select2({
    		placeholder: "Chọn trạng thái"
    	});
    	$('#ma_view_edit').select2({
    		width: 'resolve'
    	});

		/* Datetime Picker */
	    var dateFormat = "dd/mm/yy",
		from = $("#from").datepicker({
			defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 1,
			minDate: 0
		}).on("change", function() {
			to.datepicker("option", "minDate", getDate(this));
		}),

		to = $("#to").datepicker({
			defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 1
		}).on("change", function() {
			from.datepicker("option", "maxDate", getDate(this));
		});

		function getDate(element) {
			var date;
			try {
				date = $.datepicker.parseDate(dateFormat, element.value);
			} catch(error) {
				date = null;
			}

			return date;
	    }

	    /* Open Form Modal */
		$('#button_create_phong').click(function(event) {
			$('#create_phong').show();
		});
		$('.button_edit_phong').click(function(event) {
			var url = $(this).data('url');
			var url_action = $(this).data('url_action');
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				$('#edit_phong div form').attr('action', url_action);
				$('#loai_phong_edit').html(response.loai_phong.ten_loai_phong);
				$('#ten_phong_edit').val(response.phong.ten_phong);
				$('#tang_edit').val(response.phong.tang);
				$('#ma_view_edit option').each(function(index, el) {
					if ($(this).val() == response.phong.ma_view) {
						$('#ma_view_edit').val($(this).val()).trigger('change');
					}
				});
				$('#edit_phong').show();
			})
			.fail(function() {
				console.log("error");
			});
			
		});

	    /* Display fullcalendar */
		var calendarEl = document.getElementById('calendar');
        var calendar = new FullCalendar.Calendar(calendarEl, {
        	initialView: 'dayGridMonth',
        	locale: 'vi',
        	height: 700,
		    headerToolbar: {
		    	left: 'prev,next today',
		    	center: 'title',
		    	right: 'dayGridMonth,listWeek'
		    },
        	navLinks: true,
        	navLinkDayClick: 'listWeek',
        	eventClick: function(date){
        		calendar.changeView('listWeek', date.event.startStr);
        		$('.tooltip').hide();
        	},
        	eventDidMount: function(info) {
				var tooltip = new Tooltip(info.el, {
					title: 'Số lượng khách: ' + info.event.extendedProps.so_luong_khach + '<br>Giá phòng: ' + info.event.extendedProps.gia_dat_phong + 'đ',
					html: true,
					placement: 'top',
					trigger: 'hover',
					container: 'body'
				});
			},
        	displayEventEnd: true,
        	titleFormat: {
        		year: 'numeric',
        		month: '2-digit',
        		day: '2-digit'
        	},
        	views: {
        		dayGrid: {
		        	eventTimeFormat: {
		        		month: 'numeric',
		        		day: 'numeric',
			        	hour: 'numeric',
						minute: '2-digit',
						meridiem: false,
						omitZeroMinute: false
		        	}
        		}
        	},
        });
		calendar.render();

		/* Onclick change fullcalendar events */
	    $('.form_table tr').click(function(event) {
			var ma_phong = $(this).data('key');
			if ($(this).hasClass('form_table_tr_selected')) {return}
			$(this).addClass('form_table_tr_selected').siblings().removeClass("form_table_tr_selected");
			$.ajax({
				url: '{{ route('ajax.get_hoa_don_phong') }}',
				type: 'GET',
				dataType: 'JSON',
				data: {ma_phong: ma_phong},
			})
			.done(function(response) {
				var oldResource = calendar.getEventSources();
				var len = oldResource.length;
				for (var i = 0; i < len; i++) {
					oldResource[i].remove(); 
				}
				calendar.addEventSource(response);
				calendar.refetchEvents();
			})
			.fail(function() {
				console.log("error");
			});
		});
	});
</script>
@endpush