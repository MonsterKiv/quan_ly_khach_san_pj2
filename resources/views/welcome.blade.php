@extends('layout.master')

@section('title', 'Welcome')

@section('navigation')
Trang chủ
@endsection

@section('content')
<div class="grid_3">
	<div class="grid_content statistical_grid">
		<span>{{ $so_hoa_don_hien_tai }}</span>
		<span>Hóa đơn đang lưu trú</span>
		<a href="{{ route('hoa_don.index') }}">Chi tiết</a>
		<img src="{{ asset('images/interface-BW/png/invoice.png') }}">
	</div>

	<div class="grid_content statistical_grid">
		<span>{{ $tong_so_phong - $so_phong_dang_su_dung }}</span>
		<span>Phòng đang trống</span>
		<a href="{{ route('phong.index') }}">Chi tiết</a>
		<img src="{{ asset('images/interface-BW/png/door-hanger.png') }}">
	</div>

	<div class="grid_content statistical_grid">
		<span>{{ $so_luong_vat_tu_hong }}</span>
		<span>Vật tư đang hỏng / sửa chữa</span>
		<a href="{{ route('kho.index') }}">Chi tiết</a>
		<img src="{{ asset('images/interface-BW/png/air-conditioner.png') }}">
	</div>

	<div class="grid_content statistical_grid">
		<span>{{ $so_hoa_don_can_thanh_toan }}</span>
		<span>Hóa đơn cần thanh toán</span>
		<a href="{{ route('hoa_don.index') }}">Chi tiết</a>
		<img src="{{ asset('images/interface-BW/png/invoice-warn.png') }}">
	</div>

	<div class="grid_content statistical_grid">
		<span>{{ $tong_so_dich_vu_san_sang }}/{{ $tong_so_dich_vu }}</span>
		<span>Dịch vụ sẵn sàng</span>
		<a href="{{ route('dich_vu.index') }}">Chi tiết</a>
		<img src="{{ asset('images/interface-BW/png/room-service.png') }}">
	</div>

	<div class="grid_content statistical_grid">
		<span>{{ $tong_so_phong_chua_co_vat_tu }}</span>
		<span>Phòng chưa đủ loại vật tư</span>
		<a href="{{ route('phong.index') }}">Chi tiết</a>
		<img src="{{ asset('images/interface-BW/png/air-conditioner.png') }}">
	</div>
</div>
@endsection