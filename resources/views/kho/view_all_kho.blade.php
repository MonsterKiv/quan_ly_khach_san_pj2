@extends('layout.master')

@section('title', 'Kho')

@section('navigation')
Kho
@endsection

@section('search')
@endsection

@section('form_popup')
<div class="form_popup" id="edit_kho">
	<div class="form_popup_header">
		<span class="form_popup_close_button">&times;</span>
		<span class="form_popup_title">Thay đổi số lượng vật tư trong kho</span>
	</div>
	<div class="form_popup_content">
		<form method="POST">
		{{ method_field('PUT') }}
		{{ csrf_field() }}
		<table class="form_popup_table">
			<tr>
				<td class="form_popup_td_label">Tên vật tư</td>
				<td class="form_popup_td_input">
					<span id="ten_vat_tu_edit"></span>
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Số lượng tổng</td>
				<td class="form_popup_td_input">
					<span id="so_luong_tong_edit"></span>
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Số lượng hỏng / sửa chữa</td>
				<td class="form_popup_td_input">
					<span id="so_luong_hong_sua_chua_edit"></span>
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Số lượng sẵn sàng</td>
				<td class="form_popup_td_input">
					<span id="so_luong_san_sang_edit"></span>
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Số lượng thay đổi</td>
				<td class="form_popup_td_input">
					<input type="number" name="so_luong_vat_tu" placeholder="Nhập số lượng">
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Thao tác</td>
				<td class="form_popup_td_input">
					<input type="radio" name="action" value="1">Nhập
					<br>
					<input type="radio" name="action" value="2">Xuất
					<br>
					<input type="radio" name="action" value="3">Hỏng / Sửa chữa
					<br>
					<input type="radio" name="action" value="4">Sửa chữa xong
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<button class="form_button_success">Thay đổi</button>
				</td>
			</tr>
		</table>
		</form>
	</div>
</div>

@section('content')
<div class="grid_1">
	<div class="grid_content">
		<table class="form_table">
			<tr>
				<th>Tên vật tư</th>
				<th>Tổng</th>
				<th>Hỏng/sửa chữa</th>
				<th>Sẵn sàng</th>
				<th>Điều chỉnh</th>
			</tr>
			@foreach ($array_kho as $each)
				<tr>
					<td>
						<p>
							{{ $each->ten_vat_tu }}
						</p>
					</td>
					<td>
						<p>
							{{ $each->tong_so_luong_vat_tu }}
						</p>
					</td>
					<td>
						<p>
							{{ $each->so_luong_vat_tu_hong }}
						</p>
					</td>
					<td>
						<p>
							{{ $each->tong_so_luong_vat_tu - $each->so_luong_vat_tu_hong }}
						</p>
					</td>
					<td>
						<a class="form_button_warning open_popup_form button_edit_kho" data-url="{{ route('kho.edit', ['ma_vat_tu' => $each->ma_vat_tu]) }}" data-url_action="{{ route('kho.update', ['ma_vat_tu' => $each->ma_vat_tu]) }}">Nhập / Xuất</a>
					</td>
				</tr>
			@endforeach
		</table>
	</div>
</div>
@endsection

@push('script')
<script type="text/javascript">
	$(document).ready(function() {
    	/* Open Form Modal */
    	$('.button_edit_kho').click(function(event) {
    		var url = $(this).data('url');
			var url_action = $(this).data('url_action');
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				$('#edit_kho div form').attr('action', url_action);
				$('#ten_vat_tu_edit').html(response.vat_tu_selected.ten_vat_tu);
				$('#so_luong_tong_edit').html(response.kho.tong_so_luong_vat_tu);
				$('#so_luong_hong_sua_chua_edit').html(response.kho.so_luong_vat_tu_hong);
				$('#so_luong_san_sang_edit').html(response.kho.tong_so_luong_vat_tu - response.kho.so_luong_vat_tu_hong);
				$('#edit_kho').show();
			})
			.fail(function() {
				console.log("error");
			});
    	});
    });
</script>
@endpush