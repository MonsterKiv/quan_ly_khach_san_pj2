@extends('layout.master')

@section('title', 'Thay đổi số lượng trong kho')

@section('navigation')
<a href="{{ route('kho.index') }}">Kho</a> → Thay đổi số lượng vật tư trong kho
@endsection

@section('search')
@endsection

@section('content')
<div class="grid_1">
	<div>
		<form id="formId">
			{{ method_field('PUT') }}
			{{ csrf_field() }}
			Vật tư thay đổi: 
			<input type="name" value="{{ $vat_tu_selected->ten_vat_tu }}" disabled>
			<br>
			Tổng số lượng
			<br>
			<input type="number" name="tong_so_luong_vat_tu" value="{{ $kho->tong_so_luong_vat_tu }}">
			{{-- @if ($errors->has('tong_so_luong_vat_tu'))
				<span class="validate_msg">{{ $errors->first('tong_so_luong_vat_tu') }}</span>
			@endif --}}
			<br>
			Số lượng hỏng/sửa chữa
			<br>
			<input type="number" name="so_luong_vat_tu_hong" value="{{ $kho->so_luong_vat_tu_hong }}">
			{{-- @if ($errors->has('so_luong_vat_tu_hong'))
				<span class="validate_msg">{{ $errors->first('so_luong_vat_tu_hong') }}</span>
			@endif --}}
			<br>
			<button id="button_submit" class="form_button_success">Thay đổi</button>
		</form>
	</div>
</div>
@endsection

@push('script')
<script type="text/javascript">
$(document).ready(function() {
	var url = '{{ route('kho.update', ['ma_vat_tu' => $kho->ma_vat_tu]) }}';
	ajaxForm(url);
});
</script>
@endpush