@extends('layout.master')

@section('title', 'Thêm vật tư')

@section('navigation')
<a href="{{ route('vat_tu.index') }}">Vật tư</a> → Thêm vật tư
@endsection

@section('search')
@endsection

@section('content')
<div class="grid_1">
	<div>
		<form action="{{ route('vat_tu.store') }}" method="POST">
			{{ csrf_field() }}
			Tên vật tư
			<br>
			<input type="text" name="ten_vat_tu" value="{{ old('ten_vat_tu') }}">
			@if ($errors->has('ten_vat_tu'))
				<span class="validate_msg">{{ $errors->first('ten_vat_tu') }}</span>
			@endif
			<br>
			Đơn giá (VND)
			<br>
			<input type="number" name="gia_vat_tu" value="{{ old('gia_vat_tu') }}">
			@if ($errors->has('gia_vat_tu'))
				<span class="validate_msg">{{ $errors->first('gia_vat_tu') }}</span>
			@endif
			<br>
			<button class="form_button_success">Thêm</button>
		</form>
	</div>
</div>
@endsection