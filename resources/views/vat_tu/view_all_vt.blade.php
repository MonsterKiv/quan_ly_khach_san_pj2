@extends('layout.master')

@section('title', 'Vật tư')

@section('navigation')
Vật tư
@endsection

@section('search')
@endsection

@section('form_popup')
<div class="form_popup" id="create_vat_tu">
	<div class="form_popup_header">
		<span class="form_popup_close_button">&times;</span>
		<span class="form_popup_title">Thêm vật tư mới</span>
	</div>
	<div class="form_popup_content">
		<form action="{{ route('vat_tu.store') }}" method="POST">
		{{ csrf_field() }}
		<table class="form_popup_table">
			<tr>
				<td class="form_popup_td_label">Tên vật tư</td>
				<td class="form_popup_td_input">
					<input type="text" name="ten_vat_tu" placeholder="VD: Điều hòa 2 chiều" value="{{ old('ten_vat_tu') }}">
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Đơn giá (VND)</td>
				<td class="form_popup_td_input">
					<input type="number" name="gia_vat_tu" placeholder="VD: 5000000" value="{{ old('gia_vat_tu') }}">
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<button class="form_button_success">Thêm</button>
				</td>
			</tr>
		</table>
		</form>
	</div>
</div>
<div class="form_popup" id="edit_vat_tu">
	<div class="form_popup_header">
		<span class="form_popup_close_button">&times;</span>
		<span class="form_popup_title">Sửa thông tin vật tư</span>
	</div>
	<div class="form_popup_content">
		<form method="POST">
		{{ method_field('PUT') }}
		{{ csrf_field() }}
		<table class="form_popup_table">
			<tr>
				<td class="form_popup_td_label">Tên vật tư</td>
				<td class="form_popup_td_input">
					<input type="text" name="ten_vat_tu" placeholder="VD: Điều hòa 2 chiều" id="ten_vat_tu_edit">
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Đơn giá (VND)</td>
				<td class="form_popup_td_input">
					<input type="number" name="gia_vat_tu" placeholder="VD: 5000000" id="gia_vat_tu_edit">
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<button class="form_button_success">Sửa</button>
				</td>
			</tr>
		</table>
		</form>
	</div>
</div>
@endsection

@section('content')
<div class="grid_1">
	<div class="grid_content">
		<a class="form_button_success open_popup_form" id="button_create_vat_tu">Thêm vật tư</a>
		<table class="form_table">
			<tr>
				<th>Tên vật tư</th>
				<th>Giá vật tư (VND)</th> 
				<th colspan="2">Thao tác</th>
			</tr>
			@foreach ($array_vat_tu as $each)
				<tr>
					<td>
						<p>
							{{ $each->ten_vat_tu }}
						</p>
					</td>
					<td>
						<p>
							{{ number_format($each->gia_vat_tu) }}
						</p>
					</td>
					<td><a class="form_button_warning open_popup_form button_edit_vat_tu" data-url="{{ route('vat_tu.edit', ['ma_vat_tu' => $each->ma_vat_tu]) }}" data-url_action="{{ route('vat_tu.update', ['ma_vat_tu' => $each->ma_vat_tu]) }}">Sửa</a></td>
					<td>
						<button class="form_button_danger button_trigger">Xóa</button>
						<div style="display: none;">
							<form>
								{{ method_field('DELETE') }}
								{{ csrf_field() }}
								Bạn chắc chứ?
								<br>
								<button class="form_button_danger" formaction="{{ route('vat_tu.destroy',['ma_vat_tu' => $each->ma_vat_tu]) }}" formmethod="POST">Xóa</button>
								<button class="form_button_return button_return">Quay lại</button>
							</form>
						</div>
					</td>
				</tr>
			@endforeach
		</table>
	</div>
</div>
@endsection

@push('script')
<script type="text/javascript">
	$(document).ready(function() {
    	/* Open Form Modal */
    	$('#button_create_vat_tu').click(function(event) {
    		$('#create_vat_tu').show();
    	});
    	$('.button_edit_vat_tu').click(function(event) {
    		var url = $(this).data('url');
			var url_action = $(this).data('url_action');
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				$('#edit_vat_tu div form').attr('action', url_action);
				$('#ten_vat_tu_edit').val(response.vat_tu.ten_vat_tu);
				$('#gia_vat_tu_edit').val(response.vat_tu.gia_vat_tu);
				$('#edit_vat_tu').show();
			})
			.fail(function() {
				console.log("error");
			});
    	});
    });
</script>
@endpush