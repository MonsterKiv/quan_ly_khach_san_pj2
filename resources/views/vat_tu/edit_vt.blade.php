@extends('layout.master')

@section('title', 'Sửa đổi vật tư')

@section('navigation')
<a href="{{ route('vat_tu.index') }}">Vật tư</a> → Sửa đổi vật tư
@endsection

@section('search')
@endsection

@section('content')
<div class="grid_1">
	<div>
		<form id="formId">
			{{ method_field('PUT') }}
			{{ csrf_field() }}
			Tên vật tư
			<br>
			<input type="text" name="ten_vat_tu" value="{{ $vat_tu->ten_vat_tu }}">
			{{-- @if ($errors->has('ten_vat_tu'))
				<span class="validate_msg">{{ $errors->first('ten_vat_tu') }}</span>
			@endif --}}
			<br>
			Đơn giá (VND)
			<br>
			<input type="number" name="gia_vat_tu" value="{{ $vat_tu->gia_vat_tu }}">
			{{-- @if ($errors->has('gia_vat_tu'))
				<span class="validate_msg">{{ $errors->first('gia_vat_tu') }}</span>
			@endif --}}
			<br>
			<button id="button_submit" class="form_button_success">Thay đổi</button>
		</form>
	</div>
</div>
@endsection

@push('script')
<script type="text/javascript">
$(document).ready(function() {
	var url = '{{ route('vat_tu.update', ['ma_vat_tu' => $vat_tu->ma_vat_tu]) }}';
	ajaxForm(url);
});
</script>
@endpush