<!DOCTYPE html>
<html>
<head>
	<title>Đăng nhập</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/login.css') }}">
</head>
<body>
<div class="all">
	<div>
		<img src="{{ url('Images/login_background.jpg') }}" id="bg-img">
	</div>
	<div>
		<div class="form-login">
			@if (Session::has('error'))
				<div class="alert">
					<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
					{{ Session::get('error') }}
				</div>
			@endif
			@if (Session::has('logout'))
				<div class="success">
					<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
					{{ Session::get('logout') }}
				</div>
			@endif
			<form method="POST" action="{{ route('process_login') }}">
				{{ csrf_field() }}
				<input type="text" name="ten_dang_nhap" placeholder="Tên đăng nhập">
				<br>
				<input type="password" name="mat_khau" placeholder="Mật khẩu">
				<br>
				<button>Đăng nhập</button>
			</form>
		</div>
	</div>
</div>
</body>
</html>
