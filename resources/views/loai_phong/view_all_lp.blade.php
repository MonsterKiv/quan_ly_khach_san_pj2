@extends('layout.master')

@section('title', 'Loại phòng')

@section('navigation')
Loại phòng
@endsection

@section('search')
@endsection

@section('form_popup')
<div class="form_popup" id="create_loai_phong">
	<div class="form_popup_header">
		<span class="form_popup_close_button">&times;</span>
		<span class="form_popup_title">Thêm loại phòng mới</span>
	</div>
	<div class="form_popup_content">
		<form action="{{ route('loai_phong.store') }}" method="POST">
			{{ csrf_field() }}
			<table class="form_popup_table">
				<tr>
					<td class="form_popup_td_label">Tên loại phòng</td>
					<td class="form_popup_td_input">
						<input type="text" name="ten_loai_phong" placeholder="VD: Thường" value="{{ old('ten_loai_phong') }}">
					</td>
				</tr>
				<tr>
					<td class="form_popup_td_label">Giá phòng (VND)</td>
					<td class="form_popup_td_input">
						<input type="number" name="gia_phong" placeholder="VD: 100000" value="{{ old('gia_phong') }}">
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<button class="form_button_success">Thêm</button>
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>
<div class="form_popup" id="edit_loai_phong">
	<div class="form_popup_header">
		<span class="form_popup_close_button">&times;</span>
		<span class="form_popup_title">Sửa đổi loại phòng</span>
	</div>
	<div class="form_popup_content">
		<form method="POST">
			{{ method_field('PUT') }}
			{{ csrf_field() }}
			<table class="form_popup_table">
				<tr>
					<td class="form_popup_td_label">Tên loại phòng</td>
					<td class="form_popup_td_input">
						<input type="text" name="ten_loai_phong" placeholder="VD: Thường" id="ten_loai_phong_edit">
					</td>
				</tr>
				<tr>
					<td class="form_popup_td_label">Giá phòng (VND)</td>
					<td class="form_popup_td_input">
						<input type="number" name="gia_phong" placeholder="VD: 100000" id="gia_phong_edit">
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<button class="form_button_success">Sửa</button>
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>
@endsection

@section('content')
<div class="grid_1">
	<div class="grid_content">
		<a class="form_button_success open_popup_form" id="button_create_loai_phong">Thêm loại phòng</a>
		<table class="form_table">
			<tr>
				<th>Tên loại phòng</th>
				<th>Giá phòng (VND)</th>
				<th>Vật tư</th>
				<th colspan="2">Điều chỉnh</th>
			</tr>
			@foreach ($array_loai_phong as $each)
				<tr>
					<td>
						<p>{{ $each->ten_loai_phong }}</p>
					</td>
					<td>
						<p>
							{{ number_format($each->gia_phong) }}
						</p>
					</td>
					<td><a href="{{ route('vat_tu_loai_phong.index', ['ma_loai_phong' => $each->ma_loai_phong]) }}" class="form_button_redirect">Xem</a></td>
					<td><a class="form_button_warning open_popup_form button_edit_loai_phong" data-url="{{ route('loai_phong.edit', ['ma_loai_phong' => $each->ma_loai_phong]) }}" data-url_action="{{ route('loai_phong.update', ['ma_loai_phong' => $each->ma_loai_phong]) }}">Sửa</a></td>
					<td>
						<button class="form_button_danger button_trigger">Xóa</button>
						<div style="display: none;">
							<form>
								{{ method_field('DELETE') }}
								{{ csrf_field() }}
								Bạn chắc chứ?
								<br>
								<button class="form_button_danger" formaction="{{ route('loai_phong.destroy', ['ma_loai_phong' => $each->ma_loai_phong]) }}" formmethod="POST">Xóa</button>
								<button class="form_button_return button_return">Quay lại</button>
							</form>
						</div>
					</td>
				</tr>
			@endforeach
		</table>
	</div>
</div>
@endsection

@push('script')
<script type="text/javascript">
	$(document).ready(function() {
		/* Open Form Modal */
		$('#button_create_loai_phong').click(function(event) {
			$('#create_loai_phong').show();
		});
		$('.button_edit_loai_phong').click(function(event) {
			var url = $(this).data('url');
			var url_action = $(this).data('url_action');
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				$('#edit_loai_phong div form').attr('action', url_action);
				$('#ten_loai_phong_edit').val(response.loai_phong.ten_loai_phong);
				$('#gia_phong_edit').val(response.loai_phong.gia_phong);
				$('#edit_loai_phong').show();
			})
			.fail(function() {
				console.log("error");
			});
		});
	});
</script>
@endpush