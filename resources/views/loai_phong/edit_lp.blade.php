@extends('layout.master')

@section('title', 'Sửa đổi loại phòng')

@section('navigation')
<a href="{{ route('loai_phong.index') }}">Loại phòng</a> → Sửa đổi loại phòng
@endsection

@section('search')
@endsection

@section('content')
<div class="grid_1">
	<div>
		<form id="formId">
			{{ method_field('PUT') }}
			{{ csrf_field() }}
			Tên loại phòng
			<br>
			<input type="text" name="ten_loai_phong" value="{{ $loai_phong->ten_loai_phong }}">
			{{-- @if ($errors->has('ten_loai_phong'))
				<span class="validate_msg">{{ $errors->first('ten_loai_phong') }}</span>
			@endif --}}
			<br>
			Giá phòng (VND)
			<br>
			<input type="number" name="gia_phong" value="{{ $loai_phong->gia_phong }}">
			{{-- @if ($errors->has('gia_phong'))
				<span class="validate_msg">{{ $errors->first('gia_phong') }}</span>
			@endif --}}
			<br>
			<button id="button_submit" class="form_button_success">Thay đổi</button>
		</form>
	</div>
</div>
@endsection

@push('script')
<script type="text/javascript">
$(document).ready(function() {
	var url = '{{ route('loai_phong.update', ['ma_loai_phong' => $loai_phong->ma_loai_phong]) }}';
	ajaxForm(url);
});
</script>
@endpush