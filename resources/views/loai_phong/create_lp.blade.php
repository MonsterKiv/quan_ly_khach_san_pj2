@extends('layout.master')

@section('title', 'Thêm loại phòng')

@section('navigation')
<a href="{{ route('loai_phong.index') }}">Loại phòng</a> → Thêm loại phòng
@endsection

@section('search')
@endsection

@section('content')
<div class="grid_1">
	<div>
		<form id="formId" action="{{ route('loai_phong.store') }}" method="POST">
			{{ csrf_field() }}
			Tên loại phòng
			<br>
			<input type="text" name="ten_loai_phong" value="{{ old('ten_loai_phong') }}">
			@if ($errors->has('ten_loai_phong'))
				<span class="validate_msg">{{ $errors->first('ten_loai_phong') }}</span>
			@endif
			<br>
			Giá phòng (VND)
			<br>
			<input type="number" name="gia_phong" value="{{ old('gia_phong') }}">
			@if ($errors->has('gia_phong'))
				<span class="validate_msg">{{ $errors->first('gia_phong') }}</span>
			@endif
			<br>
			<button id="button_submit" class="form_button_success">Thêm</button>
		</form>
	</div>
</div>
@endsection

{{-- @push('script')
<script type="text/javascript">
$(document).ready(function() {
	var url = '{{ route('loai_phong.store') }}';
	ajaxForm(url);
});
</script>
@endpush --}}