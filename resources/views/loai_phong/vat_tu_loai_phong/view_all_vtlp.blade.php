@extends('layout.master')

@section('title', 'Vật tư loại phòng')

@section('navigation')
<a href="{{ route('loai_phong.index') }}">Loại phòng</a> → Vật tư loại phòng
@endsection

@section('search')
@endsection

@section('form_popup')
<div class="form_popup" id="create_vtlp">
	<div class="form_popup_header">
		<span class="form_popup_close_button">&times;</span>
		<span class="form_popup_title">Thêm vật tư loại phòng mới</span>
	</div>
	<div class="form_popup_content">
		<form action="{{ route('vat_tu_loai_phong.store',['ma_loai_phong' => $ma_loai_phong]) }}" method="POST">
		{{ csrf_field() }}
		<table class="form_popup_table">
			<tr>
				<td class="form_popup_td_label">Chọn vật tư</td>
				<td class="form_popup_td_input">
					<select name="ma_vat_tu" id="ma_vat_tu">
						<option></option>
						@foreach ($array_vat_tu as $each)
							<option value="{{ $each->ma_vat_tu }}">{{ $each->ten_vat_tu }}</option>
						@endforeach
					</select>
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Số lượng / phòng</td>
				<td class="form_popup_td_input">
					<input type="number" name="so_luong_vat_tu" id="so_luong_vat_tu" data-placeholder="VD: 3" value="{{ old('so_luong_vat_tu') }}">
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<button class="form_button_success">Thêm</button>
				</td>
			</tr>
		</table>
		</form>
	</div>
</div>
<div class="form_popup" id="edit_vtlp">
	<div class="form_popup_header">
		<span class="form_popup_close_button">&times;</span>
		<span class="form_popup_title">Thay đổi số lượng vật tư loại phòng</span>
	</div>
	<div class="form_popup_content">
		<form method="POST">
		{{ method_field('PUT') }}
		{{ csrf_field() }}
		<table class="form_popup_table">
			<tr>
				<td class="form_popup_td_label">Chọn vật tư</td>
				<td class="form_popup_td_input">
					<span id="ma_vat_tu_edit"></span>
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Số lượng / phòng</td>
				<td class="form_popup_td_input">
					<input type="number" name="so_luong_vat_tu" id="so_luong_vat_tu_edit" data-placeholder="VD: 3">
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<button class="form_button_success">Thay đổi</button>
				</td>
			</tr>
		</table>
		</form>
	</div>
</div>
@endsection

@section('content')
<div class="grid_1">
	<div class="grid_content">
		<table width="45%" align="center">
			<tr>
				<td>Loại phòng: {{ $loai_phong->ten_loai_phong }}</td>
				<td>Giá: {{ $loai_phong->gia_phong }} đồng</td>
			</tr>
		</table>
	</div>
	<div class="grid_content">
		<a class="form_button_success open_popup_form" id="button_create_vtlp">Thêm vật tư loại phòng</a>
		<table class="form_table">
			<tr>
				<th>Tên vật tư</th>
				<th>Số lượng</th>
				<th colspan="2">Điều chỉnh</th>
			</tr>
			@foreach ($array_vat_tu_loai_phong as $each)
				<tr>
					<td>
						<p>
							{{ $each->ten_vat_tu }}
						</p>
					</td>
					<td>
						<p>
							{{ $each->so_luong_vat_tu }}
						</p>
					</td>
					<td>
						<a data-url="{{ route('vat_tu_loai_phong.edit', [
								'ma_loai_phong' => $ma_loai_phong,
								'ma_vat_tu' => $each->ma_vat_tu
							]) }}" data-url_action="{{ route('vat_tu_loai_phong.update',['ma_loai_phong' => $ma_loai_phong, 'ma_vat_tu' => $each->ma_vat_tu]) }}" class="form_button_warning open_popup_form button_edit_vtlp">
							Cập nhật
						</a>
					</td>
					<td>
						<form action="{{ route('vat_tu_loai_phong.destroy', [
								'ma_loai_phong' => $ma_loai_phong,
								'ma_vat_tu' => $each->ma_vat_tu
							]) }}" method="POST">
							{{ method_field('DELETE') }}
							{{ csrf_field() }}
							<button class="form_button_danger">Xóa</button>
						</form>
					</td>
				</tr>
			@endforeach
		</table>
	</div>
</div>
@endsection

@push('script')
<script type="text/javascript">
	$(document).ready(function() {
		/* Active Select2 */
    	$('#ma_vat_tu').select2({
    		placeholder: 'Chọn vật tư'
    	});

    	/* Open Form Modal */
    	$('#button_create_vtlp').click(function(event) {
    		$('#create_vtlp').show();
    	});
    	$('.button_edit_vtlp').click(function(event) {
    		var url = $(this).data('url');
			var url_action = $(this).data('url_action');
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				$('#edit_vtlp div form').attr('action', url_action);
				$('#ma_vat_tu_edit').html(response.vat_tu_loai_phong.ten_vat_tu);
				$('#so_luong_vat_tu_edit').val(response.vat_tu_loai_phong.so_luong_vat_tu);
				$('#edit_vtlp').show();
			})
			.fail(function() {
				console.log("error");
			});
    	});
    });
</script>
@endpush