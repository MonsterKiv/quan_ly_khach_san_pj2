@extends('layout.master')

@section('title', 'Sửa đổi vật tư loại phòng')

@section('navigation')
<a href="{{ route('loai_phong.index') }}">Loại phòng</a> → <a href="{{ route('vat_tu_loai_phong.index',['ma_loai_phong' => $ma_loai_phong]) }}">Vật tư loại phòng</a> → Sửa đổi vật tư
@endsection

@section('search')
@endsection

@section('content')
<div class="grid_1">
	@foreach ($array_vat_tu_loai_phong as $each)
		<div>
			<table width="45%" align="center">
				<tr>
					<td>Loại phòng: {{ $each->ten_loai_phong }}</td>
					<td>Giá: {{ $each->gia_phong }} đồng</td>
				</tr>
			</table>
		</div>
		<div>
			<form action="{{ route('vat_tu_loai_phong.update',['ma_loai_phong' => $ma_loai_phong, 'ma_vat_tu' => $ma_vat_tu]) }}" method="POST">
				{{ method_field('PUT') }}
				{{ csrf_field() }}
				Vật tư: <input type="text" value="{{ $each->ten_vat_tu }}" disabled>	
				<br>
				Số lượng / phòng
				<br>
				<input type="number" name="so_luong_vat_tu" value="{{ $each->so_luong_vat_tu }}">
				@if ($errors->has('so_luong_vat_tu'))
					<span class="validate_msg">{{ $errors->first('so_luong_vat_tu') }}</span>
				@endif
				<br>
				<button class="form_button_success">Sửa đổi</button>
			</form>
		</div>
	@endforeach
</div>
@endsection