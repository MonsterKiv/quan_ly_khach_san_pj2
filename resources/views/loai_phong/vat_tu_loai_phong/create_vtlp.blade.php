@extends('layout.master')

@section('title', 'Thêm vật tư loại phòng')

@section('navigation')
<a href="{{ route('loai_phong.index') }}">Loại phòng</a> → <a href="{{ route('vat_tu_loai_phong.index',['ma_loai_phong' => $ma_loai_phong]) }}">Vật tư loại phòng</a> → Thêm vật tư
@endsection

@section('search')
@endsection

@section('content')
<div class="grid_1">
	<div>
		<table width="45%" align="center">
			<tr>
				<td>Loại phòng: {{ $loai_phong->ten_loai_phong }}</td>
				<td>Giá: {{ $loai_phong->gia_phong }} đồng</td>
			</tr>
		</table>
	</div>
	<div>
		<form action="{{ route('vat_tu_loai_phong.store',['ma_loai_phong' => $ma_loai_phong]) }}" method="POST">
			{{ csrf_field() }}
			Chọn vật tư
			<br>
			<select name="ma_vat_tu">
				<option disabled selected>Vật tư</option>
				@foreach ($array_vat_tu as $each)
					<option value="{{ $each->ma_vat_tu }}">{{ $each->ten_vat_tu }}</option>
				@endforeach
			</select>
			@if ($errors->has('ma_vat_tu'))
				<span class="validate_msg">{{ $errors->first('ma_vat_tu') }}</span>
			@endif
			<br>
			Số lượng / phòng
			<br>
			<input type="number" name="so_luong_vat_tu" placeholder="(VD: 1)" value="{{ old('so_luong_vat_tu') }}">
			@if ($errors->has('so_luong_vat_tu'))
				<span class="validate_msg">{{ $errors->first('so_luong_vat_tu') }}</span>
			@endif
			<br>
			<button class="form_button_success">Thêm</button>
		</form>
	</div>
</div>
@endsection