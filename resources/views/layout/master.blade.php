<!DOCTYPE html>
<html>
<head>
	<title>@yield('title') - Quản lý khách sạn</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/select2.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery-ui.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.ui.datepicker.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/fullcalendar/main.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/custom_tooltip.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/chart.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/general.css') }}">
	@stack('css')
</head>
<body>
<div class="all">

	{{-- Sticky Shortcuts --}}
	<div>
		<button class="go_to_top" title="Lên đầu trang">↑</button>
		<button class="toggle_admin_modal" title="Cá nhân">
			<img src="{{ asset('Images/interface-BW/png/016-user.png') }}">
		</button>
	</div>

	{{-- Menu - Left tab --}}
	<div class="menu">
		<div class="div_title">
			<button class="navigation">←</button>
			<div class="title">
				<a href="{{ route('welcome') }}">Quản lý khách sạn</a>
			</div>
		</div>
		<div class="menu_h1 collapsible">
			<span class="menu_text_h1">
				Phòng
			</span>
		</div>
		<div class="menu_group_h2">
			<div class="menu_h2">
				<a href="{{ route('dat_phong.index') }}">
					<div class="menu_mini_h2">
						Đặt phòng
					</div>
				</a>
			</div>
			<div class="menu_h2">
				<a href="{{ route('loai_phong.index') }}">
					<div class="menu_mini_h2">
						Danh sách loại phòng
					</div>
				</a>
			</div>
			<div class="menu_h2">
				<a href="{{ route('phong.index') }}">
					<div class="menu_mini_h2">
						Danh sách phòng
					</div>
				</a>
			</div>
		</div>
		<div class="menu_h1 collapsible">
			<span class="menu_text_h1">
				Dịch vụ
			</span>
		</div>
		<div class="menu_group_h2">
			<div class="menu_h2">
				<a href="{{ route('dat_dich_vu.index') }}">
					<div class="menu_mini_h2">
						Đặt dịch vụ
					</div>
				</a>
			</div>
			<div class="menu_h2">
				<a href="{{ route('dich_vu.index') }}">
					<div class="menu_mini_h2">
						Danh sách dịch vụ
					</div>
				</a>
			</div>
		</div>
		<div class="menu_h1">
			<a href="{{ route('khach_hang.index') }}">
				<div class="menu_mini_h1">
					Khách hàng
				</div>
			</a>
		</div>
		<div class="menu_h1 collapsible">
			<span class="menu_text_h1">
				Vật tư
			</span>
		</div>
		<div class="menu_group_h2">
			<div class="menu_h2">
				<a href="{{ route('vat_tu.index') }}">
					<div class="menu_mini_h2">
						Danh sách vật tư
					</div>
				</a>
			</div>
			<div class="menu_h2">
				<a href="{{ route('kho.index') }}">
					<div class="menu_mini_h2">
						Kho
					</div>
				</a>
			</div>
		</div>
		<div class="menu_h1">
			<a href="{{ route('hoa_don.index') }}">
				<div class="menu_mini_h1">
					Hóa đơn
				</div>
			</a>
		</div>
		<div class="menu_h1">
			<a href="{{ route('thong_ke') }}">
				<div class="menu_mini_h1">
					Thống kê
				</div>
			</a>
		</div>
	</div>

	{{-- Overlay --}}
	<div class="overlay"></div>
	<div class="overlay_menu"></div>
	<div class="overlay_form"></div>

	{{-- Form Popup --}}
	<div class="form_popup_container">
		@yield('form_popup')
	</div>

	{{-- Admin Modal - Right tab --}}
	<div class="modal_admin">
		<div class="modal_admin_top">
			<div class="admin_avatar">
				<img src="{{ url('Images/interface-color/png/051-avatar.png') }}">
			</div>
			<div class="admin_info">
				<span>{{ Session::get('ho_ten') }}</span>
				<br>
				<span>{{ Session::get('email') }}</span>
			</div>
			<div class="admin_action">
				<div class="admin_action_button">
					<a href="{{ route('change_password') }}">
						Đổi mật khẩu
					</a>
				</div>
				<div class="admin_action_button">
					<a href="{{ route('logout') }}">
						Đăng xuất
					</a>
				</div>
			</div>
		</div>
	</div>

	{{-- Main content --}}
	<div class="content_big">
		<div class="header">
			{{-- Menu (Left Tab) button trigger (Mobile) --}}
			<div class="header_menu_toggle">
				<img src="{{ url('Images/interface-color/png/003-show.png') }}">
			</div>
			{{-- Username display, Admin tab (Right Tab) button trigger --}}
			<div class="header_username">
				<span>Chào {{ Session::get('ho_ten') }}</span>
			</div>
		</div>
		<div class="sub_header">
			{{-- Navigation pane --}}
			<div class="header_navigation">
				@yield('navigation')
			</div>
			{{-- Search pane --}}
			<div class="search">
				@yield('search')
			</div>
		</div>
		<div class="content">
			{{-- Success / Fail messages returned from Session --}}
			@if (Session::has('success'))
				<div class="success">
					<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
					{{ Session::get('success') }}
				</div>
			@elseif (Session::has('alert'))
				<div class="alert">
					<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
					{{ Session::get('alert') }}
				</div>
			@endif
			{{-- Success / Fail messages returned from Validation --}}
			@if ($errors->any())
			    <div class="alert">
			    	<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
			    	Thao tác thất bại: {{ $errors->first() }}
			    </div>
			@endif
			{{-- Seprate noti div --}}
			<div id="noti"></div>
			@yield('content')
		</div>
	</div>
</div>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery-ui.js') }}"></script>
<script src="{{ asset('js/select2.min.js') }}"></script>
<script src="{{ asset('js/fullcalendar/main.js') }}"></script>
<script src="{{ asset('js/fullcalendar/locales-all.js') }}"></script>
<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="{{ asset('js/tooltip.min.js') }}"></script>
<script src="{{ asset('js/chart.min.js') }}"></script>
<script src="{{ asset('js/chart.bundle.min.js') }}"></script>
<script src="{{ asset('js/general.js') }}"></script>
@stack('script')
</body>
</html>