@if ($paginator->hasPages())
    <!-- Pagination -->
    <div class="pagination">
        {{-- Page Link List --}}
        <ul class="pagination">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <span>
                    <li class="disabled prev_next_pagination">
                        Trước
                    </li>
                </span>
            @else
                <a href="{{ $paginator->previousPageUrl() }}">
                    <li class="prev_next_pagination">
                        <span>Trước</span>
                    </li>
                </a>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <span>
                                <li class="active">
                                    {{ $page }}
                                </li>
                            </span>
                        @elseif ($page == $paginator->currentPage() - 1 || $page == $paginator->currentPage() + 1 || $page == 1 || $page == $paginator->lastPage())
                            <a href="{{ $url }}">
                                <li>
                                    {{ $page }}
                                </li>
                            </a>
                        @elseif ($page == $paginator->lastPage() - 1 || $page == 2)
                            <span>
                                <li class="disabled">
                                    ...
                                </li>
                            </span>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class="prev_next_pagination">
                    <span>
                        <a href="{{ $paginator->nextPageUrl() }}">
                            Sau
                        </a>
                    </span>
                </li>
            @else
                <span>
                    <li class="disabled prev_next_pagination">
                        Sau
                    </li>
                </span>
            @endif
        </ul>
        {{-- End Page Link List --}}

        {{-- Pagination Info Right --}}
        <div class="pagination_info_right">
            <p>
                {{ $paginator->lastItem() - $paginator->count() + 1 }} - {{ $paginator->lastItem() }} trên {{ $paginator->total() }} kết quả
            </p>
        </div>
        {{-- End Pagination Info Right --}}
    </div>
    <!-- Pagination -->
@endif