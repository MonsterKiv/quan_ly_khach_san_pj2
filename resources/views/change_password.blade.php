@extends('layout.master')

@section('title', 'Đổi mật khẩu')

@section('navigation')
Đổi mật khẩu
@endsection

@section('search')
@endsection

@section('content')
<div class="grid_1">
	<div>
		<form action="{{ route('update_admin_password') }}" method="POST">
			{{ csrf_field() }}
			Mật khẩu cũ:
			<br>
			<input type="password" name="mat_khau">
			<br>
			Mật khẩu mới:
			<br>
			<input type="password" name="mat_khau_moi">
			<br>
			Nhập lại mật khẩu mới:
			<br>
			<input type="password" name="nhap_lai_mat_khau_moi">
			<br>
			<button>Đổi mật khẩu</button>
		</form>
	</div>
</div>
@endsection