@extends('layout.master')

@section('title', 'Khách hàng')

@section('navigation')
Khách hàng
@endsection

@section('search')
@endsection

@section('form_popup')
<div class="form_popup" id="create_khach_hang">
	<div class="form_popup_header">
		<span class="form_popup_close_button">&times;</span>
		<span class="form_popup_title">Thêm khách hàng mới</span>
	</div>
	<div class="form_popup_content">
		<form action="{{ route('khach_hang.store') }}" method="POST">
		{{ csrf_field() }}
		<table class="form_popup_table">
			<tr>
				<td class="form_popup_td_label">Họ và tên</td>
				<td class="form_popup_td_input">
					<input type="text" name="ten_kh" placeholder="VD: Nguyễn Đình Ngân An" value="{{ old('ten_kh') }}">
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Ngày sinh</td>
				<td class="form_popup_td_input">
					<input type="date" name="ngay_sinh_kh" value="{{ old('ngay_sinh_kh') }}">
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Giới tính</td>
				<td class="form_popup_td_input">
					<select name="gioi_tinh_kh" id="gioi_tinh_kh">
						<option></option>
						<option value="0">Nam</option>
						<option value="1">Nữ</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Quốc tịch</td>
				<td class="form_popup_td_input">
					<input type="text" name="quoc_tich_kh" placeholder="VD: Việt Nam" value="{{ old('quoc_tich_kh') }}">
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Chứng minh thư / Hộ chiếu</td>
				<td class="form_popup_td_input">
					<input type="text" name="cmt_hc_kh" placeholder="VD: 001200000XXX" value="{{ old('cmt_hc_kh') }}">
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Email</td>
				<td class="form_popup_td_input">
					<input type="text" name="email_kh" placeholder="VD: test@demo.com" value="{{ old('email_kh') }}">
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Số điện thoại</td>
				<td class="form_popup_td_input">
					<input type="text" name="sdt_kh" placeholder="VD: 037337XXXX" value="{{ old('sdt_kh') }}">
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<button class="form_button_success">Thêm</button>
				</td>
			</tr>
		</table>
		</form>
	</div>
</div>
<div class="form_popup" id="edit_khach_hang">
	<div class="form_popup_header">
		<span class="form_popup_close_button">&times;</span>
		<span class="form_popup_title">Thêm khách hàng mới</span>
	</div>
	<div class="form_popup_content">
		<form method="POST">
		{{ method_field('PUT') }}
		{{ csrf_field() }}
		<table class="form_popup_table">
			<tr>
				<td class="form_popup_td_label">Họ và tên</td>
				<td class="form_popup_td_input">
					<input type="text" name="ten_kh" placeholder="VD: Nguyễn Đình Ngân An" id="ten_kh_edit">
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Ngày sinh</td>
				<td class="form_popup_td_input">
					<input type="date" name="ngay_sinh_kh" id="ngay_sinh_kh_edit">
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Giới tính</td>
				<td class="form_popup_td_input">
					<select name="gioi_tinh_kh" id="gioi_tinh_kh_edit">
						<option value="0">Nam</option>
						<option value="1">Nữ</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Quốc tịch</td>
				<td class="form_popup_td_input">
					<input type="text" name="quoc_tich_kh" placeholder="VD: Việt Nam" id="quoc_tich_kh_edit">
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Chứng minh thư / Hộ chiếu</td>
				<td class="form_popup_td_input">
					<input type="text" name="cmt_hc_kh" placeholder="VD: 001200000XXX" id="cmt_hc_kh_edit">
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Email</td>
				<td class="form_popup_td_input">
					<input type="text" name="email_kh" placeholder="VD: test@demo.com" id="email_kh_edit">
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Số điện thoại</td>
				<td class="form_popup_td_input">
					<input type="text" name="sdt_kh" placeholder="VD: 037337XXXX" id="sdt_kh_edit">
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<button class="form_button_success">Sửa</button>
				</td>
			</tr>
		</table>
		</form>
	</div>
</div>
@endsection

@section('content')
<div class="grid_1">
	<div class="grid_content">
		<a class="form_button_success open_popup_form" id="button_create_khach_hang">Thêm khách hàng</a>
		<table class="form_table">
			<tr>
				<th>Tên khách hàng</th>
				<th>Ngày sinh</th>
				<th>Giới tính</th>
				<th>Quốc tịch</th>
				<th>Chứng minh thư / hộ chiếu</th>
				<th>Email</th>
				<th>Số điện thoại</th>
				<th>Điều chỉnh</th>
			</tr>
			@foreach ($array_khach_hang as $each)
				<tr>
					<td>
						<p>
							{{ $each->ten_kh }}
						</p>
					</td>
					<td>
						<p>
							{{ $each->ngaySinhFormat() }}
						</p>
					</td>
					<td>
						<p>
							{{ $each->getGioiTinh() }}
						</p>
					</td>
					<td>
						<p>
							{{ $each->quoc_tich_kh }}
						</p>
					</td>
					<td>
						<p>
							{{ $each->cmt_hc_kh }}
						</p>
					</td>
					<td>
						<p>
							{{ $each->email_kh }}
						</p>
					</td>
					<td>
						<p>
							{{ $each->sdt_kh }}
						</p>
					</td>
					<td><a class="form_button_warning open_popup_form button_edit_khach_hang" data-url="{{ route('khach_hang.edit', ['ma' => $each->ma_kh]) }}" data-url_action="{{ route('khach_hang.update',['ma' => $each->ma_kh]) }}">Sửa</a></td>
					{{-- <td>
						<form action="{{ route('khach_hang.destroy',['ma' => $each->ma_kh]) }}" method="POST">
							{{ method_field('DELETE') }}
							{{ csrf_field() }}
							<button>Xóa</button>
						</form>
					</td> --}}
				</tr>
			@endforeach
		</table>
	</div>
</div>
@endsection

@push('script')
<script type="text/javascript">
	$(document).ready(function() {
		/* Active Select2 */
    	$('.search_item').select2({
    		width: 'resolve'
    	});
    	$('#gioi_tinh_kh').select2({
    		placeholder: 'Chọn giới tính'
    	});
    	$('#gioi_tinh_kh_edit').select2();

    	/* Open Form Modal */
    	$('#button_create_khach_hang').click(function(event) {
    		$('#create_khach_hang').show();
    	});
    	$('.button_edit_khach_hang').click(function(event) {
    		var url = $(this).data('url');
			var url_action = $(this).data('url_action');
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				$('#edit_khach_hang div form').attr('action', url_action);
				$('#ten_kh_edit').val(response.khach_hang.ten_kh);
				$('#ngay_sinh_kh_edit').val(response.khach_hang.ngay_sinh_kh);
				$('#gioi_tinh_kh_edit option').each(function(index, el) {
					if ($(this).val() == response.khach_hang.gioi_tinh_kh) {
						$('#gioi_tinh_kh_edit').val($(this).val()).trigger('change');
					}
				});
				$('#quoc_tich_kh_edit').val(response.khach_hang.quoc_tich_kh);
				$('#cmt_hc_kh_edit').val(response.khach_hang.cmt_hc_kh);
				$('#email_kh_edit').val(response.khach_hang.email_kh);
				$('#sdt_kh_edit').val(response.khach_hang.sdt_kh);
				$('#edit_khach_hang').show();
			})
			.fail(function() {
				console.log("error");
			});
    	});
    });
</script>
@endpush