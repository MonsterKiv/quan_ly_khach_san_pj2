@extends('layout.master')

@section('title', 'Thêm khách hàng')

@section('navigation')
<a href="{{ route('khach_hang.index') }}">Khách hàng</a> → Thêm khách hàng
@endsection

@section('search')
@endsection

@section('content')
<div class="grid_1">
	<div>
		Điền thông tin của khách hàng:
		<form id="formId" action="{{ route('khach_hang.store') }}" method="POST">
			{{ csrf_field() }}
			Họ và tên
			<br>
			<input type="text" name="ten_kh" value="{{ old('ten_kh') }}">
			@if ($errors->has('ten_kh'))
				<span class="validate_msg">{{ $errors->first('ten_kh') }}</span>
			@endif
			<br>
			Ngày sinh
			<br>
			<input type="date" name="ngay_sinh_kh" value="{{ old('ngay_sinh_kh') }}">
			@if ($errors->has('ngay_sinh_kh'))
				<span class="validate_msg">{{ $errors->first('ngay_sinh_kh') }}</span>
			@endif
			<br>
			Giới tính
			<br>
			<select name="gioi_tinh_kh" selected="">
				<option value="0" @if (old('gioi_tinh_kh') == 0) selected @endif>Nam</option>
				<option value="1" @if (old('gioi_tinh_kh') == 1) selected @endif>Nữ</option>
			</select>
			@if ($errors->has('gioi_tinh_kh'))
				<span class="validate_msg">{{ $errors->first('gioi_tinh_kh') }}</span>
			@endif
			<br>
			Quốc tịch
			<br>
			<input type="text" name="quoc_tich_kh" value="{{ old('quoc_tich_kh') }}">
			@if ($errors->has('quoc_tich_kh'))
				<span class="validate_msg">{{ $errors->first('quoc_tich_kh') }}</span>
			@endif
			<br>
			Chứng minh thư / Hộ chiếu
			<br>
			<input type="text" name="cmt_hc_kh" value="{{ old('cmt_hc_kh') }}">
			@if ($errors->has('cmt_hc_kh'))
				<span class="validate_msg">{{ $errors->first('cmt_hc_kh') }}</span>
			@endif
			<br>
			Email
			<br>
			<input type="text" name="email_kh" value="{{ old('email_kh') }}">
			@if ($errors->has('email_kh'))
				<span class="validate_msg">{{ $errors->first('email_kh') }}</span>
			@endif
			<br>
			Số điện thoại
			<br>
			<input type="text" name="sdt_kh" value="{{ old('sdt_kh') }}">
			@if ($errors->has('sdt_kh'))
				<span class="validate_msg">{{ $errors->first('sdt_kh') }}</span>
			@endif
			<br>
			<button id="button_submit" class="form_button_success">Thêm</button>
		</form>
	</div>
</div>
@endsection

{{-- @push('script')
<script type="text/javascript">
$(document).ready(function() {
	var url = '{{ route('khach_hang.store') }}';
	ajaxForm(url);
});
</script>
@endpush --}}