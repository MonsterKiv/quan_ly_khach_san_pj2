@extends('layout.master')

@section('title', 'Thay đổi thông tin khách hàng')

@section('navigation')
<a href="{{ route('khach_hang.index') }}">Khách hàng</a> → Thay đổi thông tin
@endsection

@section('search')
@endsection

@section('content')
<div class="grid_1">
	<div>
		Thay đổi thông tin của khách hàng:
		<form id="formId">
			{{ method_field('PUT') }}
			{{ csrf_field() }}
			Họ và tên
			<br>
			<input type="text" name="ten_kh" value="{{ $khach_hang->ten_kh }}">
			{{-- @if ($errors->has('ten_kh'))
				<span class="validate_msg">{{ $errors->first('ten_kh') }}</span>
			@endif --}}
			<br>
			Ngày sinh
			<br>
			<input type="date" name="ngay_sinh_kh" value="{{ $khach_hang->ngay_sinh_kh }}">
			{{-- @if ($errors->has('ngay_sinh_kh'))
				<span class="validate_msg">{{ $errors->first('ngay_sinh_kh') }}</span>
			@endif --}}
			<br>
			Giới tính
			<br>
			<select name="gioi_tinh_kh">
				@if ($khach_hang->gioi_tinh_kh == 0)
					<option value="0" selected>Nam</option>
					<option value="1">Nữ</option>
				@else
					<option value="0">Nam</option>
					<option value="1" selected>Nữ</option>
				@endif
			</select>
			{{-- @if ($errors->has('gioi_tinh_kh'))
				<span class="validate_msg">{{ $errors->first('gioi_tinh_kh') }}</span>
			@endif --}}
			<br>
			Quốc tịch
			<br>
			<input type="text" name="quoc_tich_kh" value="{{ $khach_hang->quoc_tich_kh }}">
			{{-- @if ($errors->has('quoc_tich_kh'))
				<span class="validate_msg">{{ $errors->first('quoc_tich_kh') }}</span>
			@endif --}}
			<br>
			Chứng minh thư / Hộ chiếu
			<br>
			<input type="text" name="cmt_hc_kh" value="{{ $khach_hang->cmt_hc_kh }}">
			{{-- @if ($errors->has('cmt_hc_kh'))
				<span class="validate_msg">{{ $errors->first('cmt_hc_kh') }}</span>
			@endif --}}
			<br>
			Email
			<br>
			<input type="text" name="email_kh" value="{{ $khach_hang->email_kh }}">
			{{-- @if ($errors->has('email_kh'))
				<span class="validate_msg">{{ $errors->first('email_kh') }}</span>
			@endif --}}
			<br>
			Số điện thoại
			<br>
			<input type="text" name="sdt_kh" value="{{ $khach_hang->sdt_kh }}">
			{{-- @if ($errors->has('sdt_kh'))
				<span class="validate_msg">{{ $errors->first('sdt_kh') }}</span>
			@endif --}}
			<br>
			<button id="button_submit" class="form_button_success">Chỉnh sửa</button>
		</form>
	</div>
</div>
@endsection

@push('script')
<script type="text/javascript">
$(document).ready(function() {
	var url = '{{ route('khach_hang.update',['ma' => $khach_hang->ma_kh]) }}';
	ajaxForm(url);
});
</script>
@endpush