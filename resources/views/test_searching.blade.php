@extends('layout.master')

@section('title', 'Test tìm kiếm phòng')

@push('css')
<style type="text/css">
	#calendar {
		padding: 10px;
	}
	.js-example-basic-multiple {
		width: 150px;
	}
</style>
@endpush

@section('search')
<form action="" method="GET">
	<select class="js-example-basic-multiple" id="test_select">
		<option></option>
		<option value="1">Test 1</option>
		<option value="2">Test 2</option>
		<option value="3">Test 3</option>
		<option value="4">Test 4</option>
		<option value="5">101</option>
		<option value="6">201</option>
		<option value="8">202</option>
		<option value="9">301</option>
		<option value="10">302</option>
	</select>
	<button>Tìm kiếm</button>
</form>
@endsection

@section('content')
<div class="grid_1">
	<div class="grid_content">
		<table border="1px solid black" width="50%">
			<tr>
				<th>Mã phòng</th>
				<th>Mã loai phòng</th>
				<th>Xem chi tiết</th>
			</tr>
			@foreach ($array_phong as $each)
				<tr>
					<td>{{ $each->ma_phong }}</td>
					<td>{{ $each->ma_loai_phong }}</td>
					<td><input type="radio" name="ma_phong" value="{{ $each->ma_phong }}" class="test_radio"></td>
				</tr>
			@endforeach
		</table>
	</div>
	<div class="grid_content">
		<div id="calendar">
			Hãy chọn phòng để hiển thị lịch đặt
		</div>
	</div>
	<div class="container">
		<div class="invoice">
			<header>
				<section>
					<h1>Nguyễn Đình Ngân An</h1>
					<span>30/02/2020 - Chưa thanh toán</span>
				</section>

				<section>
					<span>89289</span>
				</section>
			</header>

			<main>
				<section>
					<span>Phòng</span>
				</section>

				<section>
					<span>Tên phòng</span>
					<span>Giá phòng / ngày</span>
					<span>Thời gian thuê</span>
					<span>Thành tiền</span>
				</section>

				<section>
					<figure>
						<span><strong>Espresso</strong> (large)</span>
						<span>2.90</span>
						<span>1 ngày 0 giờ</span>
						<span>2.90</span>
					</figure>

					<figure>
						<span><strong>Cappuccino</strong> (small)</span>
						<span>3.50</span>
						<span>2 ngày 0 giờ</span>
						<span>7.00</span>
					</figure>
				</section>

				<section>
					<span>Dịch vụ</span>
				</section>

				<section>
					<span>Tên dịch vụ</span>
					<span>Đơn giá</span>
					<span>Số lượng</span>
					<span>Thành tiền</span>
				</section>

				<section>
					<figure>
						<span><strong>Espresso</strong> (large)</span>
						<span>2.90</span>
						<span>1</span>
						<span>2.90</span>
					</figure>

					<figure>
						<span><strong>Cappuccino</strong> (small)</span>
						<span>3.50</span>
						<span>2</span>
						<span>7.00</span>
					</figure>
				</section>

				<section>
					<span>Total</span>
					<span>9.90</span>
				</section>
			</main>

			<footer>
				<a href="#0">Xem chi tiết</a>
				<a href="#0">Thanh toán</a>
			</footer>
		</div>
	</div>
</div>
@endsection

@push('script')
<script type="text/javascript">
	$(document).ready(function() {
    	$('.js-example-basic-multiple').select2({
    		width: 'resolve',
    		placeholder: "Chọn phòng"
		});
        $('.test_radio').change(function(event) {
			var ma_phong = $(this).val();
			$('#calendar').empty();
			$.ajax({
				url: '{{ route('ajax.get_hoa_don_phong') }}',
				type: 'GET',
				dataType: 'JSON',
				data: {ma_phong: ma_phong},
			})
			.done(function(response) {
				var calendarEl = document.getElementById('calendar');
		        var calendar = new FullCalendar.Calendar(calendarEl, {
		        	initialView: 'dayGridMonth',
		        	initialDate: '2020-07-07',
		        	locale: 'vi',
		        	height: 700,
				    headerToolbar: {
				      left: 'prev,next today',
				      center: 'title',
				      right: 'dayGridMonth,timeGridWeek,listWeek'
				    },
		        	displayEventEnd: true,
		        	titleFormat: {
		        		year: 'numeric',
		        		month: '2-digit',
		        		day: '2-digit'
		        	},
		        	views: {
		        		dayGrid: {
				        	eventTimeFormat: {
				        		month: 'numeric',
				        		day: 'numeric',
					        	hour: 'numeric',
								minute: '2-digit',
								meridiem: false,
								omitZeroMinute: false
				        	}
		        		}
		        	},
		        });
				calendar.addEventSource(response);
                calendar.refetchEvents();
        		calendar.render();
			})
			.fail(function() {
				console.log("error");
			});
		});
    });
</script>
@endpush