@extends('layout.master')

@section('title', 'Thêm dịch vụ')

@section('navigation')
<a href="{{ route('dich_vu.index') }}">Dịch vụ</a> → Thêm dịch vụ
@endsection

@section('search')
@endsection

@section('content')
<div class="grid_1">
	<div>
		Điền thông tin của dịch vụ:
		<form action="{{ route('dich_vu.store') }}" method="POST">
			{{ csrf_field() }}
			Tên dịch vụ
			<br>
			<input type="text" name="ten_dich_vu" value="{{ old('ten_dich_vu') }}">
			@if ($errors->has('ten_dich_vu'))
				<span class="validate_msg">{{ $errors->first('ten_dich_vu') }}</span>
			@endif
			<br>
			Giá dịch vụ
			<br>
			<input type="text" name="gia_dich_vu" value="{{ old('gia_dich_vu') }}">
			@if ($errors->has('gia_dich_vu'))
				<span class="validate_msg">{{ $errors->first('gia_dich_vu') }}</span>
			@endif
			<br>
			Đơn vị
			<br>
			<input type="text" name="don_vi_dich_vu" value="{{ old('don_vi_dich_vu') }}">
			@if ($errors->has('don_vi_dich_vu'))
				<span class="validate_msg">{{ $errors->first('don_vi_dich_vu') }}</span>
			@endif
			<br>
			Trạng thái
			<br>
			<select name="trang_thai_dich_vu">
				<option value="1">Sẵn sàng</option>
				<option value="0">Hết dịch vụ</option>
				<option value="2">Tạm ngưng</option>
			</select>
			@if ($errors->has('trang_thai_dich_vu'))
				<span class="validate_msg">{{ $errors->first('trang_thai_dich_vu') }}</span>
			@endif
			<br>
			<button class="form_button_success">Thêm</button>
		</form>
	</div>
</div>
@endsection