@extends('layout.master')

@section('title', 'Chỉnh sửa dịch vụ')

@section('navigation')
<a href="{{ route('dich_vu.index') }}">Dịch vụ</a> → Chỉnh sửa dịch vụ
@endsection

@section('search')
@endsection

@section('content')
<div class="grid_1">
	<div>
		Chỉnh sửa dịch vụ:
		<form action="{{ route('dich_vu.update', ['ma_dich_vu' => $dich_vu->ma_dich_vu]) }}" method="POST">
			{{ method_field('PUT') }}
			{{ csrf_field() }}
			Tên dịch vụ
			<br>
			<input type="text" name="ten_dich_vu" value="{{ $dich_vu->ten_dich_vu }}">
			@if ($errors->has('ten_dich_vu'))
				<span class="validate_msg">{{ $errors->first('ten_dich_vu') }}</span>
			@endif
			<br>
			Giá dịch vụ (VND)
			<br>
			<input type="text" name="gia_dich_vu" value="{{ $dich_vu->gia_dich_vu }}">
			@if ($errors->has('gia_dich_vu'))
				<span class="validate_msg">{{ $errors->first('gia_dich_vu') }}</span>
			@endif
			<br>
			Đơn vị tính
			<br>
			<input type="text" name="don_vi_dich_vu" value="{{ $dich_vu->don_vi_dich_vu }}">
			@if ($errors->has('don_vi_dich_vu'))
				<span class="validate_msg">{{ $errors->first('don_vi_dich_vu') }}</span>
			@endif
			<br>
			<button id="button_submit" class="form_button_success">Thay đổi</button>
		</form>
	</div>
</div>
@endsection