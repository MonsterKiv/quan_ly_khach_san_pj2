@extends('layout.master')

@section('title', 'Dịch vụ')

@section('navigation')
Dịch vụ
@endsection

@section('search')
<form action="{{ route('dich_vu.index') }}" method="GET">
	<select class="search_item" name="trang_thai_dich_vu[]" multiple="multiple" data-placeholder="Chọn tình trạng" style="width: 150px">
		@foreach ($array_trang_thai_dich_vu as $each)
			@if (isset($trang_thai_dich_vu) && in_array($each->ma_trang_thai_dich_vu, $trang_thai_dich_vu))
				<option value="{{ $each->ma_trang_thai_dich_vu }}" selected>{{ $each->ten_trang_thai_dich_vu }}</option>
			@else
				<option value="{{ $each->ma_trang_thai_dich_vu }}">{{ $each->ten_trang_thai_dich_vu }}</option>
			@endif
		@endforeach
	</select>
	<input type="text" name="ten_dich_vu" placeholder="Tên dịch vụ" value="{{ $ten_dich_vu }}" style="width: 120px;">
	<button class="form_button_success">Tìm kiếm</button>
</form>
@endsection

@section('form_popup')
<div class="form_popup" id="create_dich_vu">
	<div class="form_popup_header">
		<span class="form_popup_close_button">&times;</span>
		<span class="form_popup_title">Thêm dịch vụ mới</span>
	</div>
	<div class="form_popup_content">
		<form action="{{ route('dich_vu.store') }}" method="POST">
		{{ csrf_field() }}
		<table class="form_popup_table">
			<tr>
				<td class="form_popup_td_label">Tên dịch vụ</td>
				<td class="form_popup_td_input">
					<input type="text" name="ten_dich_vu" placeholder="VD: Bò Húc" value="{{ old('ten_dich_vu') }}">
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Giá dịch vụ (VND)</td>
				<td class="form_popup_td_input">
					<input type="number" name="gia_dich_vu" placeholder="VD: 10000" value="{{ old('gia_dich_vu') }}">
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Đơn vị tính</td>
				<td class="form_popup_td_input">
					<input type="text" name="don_vi_dich_vu" placeholder="VD: Lon" value="{{ old('don_vi_dich_vu') }}">
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Trạng thái</td>
				<td class="form_popup_td_input">
					<select name="trang_thai_dich_vu" id="trang_thai_dich_vu">
						<option></option>
						@foreach ($array_trang_thai_dich_vu as $each)
							<option value="{{ $each->ma_trang_thai_dich_vu }}">{{ $each->ten_trang_thai_dich_vu }}</option>
						@endforeach
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<button class="form_button_success">Thêm</button>
				</td>
			</tr>
		</table>
		</form>
	</div>
</div>
<div class="form_popup" id="edit_dich_vu">
	<div class="form_popup_header">
		<span class="form_popup_close_button">&times;</span>
		<span class="form_popup_title">Sửa đổi dịch vụ</span>
	</div>
	<div class="form_popup_content">
		<form method="POST">
			{{ method_field('PUT') }}
			{{ csrf_field() }}
		<table class="form_popup_table">
			<tr>
				<td class="form_popup_td_label">Tên dịch vụ</td>
				<td class="form_popup_td_input">
					<input type="text" name="ten_dich_vu" placeholder="VD: Bò Húc" id="ten_dich_vu_edit">
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Giá dịch vụ (VND)</td>
				<td class="form_popup_td_input">
					<input type="number" name="gia_dich_vu" placeholder="VD: 10000" id="gia_dich_vu_edit">
				</td>
			</tr>
			<tr>
				<td class="form_popup_td_label">Đơn vị tính</td>
				<td class="form_popup_td_input">
					<input type="text" name="don_vi_dich_vu" placeholder="VD: Lon" id="don_vi_dich_vu_edit">
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<button class="form_button_success">Sửa</button>
				</td>
			</tr>
		</table>
		</form>
	</div>
</div>
@endsection

@section('content')
<div class="grid_1">
	<div class="grid_content">
		<a class="form_button_success open_popup_form" id="button_create_dich_vu">Thêm dịch vụ</a>
		<table class="form_table">
			<tr>
				<th>Tên dịch vụ</th>
				<th>Giá dịch vụ (VND)</th>
				<th>Đơn vị tính</th>
				<th>Trạng thái</th>
				<th colspan="3">Thao tác</th>
			</tr>
			@if ($array_dich_vu->isEmpty())
				<tr>
					<td colspan="5">Không có dữ liệu</td>
				</tr>
			@else
			@foreach ($array_dich_vu as $each)
				<tr>
					<td>
						<p>
							{{ $each->ten_dich_vu }}
						</p>
					</td>
					<td>
						<p>
							{{ number_format($each->gia_dich_vu) }}
						</p>
					</td>
					<td>
						<p>
							{{ $each->don_vi_dich_vu }}
						</p>
					</td>
					<td>
						<p>
							{{ $each->ten_trang_thai_dich_vu }}
						</p>
					</td>
					<td>
						@for ($i = 1; $i < 4; $i++)
							@if ($i != $each->ma_trang_thai_dich_vu)
								<form action="{{ route('dich_vu.update', ['ma_dich_vu' => $each->ma_dich_vu]) }}" method="POST">
									{{ method_field('PUT') }}
									{{ csrf_field() }}
									<input type="hidden" name="ma_trang_thai_dich_vu" value="{{ $i }}">
									<input type="hidden" name="ten_dich_vu" value="{{ $each->ten_dich_vu }}">
									<input type="hidden" name="gia_dich_vu" value="{{ $each->gia_dich_vu }}">
									<input type="hidden" name="don_vi_dich_vu" value="{{ $each->don_vi_dich_vu }}">
									<button class="form_button_alternative">{{ $each->getTrangThaiDichVuForm($i) }}</button>
								</form>
							@endif
						@endfor
					</td>
					<td><a class="form_button_warning open_popup_form button_edit_dich_vu" data-url="{{ route('dich_vu.edit', ['ma_dich_vu' => $each->ma_dich_vu]) }}" data-url_action="{{ route('dich_vu.update', ['ma_dich_vu' => $each->ma_dich_vu]) }}">Sửa</a></td>
					<td>
						<button class="form_button_danger button_trigger">Xóa</button>
						<div style="display: none;">
							<form>
								{{ method_field('DELETE') }}
								{{ csrf_field() }}
								Bạn chắc chứ?
								<br>
								<button class="form_button_danger" formaction="{{ route('dich_vu.destroy', ['ma_dich_vu' => $each->ma_dich_vu]) }}" formmethod="POST">Xóa</button>
								<button class="form_button_return button_return">Quay lại</button>
							</form>
						</div>
					</td>
				</tr>
			@endforeach
			@endif
		</table>
		{{ $array_dich_vu->appends(['trang_thai_dich_vu' => $trang_thai_dich_vu, 'ten_dich_vu' => $ten_dich_vu])->render('layout.custom_paginate') }}
	</div>
</div>
@endsection

@push('script')
<script type="text/javascript">
	$(document).ready(function() {
		/* Active Select2 */
    	$('.search_item').select2({
    		width: 'resolve'
    	});
    	$('#trang_thai_dich_vu').select2({
    		placeholder: 'Chọn trạng thái'
    	});

    	/* Open form modal */
    	$('#button_create_dich_vu').click(function(event) {
			$('#create_dich_vu').show();
		});
		$('.button_edit_dich_vu').click(function(event) {
			var url = $(this).data('url');
			var url_action = $(this).data('url_action');
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(response) {
				$('#edit_dich_vu div form').attr('action', url_action);
				$('#ten_dich_vu_edit').val(response.dich_vu.ten_dich_vu);
				$('#gia_dich_vu_edit').val(response.dich_vu.gia_dich_vu);
				$('#don_vi_dich_vu_edit').val(response.dich_vu.don_vi_dich_vu);
				$('#edit_dich_vu').show();
			})
			.fail(function() {
				console.log("error");
			});
		});
	});
</script>
@endpush