@extends('layout.master')

@section('title', 'Đặt dịch vụ')

@section('navigation')
<a href="{{ route('dich_vu.index') }}">Dịch vụ</a> → Đặt dịch vụ
@endsection

@section('search')
@endsection

@section('content')
<div class="grid_1">
	<div class="grid_content">
		<form action="{{ route('dat_dich_vu.store') }}" method="POST">
			{{ csrf_field() }}
			Chọn hóa đơn
			<br>
			<select name="ma_hoa_don" id="ma_hoa_don">
				<option></option>
				@foreach ($array_hoa_don as $each)
					<option value="{{ $each->ma_hoa_don }}">{{ $each->ten_kh }} - #{{ $each->ma_hoa_don }}</option>
				@endforeach
			</select>
			@if ($errors->has('ma_hoa_don'))
				<span class="validate_msg">{{ $errors->first('ma_hoa_don') }}</span>
			@endif
			<br>
			Chọn dịch vụ
			<br>
			<select name="ma_dich_vu[]" id="ma_dich_vu_select" class="select_item" style="width: 180px" data-placeholder="Dịch vụ" multiple="multiple">
				@foreach ($array_dich_vu as $each)
					<option value="{{ $each->ma_dich_vu }}">{{ $each->ten_dich_vu }} ({{ $each->don_vi_dich_vu }})</option>
				@endforeach
			</select>
			@if ($errors->has('ma_dich_vu'))
				<span class="validate_msg">{{ $errors->first('ma_dich_vu') }}</span>
			@endif
			<div id="target_don_vi" class="disable_grid">Hãy chọn dịch vụ để điền số lượng</div>
			{{-- <br>
			Số lượng
			<br>
			<input type="text" name="so_luong_dich_vu"> <span id="target_don_vi">đơn vị</span>
			<br> --}}
			@if ($errors->has('so_luong_dich_vu.*'))
				<span class="validate_msg">{{ $errors->first('so_luong_dich_vu.*') }}</span>
			@endif
			<button class="form_button_success">Đặt</button>
			<br>
			*Lưu ý: Thời gian lấy tại thời điểm đặt
		</form>
	</div>
</div>
@endsection

@push('script')
<script type="text/javascript">
$(document).ready(function() {
	$('.select_item').select2({
		width: 'resolve'
	});
	$('#ma_hoa_don').select2({
		placeholder: 'Chọn hóa đơn'
	});
	$("#ma_dich_vu_select").change(function () {
		var ma_dich_vu = $(this).val();
		$("#target_don_vi").html('');
		$.ajax({
			url: '{{ route('ajax.get_dich_vu') }}',
			type: 'GET',
			dataType: 'json',
			data: {ma_dich_vu: ma_dich_vu},
		})
		.done(function(response) {
			$(response).each(function() {
				$("#target_don_vi").append(`
					<table>
						<tr>
							<td>Dịch vụ</td>
							<td><input type="text" value="${this.ten_dich_vu}" disabled></td>
						</tr>
						<tr>
							<td>Số lượng</td>
							<td><input type="number" name="so_luong_dich_vu[${this.ma_dich_vu}]">${this.don_vi_dich_vu}</td>
						</tr>
					</table>
				`);
			});
		})
		.fail(function() {
			console.log("error");
		})
	});
});
</script>
@endpush