@extends('layout.master')

@section('title', 'Welcome')

@section('navigation')
Thống kê
@endsection

@push('css')
<style type="text/css">
	#doanh_thu_nam, #doanh_thu_thang{
		width: 110px;
	}
</style>
@endpush

@section('search')
	<div class="short_search">
		Chọn thời gian: 
		<select id="doanh_thu_thang">
			<option></option>
			@for ($i = 1; $i <= 12; $i++)
				<option value="@if ($i < 10)0{{ $i }}@else {{ $i }} @endif">Tháng {{ $i }}</option>
			@endfor
		</select>
		<select id="doanh_thu_nam">
			<option></option>
			@for ($i = $start_year; $i <= $current_year; $i++)
				<option value="{{ $i }}">{{ $i }}</option>
			@endfor
		</select>
		<button class="form_button_success" id="button_xem">Xem</button>
	</div>
@endsection

@section('content')
<div class="grid_3">
	<div class="grid_content">
		<canvas id="myChart" width="454" height="454"></canvas>
	</div>
	<div class="grid_content" style="grid-area: 1/2/1/4">
		<div class="preview_statistic">
			<h2 style="text-align: center;">Chọn một tháng để hiển thị số liệu nổi bật của tháng đó</h2>
			<h3>Thông tin hiển thị: </h3>
			<p>- Tổng doanh thu</p>
			<p>- Phòng được trong thời gian lâu nhất</p>
			<p>- Dịch vụ được sử dụng nhiều nhất</p>
			<p>- Số hóa đơn trong tháng</p>
			<p>- Số lượng khách quay lại khách sạn</p>
		</div>
		<div class="display_statistic" hidden>
			<h2 style="text-align: center;">Số liệu nổi bật trong tháng <span id="display_month"></span></h2>
			<p>Tổng doanh thu: <span id="tong_doanh_thu"></span></p>
			<p>Phòng được đặt trong thời gian lâu nhất: <span id="phong"></span> - <span id="tgdp"></span></p>
			<p>Dịch vụ được sử dụng nhiều nhất: <span id="ten_dich_vu"></span> - <span id="so_luong_dich_vu"></span> lần</p>
			<p>Tổng số hóa đơn trong tháng: <span id="so_hoa_don"></span></p>
			<p>Số khách hàng quay lại khách sạn trong tháng: <span id="so_hoa_don_dat_lai"></span></p>
		</div>
	</div>
</div>
@endsection

@push('script')
<script type="text/javascript">
	$(document).ready(function() {
		/* Select2 */
		$('#doanh_thu_thang').select2({
			width: 'resolve',
			placeholder: 'Chọn tháng'
		});
		$('#doanh_thu_nam').select2({
			width: 'resolve',
			placeholder: 'Chọn năm'
		});

		/* Display chart */
		var ctx = $('#myChart');
		var myChart = new Chart(ctx, {
		    type: 'doughnut',
		    data: {
			    datasets: [{
			        data: [0, 0],
		   			backgroundColor: [
		   				'rgba(255, 99, 132, 0.2)',
		   				'rgba(54, 162, 235, 0.2)'
	   				],
		            borderColor: [
		            	'rgba(255, 99, 132, 1)',
		            	'rgba(54, 162, 235, 1)'
		            ],
		            borderWidth: 1
			    }],
			    labels: [
			        'Phòng',
			        'Dịch vụ'
			    ],
			},
		    options: {
		    	cutoutPercentage: 40,
		    	title: {
		            display: true,
		            text: 'Doanh thu trong tháng',
		            fontSize: 22
		        },
		    }
		});

		/* Onclick get data and return */
		$('#button_xem').click(function(event) {
			var doanh_thu_nam = $('#doanh_thu_nam').val();
			var doanh_thu_thang = $('#doanh_thu_thang').val();
			$.ajax({
				url: '{{ route('ajax.get_thong_ke') }}',
				type: 'GET',
				dataType: 'json',
				data: {
					selected_nam: doanh_thu_nam,
					selected_thang: doanh_thu_thang
				},
			})
			.done(function(response) {
				removeChartData(myChart);
				addChartData(myChart, response);
				var tong_doanh_thu = 0;
				$.each(response, function(index, val) {
					tong_doanh_thu += val;
				});
				var tong_doanh_thu_formatted = numberFormat(tong_doanh_thu, 0, '.', ',');
				$('#tong_doanh_thu').html(`${tong_doanh_thu_formatted} đ`);
			})
			.fail(function() {
				// console.log("error");
			});

			$.ajax({
				url: '{{ route('ajax.get_thong_ke_noi_bat') }}',
				type: 'GET',
				dataType: 'json',
				data: {
					selected_nam: doanh_thu_nam,
					selected_thang: doanh_thu_thang
				},
			})
			.done(function(response) {
				$('.alert').remove();
				$('.preview_statistic').hide();
				$('.display_statistic').show();
				$('#display_month').html(`${doanh_thu_thang}/${doanh_thu_nam}`);
				$('#tgdp').html(response.tgdp);
				$('#phong').html(response.phong);
				$('#so_luong_dich_vu').html(response.so_luong_dich_vu);
				$('#ten_dich_vu').html(response.ten_dich_vu);
				$('#so_hoa_don').html(response.so_hoa_don);
				$('#so_hoa_don_dat_lai').html(response.so_hoa_don_dat_lai);
			})
			.fail(function() {
				$('.display_statistic').hide();
				$('.preview_statistic').show();
				$("#noti").append(`
						<div class="alert">
							<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
							Không có dữ liệu trong tháng đã chọn!
						</div>
					`);
			});
		});
	});

	function addChartData(chart, data) {
	    chart.data.datasets.forEach((dataset) => {
	    	$.each(data, function(index, val) {
	    		 dataset.data.push(val);
	    	});
	    });
	    chart.update();
	}

	function removeChartData(chart) {
	    chart.data.datasets.forEach((dataset) => {
	    	$.each(dataset.data, function(index, val) {
	    		dataset.data.pop();
	    	});
	    });
	    chart.update();
	}

	function numberFormat(number, decimals, dec_point, thousands_sep) {
        number = number.toFixed(decimals);

        var nstr = number.toString();
        nstr += '';
        x = nstr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? dec_point + x[1] : '';
        var rgx = /(\d+)(\d{3})/;

        while (rgx.test(x1))
            x1 = x1.replace(rgx, '$1' + thousands_sep + '$2');

        return x1 + x2;
    }
</script>
@endpush