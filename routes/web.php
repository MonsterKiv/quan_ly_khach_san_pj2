<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('','LoginController@view_login')->name('view_login');
Route::post('process_login','LoginController@process_login')->name('process_login');

Route::group(['middleware' => 'CheckLogin'], function(){
	Route::get('welcome','LoginController@welcome')->name('welcome');
	Route::get('logout','LoginController@logout')->name('logout');
	Route::get('change_password','LoginController@change_password')->name('change_password');
	Route::post('update_admin_password','LoginController@update_admin_password')->name('update_admin_password');

	Route::resource('khach_hang','KhachHangController');
	Route::resource('hoa_don','HoaDonController');
	Route::resource('dich_vu','DichVuController');
	Route::resource('dat_dich_vu','DatDichVuController');
	Route::resource('dat_phong','DatPhongController');
	Route::resource('vat_tu','VatTuController');
	Route::resource('kho','KhoController');
	Route::resource('loai_phong','LoaiPhongController');
	Route::resource('loai_phong/{ma_loai_phong}/vat_tu','VatTuLoaiPhongController')->names('vat_tu_loai_phong');
	Route::resource('phong','PhongController');
	Route::resource('phong/{ma_phong}/vat_tu','VatTuPhongController')->names('vat_tu_phong');

	Route::get('thong_ke','ThongKeController@index')->name('thong_ke');
});

Route::group(['as' => 'ajax.', 'prefix' => 'ajax'], function() {
	Route::get('get_dich_vu','AjaxController@getDichVu')->name('get_dich_vu');
	Route::get('get_phong','AjaxController@getPhong')->name('get_phong');
	Route::get('get_hoa_don_phong','AjaxController@getHoaDonPhong')->name('get_hoa_don_phong');
	Route::get('get_thong_ke','AjaxController@getThongKe')->name('get_thong_ke');
	Route::get('get_thong_ke_noi_bat','AjaxController@getThongKeNoiBat')->name('get_thong_ke_noi_bat');
});

Route::resource('test_searching', 'TestController');