<?php

use Illuminate\Database\Seeder;
use App\Models\DichVuModel;

class DichVuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        DichVuModel::insert([
			'ten_dich_vu'			=> $faker->name,
			'gia_dich_vu'			=> $faker->numberBetween(1000,100000),
			'don_vi_dich_vu'		=> $faker->name,
			'trang_thai_dich_vu'	=> $faker->numberBetween(1,2),
        ]);
    }
}
