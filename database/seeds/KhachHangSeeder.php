<?php

use Illuminate\Database\Seeder;
use App\Models\KhachHangModel;

class KhachHangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        KhachHangModel::insert([
        	'ten_kh' => $faker->name,
        	'ngay_sinh_kh' => $faker->dateTimeBetween('-30 years', '-20 years'),
        	'gioi_tinh_kh' => $faker->boolean(),
        	'quoc_tich_kh' => $faker->country(),
            'cmt_hc_kh' => e164PhoneNumber(),
        ]);
    }
}
