<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Kho extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kho', function (Blueprint $table){
            $table->unsignedInteger('ma_vat_tu');
            $table->integer('tong_so_luong_vat_tu');
            $table->integer('so_luong_vat_tu_hong');
            $table->primary(['ma_vat_tu']);
            $table->foreign('ma_vat_tu')->references('ma_vat_tu')->on('vat_tu')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kho');
    }
}
