<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HoaDonDichVu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hoa_don_dich_vu', function (Blueprint $table){
            $table->integer('ma_hoa_don')->unsigned();
            $table->integer('ma_dich_vu')->unsigned();
            $table->integer('gia_sd_dich_vu');
            $table->integer('so_luong_dich_vu');
            $table->dateTime('thoi_gian_sd_dich_vu');
            $table->primary(['ma_hoa_don','ma_dich_vu','thoi_gian_sd_dich_vu'], 'hoa_don_dich_vu_PRIMARY_KEY');
            $table->foreign('ma_hoa_don')->references('ma_hoa_don')->on('hoa_don')->onDelete('cascade');
            $table->foreign('ma_dich_vu')->references('ma_dich_vu')->on('dich_vu')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hoa_don_dich_vu');
    }
}
