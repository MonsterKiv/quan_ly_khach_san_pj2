<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DichVu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dich_vu', function (Blueprint $table){
            $table->increments('ma_dich_vu');
            $table->string('ten_dich_vu',50);
            $table->integer('gia_dich_vu');
            $table->string('don_vi_dich_vu',50);
            $table->integer('ma_trang_thai_dich_vu');
            $table->foreign('ma_trang_thai_dich_vu')->references('ma_trang_thai_dich_vu')->on('trang_thai_dich_vu')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dich_vu');
    }
}
