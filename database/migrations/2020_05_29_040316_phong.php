<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Phong extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phong', function (Blueprint $table){
            $table->increments('ma_phong');
            $table->integer('ma_loai_phong')->unsigned();
            $table->string('ten_phong',50);
            $table->integer('tang');
            $table->integer('ma_view');
            $table->integer('ma_trang_thai_phong');
            $table->foreign('ma_loai_phong')->references('ma_loai_phong')->on('loai_phong')->onDelete('cascade');
            $table->foreign('ma_view')->references('ma_view')->on('view')->onDelete('cascade');
            $table->foreign('ma_trang_thai_phong')->references('ma_trang_thai_phong')->on('trang_thai_phong')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phong');
    }
}
