<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Admin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin', function (Blueprint $table){
            $table->increments('ma_admin');
            $table->string('ten_dang_nhap',50);
            $table->string('mat_khau',50);
            $table->string('ho_ten',50);
            $table->date('ngay_sinh');
            $table->boolean('gioi_tinh',50);
            $table->string('so_dien_thoai',50);
            $table->string('email',50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin');
    }
}
