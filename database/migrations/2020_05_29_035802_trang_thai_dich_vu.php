<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TrangThaiDichVu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trang_thai_dich_vu', function (Blueprint $table){
            $table->integer('ma_trang_thai_dich_vu');
            $table->string('ten_trang_thai_dich_vu',50);
            $table->primary(['ma_trang_thai_dich_vu']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trang_thai_dich_vu');
    }
}
