<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HoaDon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hoa_don', function (Blueprint $table){
            $table->increments('ma_hoa_don');
            $table->unsignedInteger('ma_kh');
            $table->dateTime('thoi_gian_lap_hoa_don');
            $table->integer('ma_trang_thai_hoa_don');
            $table->string('ghi_chu',50);
            $table->foreign('ma_kh')->references('ma_kh')->on('khach_hang')->onDelete('cascade');
            $table->foreign('ma_trang_thai_hoa_don')->references('ma_trang_thai_hoa_don')->on('trang_thai_hoa_don')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hoa_don');
    }
}
