<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VatTuLoaiPhong extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vat_tu_loai_phong', function (Blueprint $table){
            $table->unsignedInteger('ma_vat_tu');
            $table->integer('so_luong_vat_tu');
            $table->unsignedInteger('ma_loai_phong');
            $table->primary(['ma_vat_tu','ma_loai_phong']);
            $table->foreign('ma_vat_tu')->references('ma_vat_tu')->on('vat_tu')->onDelete('cascade');
            $table->foreign('ma_loai_phong')->references('ma_loai_phong')->on('loai_phong')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vat_tu_loai_phong');
    }
}
