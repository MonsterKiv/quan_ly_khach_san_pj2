<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HoaDonPhong extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hoa_don_phong', function (Blueprint $table){
            $table->integer('ma_hoa_don')->unsigned();
            $table->integer('ma_phong')->unsigned();
            $table->integer('gia_dat_phong');
            $table->integer('so_luong_khach');
            $table->dateTime('thoi_gian_den');
            $table->dateTime('thoi_gian_di');
            $table->primary(['ma_hoa_don','ma_phong','thoi_gian_den','thoi_gian_di'], 'hoa_don_phong_PRIMARY_KEY');
            $table->foreign('ma_hoa_don')->references('ma_hoa_don')->on('hoa_don')->onDelete('cascade');
            $table->foreign('ma_phong')->references('ma_phong')->on('phong')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hoa_don_phong');
    }
}
