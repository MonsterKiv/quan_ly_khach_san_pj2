<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TrangThaiHoaDon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trang_thai_hoa_don', function (Blueprint $table){
            $table->integer('ma_trang_thai_hoa_don');
            $table->string('ten_trang_thai_hoa_don',50);
            $table->primary(['ma_trang_thai_hoa_don']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trang_thai_hoa_don');
    }
}
