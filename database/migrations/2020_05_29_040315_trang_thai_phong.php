<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TrangThaiPhong extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trang_thai_phong', function (Blueprint $table){
            $table->integer('ma_trang_thai_phong');
            $table->string('ten_trang_thai_phong',50);
            $table->primary(['ma_trang_thai_phong']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trang_thai_phong');
    }
}
