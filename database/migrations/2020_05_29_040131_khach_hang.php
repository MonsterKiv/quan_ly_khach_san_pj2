<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KhachHang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('khach_hang', function (Blueprint $table){
            $table->increments('ma_kh');
            $table->string('ten_kh',50);
            $table->date('ngay_sinh_kh');
            $table->boolean('gioi_tinh_kh');
            $table->string('quoc_tich_kh',50);
            $table->string('cmt_hc_kh',50);
            $table->string('email_kh',50);
            $table->string('sdt_kh',50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('khach_hang');
    }
}
