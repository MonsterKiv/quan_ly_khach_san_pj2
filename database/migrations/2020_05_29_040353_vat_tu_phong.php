<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VatTuPhong extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vat_tu_phong', function (Blueprint $table){
            $table->unsignedInteger('ma_vat_tu');
            $table->integer('so_luong_vat_tu');
            $table->integer('trang_thai_vat_tu');
            $table->unsignedInteger('ma_phong');
            $table->primary(['ma_vat_tu','ma_phong']);
            $table->foreign('ma_vat_tu')->references('ma_vat_tu')->on('vat_tu')->onDelete('cascade');
            $table->foreign('ma_phong')->references('ma_phong')->on('phong')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vat_tu_phong');
    }
}
