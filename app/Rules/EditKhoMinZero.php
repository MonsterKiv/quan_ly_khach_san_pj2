<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class EditKhoMinZero implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $tong_so_luong_vat_tu;

    public function __construct($tong_so_luong_vat_tu)
    {
        $this->tong_so_luong_vat_tu = $tong_so_luong_vat_tu;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->tong_so_luong_vat_tu >= 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Số lượng vật tư phải lớn hơn 0!';
    }
}
