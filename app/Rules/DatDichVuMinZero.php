<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class DatDichVuMinZero implements Rule
{
    public function passes($attribute, $value)
    {
        if ($value > 0) {
            return true;
        }
        return false;
    }
    public function message()
    {
        return 'Số lượng dịch vụ không hợp lệ!';
    }
}
