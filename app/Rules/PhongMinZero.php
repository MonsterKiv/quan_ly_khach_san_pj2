<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class PhongMinZero implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $tang;

    public function __construct($tang)
    {
        $this->tang = $tang;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->tang >= 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Số tầng phải lớn hơn 0!';
    }
}
