<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CompareEditKho implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $tong_so_luong_vat_tu;
    protected $so_luong_vat_tu_hong;

    public function __construct($tong_so_luong_vat_tu, $so_luong_vat_tu_hong)
    {
        $this->tong_so_luong_vat_tu = $tong_so_luong_vat_tu;
        $this->so_luong_vat_tu_hong = $so_luong_vat_tu_hong;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->tong_so_luong_vat_tu >= $this->so_luong_vat_tu_hong;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Số vật tư hỏng phải ít hơn hoặc bằng tổng số vật tư';
    }
}
