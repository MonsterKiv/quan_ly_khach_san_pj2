<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class NumberMinZero implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $trang_thai_dich_vu;

    public function __construct($trang_thai_dich_vu)
    {
        $this->trang_thai_dich_vu = $trang_thai_dich_vu;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->trang_thai_dich_vu >= 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Trạng thái dịch vụ không hợp lệ!';
    }
}
