<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class DatPhongMinZero implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $so_luong_khach;

    public function __construct($so_luong_khach)
    {
        $this->so_luong_khach = $so_luong_khach;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->so_luong_khach >= 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Số lượng khách không hợp lệ!';
    }
}
