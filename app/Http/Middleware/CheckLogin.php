<?php

namespace App\Http\Middleware;

use Closure;

class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->session()->has('ma_admin')){
            return $next($request);
        }else{
            return redirect()->route('view_login')->with('error','Hãy đăng nhập trước');
        }
    }
}
