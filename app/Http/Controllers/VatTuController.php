<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\VatTuRequest;
use App\Models\VatTuModel;
use App\Models\VatTuLoaiPhongModel;
use App\Models\KhoModel;

class VatTuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $array_vat_tu = VatTuModel::all();

        return view('vat_tu.view_all_vt',[
            'array_vat_tu' => $array_vat_tu
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VatTuRequest $request)
    {
        try {
            $vat_tu             = new VatTuModel();
            $vat_tu->ten_vat_tu = $request->ten_vat_tu;
            $vat_tu->gia_vat_tu = $request->gia_vat_tu;
            $vat_tu->save();

            $ma_vat_tu_inserted = VatTuModel::all()->last()->ma_vat_tu;

            $kho                       = new KhoModel();
            $kho->ma_vat_tu            = $ma_vat_tu_inserted;
            $kho->tong_so_luong_vat_tu = 0;
            $kho->so_luong_vat_tu_hong = 0;
            $kho->save();

            return redirect()->route('vat_tu.index')->with('success','Thêm vật tư thành công!');
        } catch (\Exception $e) {
            return redirect()->route('vat_tu.index')->with('alert','Có lỗi xảy ra, vui lòng thử lại');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($ma_vat_tu)
    {
        $vat_tu = VatTuModel::find($ma_vat_tu);

        return json_encode(array('vat_tu' => $vat_tu));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(VatTuRequest $request, $ma_vat_tu)
    {
        try {
            VatTuModel::find($ma_vat_tu)->update($request->all());

            return redirect()->route('vat_tu.index')->with('success','Sửa đổi vật tư thành công!');
        } catch (\Exception $e) {
            return redirect()->route('vat_tu.index')->with('alert','Có lỗi xảy ra, vui lòng thử lại');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($ma_vat_tu)
    {
        try {
            $check = VatTuLoaiPhongModel::where('ma_vat_tu','=',$ma_vat_tu)->count();
            if ($check != 0) {
                return redirect()->route('vat_tu.index')->with('alert','Hãy xóa vật tư này trong các loại phòng trước!');
            }

            VatTuModel::find($ma_vat_tu)->delete();

            return redirect()->route('vat_tu.index')->with('success','Đã xóa vật tư!');
        } catch (\Exception $e) {
            return redirect()->route('vat_tu.index')->with('alert','Có lỗi xảy ra, vui lòng thử lại');
        }
    }
}
