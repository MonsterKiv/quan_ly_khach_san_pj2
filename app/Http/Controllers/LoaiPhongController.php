<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LoaiPhongRequest;
use App\Models\LoaiPhongModel;
use App\Models\PhongModel;

class LoaiPhongController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $array_loai_phong = LoaiPhongModel::all();

        return view('loai_phong.view_all_lp',[
            'array_loai_phong' => $array_loai_phong
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LoaiPhongRequest $request)
    {
        try {
            $loai_phong                 = new LoaiPhongModel();
            $loai_phong->ten_loai_phong = $request->ten_loai_phong;
            $loai_phong->gia_phong      = $request->gia_phong;
            $loai_phong->save();

            return redirect()->route('loai_phong.index')->with('success','Thêm loại phòng thành công!');
        } catch (\Exception $e) {
            return redirect()->route('loai_phong.index')->with('alert','Có lỗi xảy ra, vui lòng thử lại');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($ma_loai_phong)
    {
        $loai_phong = LoaiPhongModel::find($ma_loai_phong);

        return json_encode(array('loai_phong' => $loai_phong));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LoaiPhongRequest $request, $ma_loai_phong)
    {
        try {
            LoaiPhongModel::find($ma_loai_phong)->update($request->all());
            
            return redirect()->route('loai_phong.index')->with('success','Sửa đổi thông tin loại phòng thành công!');
        } catch (\Exception $e) {
            return redirect()->route('loai_phong.index')->with('alert','Có lỗi xảy ra, vui lòng thử lại');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($ma_loai_phong)
    {
        try {
            $check = PhongModel::where('ma_loai_phong','=',$ma_loai_phong)->get()->count();
            if ($check != 0) {
                return redirect()->route('loai_phong.index')->with('alert','Hãy xóa các phòng tồn tại thuộc loại phòng này trước!');
            }
            LoaiPhongModel::find($ma_loai_phong)->delete();

            return redirect()->route('loai_phong.index')->with('success','Đã xóa loại phòng!');
        } catch (\Exception $e) {
            return redirect()->route('loai_phong.index')->with('alert','Có lỗi xảy ra, vui lòng thử lại');
        }
    }
}
