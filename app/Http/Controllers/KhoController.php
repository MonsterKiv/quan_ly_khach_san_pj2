<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\EditKhoRequest;
use App\Models\KhoModel;
use App\Models\VatTuModel;

class KhoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $array_kho = KhoModel::join('vat_tu','kho.ma_vat_tu','=','vat_tu.ma_vat_tu')
            ->select('vat_tu.ten_vat_tu','kho.*')
            ->get();

        return view('kho.view_all_kho',[
            'array_kho' => $array_kho
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($ma_vat_tu)
    {
        $kho             = KhoModel::find($ma_vat_tu);
        $vat_tu_selected = VatTuModel::find($ma_vat_tu);

        return json_encode(array('kho' => $kho, 'vat_tu_selected' => $vat_tu_selected));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ma_vat_tu)
    {
        try {
            $so_luong_vat_tu = $request->so_luong_vat_tu;
            switch ($request->action) {
                case '1':
                    KhoModel::where('ma_vat_tu',$ma_vat_tu)->increment('tong_so_luong_vat_tu', $so_luong_vat_tu);
                    return redirect()->route('kho.index')->with('success','Nhập vật tư vào kho thành công!');
                    break;

                case '2':
                    KhoModel::where('ma_vat_tu',$ma_vat_tu)->decrement('tong_so_luong_vat_tu', $so_luong_vat_tu);
                    return redirect()->route('kho.index')->with('success','Xuất vật tư thành công!');
                    break;

                case '3':
                    KhoModel::where('ma_vat_tu',$ma_vat_tu)->increment('so_luong_vat_tu_hong', $so_luong_vat_tu);
                    return redirect()->route('kho.index')->with('success','Đã cập nhật số lượng vật tư hỏng / sửa chữa!');
                    break;
                case '4':
                    KhoModel::where('ma_vat_tu',$ma_vat_tu)->decrement('so_luong_vat_tu_hong', $so_luong_vat_tu);
                    return redirect()->route('kho.index')->with('success','Đã cập nhật số lượng vật tư hỏng / sửa chữa!');
                    break;
                
                default:
                    return redirect()->route('kho.index')->with('alert','Có lỗi khi chỉnh sửa dữ liệu');
                    break;
            }
        } catch (\Exception $e) {
            return redirect()->route('kho.index')->with('alert','Có lỗi xảy ra, vui lòng thử lại');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($ma_vat_tu)
    {
        //
    }
}
