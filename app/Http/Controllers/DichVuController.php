<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\DichVuRequest;
use App\Http\Requests\EditDichVuRequest;
use App\Models\DichVuModel;
use App\Models\TrangThaiDichVuModel;

class DichVuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $ten_dich_vu        = $request->ten_dich_vu;
        $trang_thai_dich_vu = $request->trang_thai_dich_vu;

        $array_trang_thai_dich_vu = TrangThaiDichVuModel::all();

        $array_dich_vu = DichVuModel::join('trang_thai_dich_vu','dich_vu.ma_trang_thai_dich_vu','trang_thai_dich_vu.ma_trang_thai_dich_vu')
            ->when($trang_thai_dich_vu, function($query) use ($trang_thai_dich_vu){
                return $query->whereIn('dich_vu.ma_trang_thai_dich_vu', $trang_thai_dich_vu);
            })
            ->when($ten_dich_vu, function($query) use ($ten_dich_vu){
                return $query->where('dich_vu.ten_dich_vu','like','%'.$ten_dich_vu.'%');
            })
            ->orderBy('ma_dich_vu')
            ->paginate(5);

        return view('dich_vu.view_all_dv',[
            'array_dich_vu'            => $array_dich_vu,
            'trang_thai_dich_vu'       => $trang_thai_dich_vu,
            'ten_dich_vu'              => $ten_dich_vu,
            'array_trang_thai_dich_vu' => $array_trang_thai_dich_vu,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DichVuRequest $request)
    {
        $dich_vu                     = new DichVuModel();
        $dich_vu->ten_dich_vu        = $request->ten_dich_vu;
        $dich_vu->gia_dich_vu        = $request->gia_dich_vu;
        $dich_vu->don_vi_dich_vu     = $request->don_vi_dich_vu;
        $dich_vu->ma_trang_thai_dich_vu = $request->trang_thai_dich_vu;
        $dich_vu->save();

        return redirect()->route('dich_vu.index')->with('success','Thêm dịch vụ thành công!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($ma_dich_vu)
    {
        $dich_vu = DichVuModel::find($ma_dich_vu);

        // return view('dich_vu.edit_dv',[
        //     'dich_vu' => $dich_vu
        // ]);
        return json_encode(array('dich_vu' => $dich_vu));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditDichVuRequest $request, $ma_dich_vu)
    {
        DichVuModel::find($ma_dich_vu)->update($request->all());
        
        return redirect()->route('dich_vu.index')->with('success','Thay đổi trạng thái dịch vụ thành công!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($ma_dich_vu)
    {
        DichVuModel::find($ma_dich_vu)->delete();

        return redirect()->route('dich_vu.index')->with('success','Đã xóa dịch vụ!');
    }
}
