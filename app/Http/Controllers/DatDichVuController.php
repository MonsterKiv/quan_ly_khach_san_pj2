<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\OrderDichVuRequest;
use App\Models\HoaDonDichVuModel;
use App\Models\SuDungDichVuModel;
use App\Models\DichVuModel;
use App\Models\HoaDonModel;

class DatDichVuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $array_hoa_don = HoaDonModel::join('khach_hang','hoa_don.ma_kh','=','khach_hang.ma_kh')
            ->where('ma_trang_thai_hoa_don', 1)
            ->select('hoa_don.*','khach_hang.ten_kh as ten_kh')
            ->get();
        $array_dich_vu = DichVuModel::where('ma_trang_thai_dich_vu', 1)->get();

        return view('dich_vu.order_dv',[
            'array_hoa_don' => $array_hoa_don,
            'array_dich_vu' => $array_dich_vu
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderDichVuRequest $request)
    {
        $currentTime = date('Y-m-d H:i:s');

        foreach ($request->ma_dich_vu as $ma_dich_vu) {
            $gia_sd_dich_vu = DichVuModel::find($ma_dich_vu)->gia_dich_vu;
            
            $hoa_don_dich_vu                       = new HoaDonDichVuModel();
            $hoa_don_dich_vu->ma_hoa_don           = $request->ma_hoa_don;
            $hoa_don_dich_vu->ma_dich_vu           = $ma_dich_vu;
            $hoa_don_dich_vu->gia_sd_dich_vu       = $gia_sd_dich_vu;
            $hoa_don_dich_vu->so_luong_dich_vu     = $request->so_luong_dich_vu[$ma_dich_vu];
            $hoa_don_dich_vu->thoi_gian_sd_dich_vu = $currentTime;
            $hoa_don_dich_vu->save();
        }

        return redirect()->route('hoa_don.show', ['ma_hoa_don' => $request->ma_hoa_don])->with('success','Đặt dịch vụ thành công!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
