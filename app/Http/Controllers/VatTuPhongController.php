<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\VatTuPhongRequest;
use App\Models\PhongModel;
use App\Models\LoaiPhongModel;
use App\Models\VatTuPhongModel;
use App\Models\KhoModel;

class VatTuPhongController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($ma_phong)
    {
        $phong      = PhongModel::find($ma_phong);
        $loai_phong = LoaiPhongModel::where('ma_loai_phong','=',$phong->ma_loai_phong)->first();

        $array_vat_tu_phong = VatTuPhongModel::join('vat_tu','vat_tu_phong.ma_vat_tu','vat_tu.ma_vat_tu')
            ->join('phong','vat_tu_phong.ma_phong','phong.ma_phong')
            ->join('vat_tu_loai_phong','vat_tu.ma_vat_tu','vat_tu_loai_phong.ma_vat_tu')
            ->where('vat_tu_phong.ma_phong','=',$ma_phong)
            ->where('vat_tu_loai_phong.ma_loai_phong','=',$phong->ma_loai_phong)
            ->select('vat_tu.ten_vat_tu',
                'vat_tu_loai_phong.so_luong_vat_tu as so_luong_vat_tu_lp',
                'vat_tu_phong.so_luong_vat_tu',
                'vat_tu_phong.trang_thai_vat_tu',
                'vat_tu_phong.ma_vat_tu')
            ->distinct('vat_tu_phong.ma_vat_tu')
            ->get();

        return view('phong.vat_tu_phong.view_all_vtp',[
            'ma_phong'           => $ma_phong,
            'phong'              => $phong,
            'loai_phong'         => $loai_phong,
            'array_vat_tu_phong' => $array_vat_tu_phong
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($ma_phong, $ma_vat_tu)
    {
        $phong               = PhongModel::find($ma_phong);
        $loai_phong          = LoaiPhongModel::where('ma_loai_phong','=',$phong->ma_loai_phong)->first();
        
        $kho                 = KhoModel::find($ma_vat_tu);

        $vat_tu_phong = VatTuPhongModel::join('vat_tu','vat_tu_phong.ma_vat_tu','vat_tu.ma_vat_tu')
            ->join('vat_tu_loai_phong','vat_tu.ma_vat_tu','vat_tu_loai_phong.ma_vat_tu')
            ->where('vat_tu_phong.ma_vat_tu','=',$ma_vat_tu)
            ->where('ma_phong','=',$ma_phong)
            ->select('vat_tu_phong.ma_vat_tu',
                'vat_tu_phong.so_luong_vat_tu',
                'vat_tu_phong.trang_thai_vat_tu',
                'vat_tu_phong.ma_phong',
                'vat_tu.*',
                'vat_tu_loai_phong.so_luong_vat_tu as so_luong_vat_tu_lp')
            ->first();

        return view('phong.vat_tu_phong.edit_vtp',[
            'ma_phong'     => $ma_phong,
            'phong'        => $phong,
            'loai_phong'   => $loai_phong,
            'vat_tu_phong' => $vat_tu_phong,
            'kho'          => $kho
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(VatTuPhongRequest $request, $ma_phong, $ma_vat_tu)
    {
        if ($request->has('trang_thai_vat_tu') == true) {
            VatTuPhongModel::where('ma_vat_tu','=',$ma_vat_tu)
            ->where('ma_phong','=',$ma_phong)
            ->update(['trang_thai_vat_tu' => $request->trang_thai_vat_tu]);
        } else {
            $kho                      = KhoModel::find($ma_vat_tu);
            $so_luong_vat_tu_kho      = $kho->tong_so_luong_vat_tu - $kho->so_luong_vat_tu_hong;
            $so_luong_vat_tu_thay_doi = $request->so_luong_vat_tu - $request->so_luong_vat_tu_cu;
            if ($so_luong_vat_tu_kho >= $so_luong_vat_tu_thay_doi) {
                VatTuPhongModel::where('ma_vat_tu','=',$ma_vat_tu)
                ->where('ma_phong','=',$ma_phong)
                ->update(['so_luong_vat_tu' => $request->so_luong_vat_tu]);

                KhoModel::find($ma_vat_tu)->update(['tong_so_luong_vat_tu' => $kho->tong_so_luong_vat_tu - $so_luong_vat_tu_thay_doi]);
            } else {
                return redirect()->route('vat_tu_phong.index',['ma_phong' => $ma_phong])->with('alert','Số lượng vật tư trong kho không đủ');
            }
        }
        
        return redirect()->route('vat_tu_phong.index',['ma_phong' => $ma_phong])->with('success','Sửa đổi vật tư phòng thành công!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
