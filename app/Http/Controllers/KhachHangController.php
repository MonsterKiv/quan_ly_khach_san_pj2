<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\KhachHangRequest;
use App\Models\KhachHangModel;

class KhachHangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $array_khach_hang = KhachHangModel::all();

        return view('khach_hang.view_all_kh',[
            'array_khach_hang' => $array_khach_hang 
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('khach_hang.create_kh');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KhachHangRequest $request)
    {
        try {
            $khach_hang               = new KhachHangModel();
            $khach_hang->ten_kh       = $request->get('ten_kh');
            $khach_hang->ngay_sinh_kh = $request->get('ngay_sinh_kh');
            $khach_hang->gioi_tinh_kh = $request->get('gioi_tinh_kh');
            $khach_hang->quoc_tich_kh = $request->get('quoc_tich_kh');
            $khach_hang->cmt_hc_kh    = $request->get('cmt_hc_kh');
            $khach_hang->email_kh     = $request->get('email_kh');
            $khach_hang->sdt_kh       = $request->get('sdt_kh');
            $khach_hang->save();

            return redirect()->route('khach_hang.index')->with('success','Thêm khách hàng thành công!');
        } catch (\Exception $e) {
            return redirect()->route('khach_hang.index')->with('alert','Có lỗi xảy ra, vui lòng thử lại');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($ma_kh)
    {
        $khach_hang = KhachHangModel::find($ma_kh);
        
        return json_encode(array('khach_hang' => $khach_hang));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KhachHangRequest $request, $ma_kh)
    {
        try {
            KhachHangModel::find($ma_kh)->update($request->all());

            return redirect()->route('khach_hang.index')->with('success','Thay đổi thông tin khách hàng thành công!');
        } catch (\Exception $e) {
            return redirect()->route('khach_hang.index')->with('alert','Có lỗi xảy ra, vui lòng thử lại');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($ma_kh)
    {
        //
    }
}
