<?php

namespace App\Http\Controllers;

use App\Models\DichVuModel;
use App\Models\HoaDonPhongModel;
use App\Models\HoaDonDichVuModel;
use App\Models\PhongModel;

class ThongKeController extends Controller
{
    public function index(){
        $now           = strtotime(now());
        $current_year  = date('Y', $now);

        $moc_thoi_gian_hoa_don_phong   = HoaDonPhongModel::orderBy('thoi_gian_den','asc')->first()->thoi_gian_den;
        $moc_thoi_gian_hoa_don_dich_vu = HoaDonDichVuModel::orderBy('thoi_gian_sd_dich_vu','asc')->first()->thoi_gian_sd_dich_vu;
        if ($moc_thoi_gian_hoa_don_phong < $moc_thoi_gian_hoa_don_dich_vu) {
            $test = explode('-', $moc_thoi_gian_hoa_don_phong);
        } else {
            $test = explode('-', $moc_thoi_gian_hoa_don_dich_vu);
        }
        $start_year = $test[0];
        
        // return $start_month;

    	return view('thong_ke',[
            'current_year'  => $current_year,
            'start_year'    => $start_year
        ]);
    }
}