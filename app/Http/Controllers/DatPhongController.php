<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\OrderPhongRequest;
use App\Models\HoaDonModel;
use App\Models\HoaDonPhongModel;
use App\Models\PhongModel;

class DatPhongController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $array_hoa_don = HoaDonModel::join('khach_hang','hoa_don.ma_kh','=','khach_hang.ma_kh')
            ->chuaThanhToan()
            ->select('hoa_don.*','khach_hang.ten_kh as ten_kh')
            ->get();
            
        return view('phong.order_phong',[
            'array_hoa_don' => $array_hoa_don
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderPhongRequest $request)
    {
        $phong = PhongModel::join('loai_phong','phong.ma_loai_phong','loai_phong.ma_loai_phong')
            ->where('ma_phong','=',$request->ma_phong)->first();

        $hoa_don_phong                 = new HoaDonPhongModel;
        $hoa_don_phong->ma_hoa_don     = $request->ma_hoa_don;
        $hoa_don_phong->ma_phong       = $request->ma_phong;
        $hoa_don_phong->gia_dat_phong  = $phong->gia_phong;
        $hoa_don_phong->so_luong_khach = $request->so_luong_khach;
        $hoa_don_phong->thoi_gian_den  = $request->thoi_gian_den;
        $hoa_don_phong->thoi_gian_di   = $request->thoi_gian_di;
        $hoa_don_phong->save();

        return redirect()->route('hoa_don.show', ['ma_hoa_don' => $request->ma_hoa_don])->with('success','Đặt phòng thành công!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ma_hoa_don)
    {
        if ($request->has('new_thoi_gian_den') == true) {
            $check = HoaDonPhongModel::where('ma_hoa_don','=',$ma_hoa_don)
                // ->where('ma_phong','!=',$request->ma_phong)
                ->where('thoi_gian_den','!=',$request->thoi_gian_den)
                ->where('thoi_gian_di','!=',$request->thoi_gian_di)
                ->where('thoi_gian_den','<',$request->new_thoi_gian_den)
                ->where('thoi_gian_di','>',$request->new_thoi_gian_den)
                ->get();
                // dd($check);
            if ($check->count() == 0) {
                HoaDonPhongModel::where('ma_hoa_don','=',$ma_hoa_don)
                    ->where('ma_phong','=',$request->ma_phong)
                    ->where('thoi_gian_di','=',$request->thoi_gian_di)
                    ->update(['thoi_gian_den' => $request->new_thoi_gian_den]);

                return redirect()->route('hoa_don.show', ['ma_hoa_don' => $ma_hoa_don])->with('success','Lấy phòng sớm thành công!');
            }
            return redirect()->route('hoa_don.show', ['ma_hoa_don' => $ma_hoa_don])->with('alert','Phòng hiện đang được sử dụng, không thể lấy phòng sớm!');
        }
        HoaDonPhongModel::where('ma_hoa_don','=',$ma_hoa_don)
            ->where('ma_phong','=',$request->ma_phong)
            ->where('thoi_gian_den','=',$request->thoi_gian_den)
            ->update(['thoi_gian_di' => $request->new_thoi_gian_di]);

        return redirect()->route('hoa_don.show', ['ma_hoa_don' => $ma_hoa_don])->with('success','Trả phòng sớm thành công!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $ma_hoa_don)
    {
        HoaDonPhongModel::where('ma_hoa_don','=',$ma_hoa_don)
            ->where('ma_phong','=',$request->ma_phong)
            ->where('thoi_gian_den','=',$request->thoi_gian_den)
            ->where('thoi_gian_di','=',$request->thoi_gian_di)
            ->delete();

        return redirect()->route('hoa_don.show', ['ma_hoa_don' => $ma_hoa_don])->with('success','Đã hủy đặt phòng!');
    }
}
