<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\HoaDonRequest;
use App\Models\KhachHangModel;
use App\Models\HoaDonModel;
use App\Models\HoaDonDichVuModel;
use App\Models\SuDungDichVuModel;
use App\Models\HoaDonPhongModel;
use DB;

class HoaDonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $array_hoa_don = HoaDonModel::join('khach_hang','hoa_don.ma_kh','=','khach_hang.ma_kh')
            ->join('trang_thai_hoa_don','hoa_don.ma_trang_thai_hoa_don','trang_thai_hoa_don.ma_trang_thai_hoa_don')
            ->with([
                'array_hoa_don_dich_vu' => function($query){
                    $query
                    ->select(DB::raw('sum(gia_sd_dich_vu * so_luong_dich_vu) as tong_tien_dich_vu'), 'ma_hoa_don')
                    ->groupBy('ma_hoa_don');
                },
                'array_hoa_don_phong' => function($query){
                    $query
                    ->select(DB::raw('round(gia_dat_phong/3 * ceil(sum(time_to_sec(timediff(hoa_don_phong.thoi_gian_di,hoa_don_phong.thoi_gian_den)))/28800),-2) as tong_tien_phong'), 'ma_hoa_don')
                    ->groupBy('ma_hoa_don','ma_phong');
                }
            ])
            ->select('hoa_don.*','khach_hang.ten_kh as ten_kh','trang_thai_hoa_don.*')
            ->get();
            
        $array_khach_hang = KhachHangModel::leftJoin('hoa_don','khach_hang.ma_kh','=','hoa_don.ma_kh')
            ->select('khach_hang.*')
            ->whereNull('ma_trang_thai_hoa_don')
            ->orWhere('ma_trang_thai_hoa_don','!=',1)
            ->distinct()
            ->get();

        return view('hoa_don.view_all_hd',[
            'array_hoa_don'    => $array_hoa_don,
            'array_khach_hang' => $array_khach_hang
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HoaDonRequest $request)
    {
        try {
            $currentTime = date('Y-m-d H:i:s');
            $ghi_chu = ($request->ghi_chu == null) ? '' : $request->ghi_chu ;

            $hoa_don                        = new HoaDonModel();
            $hoa_don->ma_kh                 = $request->ma_kh;
            $hoa_don->thoi_gian_lap_hoa_don = $currentTime;
            $hoa_don->ma_trang_thai_hoa_don = 1;
            $hoa_don->ghi_chu               = $ghi_chu;
            $hoa_don->save();

            return redirect()->route('hoa_don.index')->with('success','Thêm hóa đơn thành công!');
        } catch (\Exception $e) {
            return redirect()->route('hoa_don.index')->with('alert','Có lỗi xảy ra, vui lòng thử lại');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($ma_hoa_don)
    {
        $array_hoa_don = HoaDonModel::where('ma_hoa_don','=',$ma_hoa_don)
            ->join('khach_hang','hoa_don.ma_kh','khach_hang.ma_kh')
            ->join('trang_thai_hoa_don','hoa_don.ma_trang_thai_hoa_don','trang_thai_hoa_don.ma_trang_thai_hoa_don')
            ->with([
                'array_hoa_don_dich_vu' => function($query){
                    $query
                    ->select(DB::raw('SUM(gia_sd_dich_vu * so_luong_dich_vu) as tong_tien_dich_vu'), 'ma_hoa_don')
                    ->groupBy('ma_hoa_don');
                },
                'array_hoa_don_phong' => function($query){
                    $query
                    ->select(DB::raw('round(gia_dat_phong/3 * ceil(sum(time_to_sec(timediff(thoi_gian_di,thoi_gian_den)))/28800),-2) as tong_tien_phong'), 'ma_hoa_don')
                    ->groupBy('ma_hoa_don','ma_phong');
                }
            ])
            ->select('hoa_don.*','khach_hang.ten_kh as ten_kh','trang_thai_hoa_don.*')
            ->get();

        $array_hoa_don_phong = HoaDonPhongModel::join('phong','hoa_don_phong.ma_phong','phong.ma_phong')
            ->where('ma_hoa_don','=',$ma_hoa_don)
            ->orderBy('ma_hoa_don')
            ->get();

        $array_hoa_don_dich_vu = HoaDonDichVuModel::join('dich_vu','hoa_don_dich_vu.ma_dich_vu','dich_vu.ma_dich_vu')
            ->where('ma_hoa_don','=',$ma_hoa_don)
            ->orderBy('thoi_gian_sd_dich_vu')
            ->get();

        $array_hoa_don_phong_group_by = HoaDonPhongModel::join('phong','hoa_don_phong.ma_phong','phong.ma_phong')
            ->where('ma_hoa_don','=',$ma_hoa_don)
            ->select('phong.ten_phong','hoa_don_phong.gia_dat_phong')
            ->selectRaw('sum(time_to_sec(timediff(hoa_don_phong.thoi_gian_di,hoa_don_phong.thoi_gian_den))) as tong_thoi_gian_thue_phong')
            ->selectRaw('ceil(sum(time_to_sec(timediff(hoa_don_phong.thoi_gian_di,hoa_don_phong.thoi_gian_den)))/28800) as he_so_thoi_gian_thue')
            ->groupBy('phong.ma_phong','hoa_don_phong.gia_dat_phong')
            ->get();

        $array_hoa_don_dich_vu_group_by = HoaDonDichVuModel::join('dich_vu','hoa_don_dich_vu.ma_dich_vu','dich_vu.ma_dich_vu')
            ->where('ma_hoa_don','=',$ma_hoa_don)
            ->select('dich_vu.ten_dich_vu','hoa_don_dich_vu.gia_sd_dich_vu','dich_vu.don_vi_dich_vu')
            ->selectRaw('sum(hoa_don_dich_vu.so_luong_dich_vu) as so_luong_dich_vu')
            ->selectRaw('sum(gia_sd_dich_vu * so_luong_dich_vu) as tong_tien_dich_vu')
            ->groupBy('dich_vu.ma_dich_vu','hoa_don_dich_vu.gia_sd_dich_vu')
            ->get();

        // return $array_hoa_don;

        return view('hoa_don.view_details_hd', [
            'array_hoa_don'                  => $array_hoa_don,
            'array_hoa_don_phong'            => $array_hoa_don_phong,
            'array_hoa_don_dich_vu'          => $array_hoa_don_dich_vu,
            'array_hoa_don_phong_group_by'   => $array_hoa_don_phong_group_by,
            'array_hoa_don_dich_vu_group_by' => $array_hoa_don_dich_vu_group_by
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($ma_hoa_don)
    {
        $hoa_don = HoaDonModel::join('khach_hang','hoa_don.ma_kh','khach_hang.ma_kh')
            ->where('ma_hoa_don',$ma_hoa_don)
            ->first();

        return json_encode(array('hoa_don' => $hoa_don));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ma_hoa_don)
    {
        try {
            HoaDonModel::where('ma_hoa_don',$ma_hoa_don)->update($request->only((new HoaDonModel)->getFillable()));

            if ($request->has('ma_trang_thai_hoa_don') == true) {
                return redirect()->route('hoa_don.index')->with('success','Thay đổi trạng thái hóa đơn thành công!');
            }
            return redirect()->route('hoa_don.index')->with('success','Sửa đổi ghi chú hóa đơn thành công!');
        } catch (\Exception $e) {
            return redirect()->route('hoa_don.index')->with('alert','Có lỗi xảy ra, vui lòng thử lại');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
