<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PhongRequest;
use App\Http\Requests\EditPhongRequest;
use App\Models\PhongModel;
use App\Models\LoaiPhongModel;
use App\Models\VatTuModel;
use App\Models\VatTuPhongModel;
use App\Models\HoaDonModel;
use App\Models\HoaDonPhongModel;
use App\Models\ViewModel;
use App\Models\TrangThaiPhongModel;

class PhongController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $now = date("Y-m-d H:i:s");

        $array_phong_ordered = HoaDonPhongModel::join('phong','hoa_don_phong.ma_phong','phong.ma_phong')
            ->where('thoi_gian_den','<',$now)
            ->where('thoi_gian_di','>',$now)
            ->select('phong.ma_phong')
            ->get()
            ->toArray();
        
        $loai_phong = $request->loai_phong;
        $ngay_den   = $request->ngay_den;
        $ngay_di    = $request->ngay_di;
        $view       = $request->view;

        $array_loai_phong       = LoaiPhongModel::all();
        $array_view             = ViewModel::all();
        $array_trang_thai_phong = TrangThaiPhongModel::all();

        $array_phong = PhongModel::join('loai_phong','phong.ma_loai_phong','loai_phong.ma_loai_phong')
            ->join('view','phong.ma_view','view.ma_view')
            ->join('trang_thai_phong','phong.ma_trang_thai_phong','trang_thai_phong.ma_trang_thai_phong')
            ->when($loai_phong, function ($query) use ($loai_phong){
                return $query->whereIn('loai_phong.ma_loai_phong',$loai_phong);
            })
            ->when(($ngay_den && $ngay_di), function ($query) use ($ngay_den, $ngay_di){
                $ngay_den_format         = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $ngay_den)));
                $ngay_di_format          = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $ngay_di)));
                $array_hoa_don_phong_out = HoaDonPhongModel::where(function ($query) use ($ngay_den_format){
                        $query->where('thoi_gian_den','<=',$ngay_den_format)
                            ->where('thoi_gian_di','>=',$ngay_den_format);
                    })
                    ->orWhere(function ($query) use ($ngay_di_format){
                        $query->where('thoi_gian_den','<=',$ngay_di_format)
                            ->where('thoi_gian_di','>=',$ngay_di_format);
                    })
                    ->select('ma_phong')
                    ->get()
                    ->toArray();
                return $query->whereNotIn('ma_phong',$array_hoa_don_phong_out);
            })
            ->when($view, function ($query) use ($view){
                return $query->whereIn('view.ma_view',$view);
            })
            ->orderBy('ten_phong')
            ->paginate(5);

        return view('phong.view_all_phong',[
            'array_phong'            => $array_phong,
            'array_loai_phong'       => $array_loai_phong,
            'array_view'             => $array_view,
            'array_trang_thai_phong' => $array_trang_thai_phong,
            'loai_phong'             => $loai_phong,
            'ngay_den'               => $ngay_den,
            'ngay_di'                => $ngay_di,
            'view'                   => $view,
            'array_phong_ordered'    => $array_phong_ordered,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PhongRequest $request)
    {
        try {
            $phong                      = new PhongModel();
            $phong->ma_loai_phong       = $request->ma_loai_phong;
            $phong->ten_phong           = $request->ten_phong;
            $phong->tang                = $request->tang;
            $phong->ma_view             = $request->ma_view;
            $phong->ma_trang_thai_phong = $request->ma_trang_thai_phong;
            $phong->save();

            $array_vtlp_to_add = VatTuModel::join('vat_tu_loai_phong','vat_tu.ma_vat_tu','vat_tu_loai_phong.ma_vat_tu')
                ->where('ma_loai_phong','=',$request->ma_loai_phong)
                ->get();
            $ma_phong_added = PhongModel::all()->last()->ma_phong;
            foreach ($array_vtlp_to_add as $each) {
                $vat_tu_phong                    = new VatTuPhongModel();
                $vat_tu_phong->ma_vat_tu         = $each->ma_vat_tu;
                $vat_tu_phong->so_luong_vat_tu   = "0";
                $vat_tu_phong->trang_thai_vat_tu = "0";
                $vat_tu_phong->ma_phong          = $ma_phong_added;
                $vat_tu_phong->save();
            }

            return redirect()->route('phong.index')->with('success','Thêm phòng thành công!');
        } catch (\Exception $e) {
            return redirect()->route('phong.index')->with('alert','Có lỗi xảy ra, vui lòng thử lại');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($ma_phong)
    {
        $phong      = PhongModel::find($ma_phong);
        $loai_phong = LoaiPhongModel::where('ma_loai_phong','=',$phong->ma_loai_phong)->first();

        return json_encode(array(
            'phong'      => $phong,
            'loai_phong' => $loai_phong,
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditPhongRequest $request, $ma_phong)
    {
        try {
            PhongModel::find($ma_phong)->update($request->only((new PhongModel)->getFillable()));

            return redirect()->route('phong.index')->with('success','Chỉnh sửa phòng thành công!');
        } catch (\Exception $e) {
            return redirect()->route('phong.index')->with('alert','Có lỗi xảy ra, vui lòng thử lại');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($ma_phong)
    {
        try {
            $check = VatTuPhongModel::where('ma_phong','=',$ma_phong)
                ->where('so_luong_vat_tu','>',0)
                ->get()
                ->count();
            if ($check != 0) {
                return redirect()->route('loai_phong.index')->with('alert','Phòng vẫn còn vật tư, hãy trả vật tư về kho trước!');
            }
            PhongModel::find($ma_phong)->delete();

            return redirect()->route('phong.index')->with('success','Đã xóa phòng!');
        } catch (\Exception $e) {
            return redirect()->route('phong.index')->with('alert','Có lỗi xảy ra, vui lòng thử lại');
        }
    }
}
