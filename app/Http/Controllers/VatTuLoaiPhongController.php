<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\VatTuLoaiPhongRequest;
use App\Http\Requests\EditVatTuLoaiPhongRequest;
use App\Models\VatTuLoaiPhongModel;
use App\Models\VatTuPhongModel;
use App\Models\LoaiPhongModel;
use App\Models\VatTuModel;
use App\Models\PhongModel;

class VatTuLoaiPhongController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($ma_loai_phong)
    {
        $loai_phong = LoaiPhongModel::find($ma_loai_phong);

        $array_vat_tu_loai_phong = VatTuLoaiPhongModel::join('loai_phong','vat_tu_loai_phong.ma_loai_phong','loai_phong.ma_loai_phong')
            ->join('vat_tu','vat_tu_loai_phong.ma_vat_tu','vat_tu.ma_vat_tu')
            ->where('vat_tu_loai_phong.ma_loai_phong','=',$ma_loai_phong)
            ->get();

        $array_vat_tu_selected = VatTuLoaiPhongModel::where('ma_loai_phong','=',$ma_loai_phong)
            ->get()
            ->toArray();
        $array_vat_tu          = VatTuModel::whereNotIn('ma_vat_tu',$array_vat_tu_selected)
            ->get();

        return view('loai_phong.vat_tu_loai_phong.view_all_vtlp',[
            'ma_loai_phong'           => $ma_loai_phong,
            'loai_phong'              => $loai_phong,
            'array_vat_tu_loai_phong' => $array_vat_tu_loai_phong,
            'array_vat_tu'            => $array_vat_tu
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($ma_loai_phong)
    {
        $loai_phong = LoaiPhongModel::find($ma_loai_phong);

        $array_vat_tu_selected = VatTuLoaiPhongModel::where('ma_loai_phong','=',$ma_loai_phong)->get()->toArray();

        $array_vat_tu = VatTuModel::whereNotIn('ma_vat_tu',$array_vat_tu_selected)->get();

        return view('loai_phong.vat_tu_loai_phong.create_vtlp',[
            'ma_loai_phong' => $ma_loai_phong,
            'loai_phong'    => $loai_phong,
            'array_vat_tu'  => $array_vat_tu
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VatTuLoaiPhongRequest $request, $ma_loai_phong)
    {
        $vat_tu_loai_phong                  = new VatTuLoaiPhongModel();
        $vat_tu_loai_phong->ma_vat_tu       = $request->ma_vat_tu;
        $vat_tu_loai_phong->so_luong_vat_tu = $request->so_luong_vat_tu;
        $vat_tu_loai_phong->ma_loai_phong   = $ma_loai_phong;
        $vat_tu_loai_phong->save();

        $array_phong    = PhongModel::where('ma_loai_phong','=',$ma_loai_phong)->get();
        $array_ma_phong = [];
        foreach ($array_phong as $each) {
            array_push($array_ma_phong, $each->ma_phong);
        }

        foreach ($array_ma_phong as $ma_phong) {
            $vat_tu_phong                    = new VatTuPhongModel();
            $vat_tu_phong->ma_vat_tu         = $request->ma_vat_tu;
            $vat_tu_phong->so_luong_vat_tu   = 0;
            $vat_tu_phong->trang_thai_vat_tu = 0;
            $vat_tu_phong->ma_phong          = $ma_phong;
            $vat_tu_phong->save();
        }

        return redirect()->route('vat_tu_loai_phong.index',['ma_loai_phong' => $ma_loai_phong])->with('success','Thêm vật tư vào loại phòng thành công!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($ma_loai_phong, $ma_vat_tu)
    {
        $vat_tu_loai_phong = VatTuLoaiPhongModel::join('loai_phong','vat_tu_loai_phong.ma_loai_phong','loai_phong.ma_loai_phong')
            ->join('vat_tu','vat_tu_loai_phong.ma_vat_tu','vat_tu.ma_vat_tu')
            ->where('vat_tu_loai_phong.ma_vat_tu','=',$ma_vat_tu)
            ->where('vat_tu_loai_phong.ma_loai_phong','=',$ma_loai_phong)
            ->first();

        // return view('loai_phong.vat_tu_loai_phong.edit_vtlp',[
        //     'ma_loai_phong'           => $ma_loai_phong,
        //     'ma_vat_tu'               => $ma_vat_tu,
        //     'array_vat_tu_loai_phong' => $array_vat_tu_loai_phong
        // ]);
        return json_encode(array('vat_tu_loai_phong' => $vat_tu_loai_phong));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditVatTuLoaiPhongRequest $request, $ma_loai_phong, $ma_vat_tu)
    {
        VatTuLoaiPhongModel::where('ma_vat_tu','=',$ma_vat_tu)
            ->where('ma_loai_phong','=',$ma_loai_phong)
            ->update(['so_luong_vat_tu' => $request->so_luong_vat_tu]);

        return redirect()->route('vat_tu_loai_phong.index',['ma_loai_phong' => $ma_loai_phong])->with('success','Sửa đổi vật tư loại phòng thành công!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($ma_loai_phong, $ma_vat_tu)
    {
        $array_phong    = PhongModel::where('ma_loai_phong','=',$ma_loai_phong)->get();
        $array_ma_phong = [];
        foreach ($array_phong as $each) {
            array_push($array_ma_phong, $each->ma_phong);
        }

        $vat_tu_phong_to_delete = VatTuPhongModel::whereIn('ma_phong',$array_ma_phong)
            ->where('ma_vat_tu','=',$ma_vat_tu);
        $check = $vat_tu_phong_to_delete->get();
        foreach ($check as $each) {
            if ($each->so_luong_vat_tu > 0) {
                return redirect()->route('vat_tu_loai_phong.index',['ma_loai_phong' => $ma_loai_phong])->with('alert','Hãy xóa vật tư trong phòng thuộc loại phòng này trước!');
                break;
            }
        }

        VatTuLoaiPhongModel::where('ma_vat_tu','=',$ma_vat_tu)
            ->where('ma_loai_phong','=',$ma_loai_phong)
            ->delete();

        $vat_tu_phong_to_delete->delete();

        return redirect()->route('vat_tu_loai_phong.index',['ma_loai_phong' => $ma_loai_phong])->with('success','Đã xóa vật tư loại phòng!');
    }
}
