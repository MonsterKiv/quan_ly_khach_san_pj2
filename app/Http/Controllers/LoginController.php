<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\HoaDonModel;
use App\Models\PhongModel;
use App\Models\HoaDonPhongModel;
use App\Models\KhoModel;
use App\Models\DichVuModel;
use App\Models\VatTuPhongModel;
use Exception;
use Session;

/**
 * 
 */
class LoginController extends Controller
{
	public function view_login()
	{
		if (Session::has('ma_admin')) {
			return redirect()->route('welcome');
		} else {
			return view('view_login');
		}
	}

	public function process_login(Request $rq)
	{
		$ten_dang_nhap	= $rq->get('ten_dang_nhap');
		$mat_khau		= $rq->get('mat_khau');
		try {
			$admin = Admin::where('ten_dang_nhap',$ten_dang_nhap)
			->where('mat_khau',$mat_khau)
			->firstOrFail();

			$rq->session()->put('ma_admin',$admin->ma_admin);
			$rq->session()->put('ho_ten',$admin->ho_ten);
			$rq->session()->put('ngay_sinh',$admin->ngay_sinh);
			$rq->session()->put('gioi_tinh',$admin->gioi_tinh);
			$rq->session()->put('so_dien_thoai',$admin->so_dien_thoai);
			$rq->session()->put('email',$admin->email);

			return redirect()->route('welcome');
		} catch (Exception $e) {
			return redirect()->route('view_login')->with('error','Đăng nhập thất bại, mời thử lại');
		}
	}

	public function welcome()
	{
		$now		= strtotime(now());
		$date_time	= date('Y-m-d H:i:s', $now);

		/* Tổng hóa đơn hiện tại và cần thanh toán */
		$so_hoa_don_hien_tai		= HoaDonModel::where('ma_trang_thai_hoa_don','1')->count();
		$so_hoa_don_can_thanh_toan	= HoaDonPhongModel::where('thoi_gian_di','>','now')
			->groupBy('ma_hoa_don')
			->get()
			->count();

		/* Tổng số phòng và số phòng đang sử dụng */
		$tong_so_phong			= PhongModel::all()->count();
		$so_phong_dang_su_dung	= HoaDonPhongModel::where('thoi_gian_den','<',$date_time)
			->where('thoi_gian_di','>',$date_time)
			->count();

		/* Số lượng vật tư hỏng */
		$so_luong_vat_tu_hong = KhoModel::sum('so_luong_vat_tu_hong');

		/* Số lượng dịch vụ sẵn sàng / tổng số dịch vụ */
		$tong_so_dich_vu			= DichVuModel::all()->count();
		$tong_so_dich_vu_san_sang	= DichVuModel::where('ma_trang_thai_dich_vu','1')->count();

		/* Số phòng thiếu loại vật tư (loại vật tư trong phòng = 0, chỉ thiếu thì không liệt kê) */
		$tong_so_phong_chua_co_vat_tu = VatTuPhongModel::where('so_luong_vat_tu','0')
			->groupBy('ma_phong')
			->get()
			->count();

		// return $tong_so_phong_chua_co_vat_tu;
		return view('welcome',[
			'so_hoa_don_hien_tai'			=> $so_hoa_don_hien_tai,
			'so_hoa_don_can_thanh_toan'		=> $so_hoa_don_can_thanh_toan,
			'tong_so_phong'					=> $tong_so_phong,
			'so_phong_dang_su_dung'			=> $so_phong_dang_su_dung,
			'so_luong_vat_tu_hong'			=> $so_luong_vat_tu_hong,
			'tong_so_dich_vu'				=> $tong_so_dich_vu,
			'tong_so_dich_vu_san_sang'		=> $tong_so_dich_vu_san_sang,
			'tong_so_phong_chua_co_vat_tu'	=> $tong_so_phong_chua_co_vat_tu
		]);
	}

	public function change_password()
	{
		return view('change_password');
	}

	public function update_admin_password(Request $rq)
	{
		$mat_khau				= $rq->get('mat_khau');
		$mat_khau_moi			= $rq->get('mat_khau_moi');
		$nhap_lai_mat_khau_moi	= $rq->get('nhap_lai_mat_khau_moi');
		$ma_admin				= $rq->session()->get('ma_admin');
		$error_message			= 'Mật khẩu cũ sai hoặc mật khẩu mới nhập chưa trùng khớp, vui lòng thử lại';

		if ($mat_khau_moi == $nhap_lai_mat_khau_moi) {
			try {
				$admin = Admin::where('ma_admin',$ma_admin)
				->where('mat_khau',$mat_khau)
				->firstOrFail();
				$admin->mat_khau = $mat_khau_moi;
				$admin->save();

				return redirect()->route('welcome')->with('success','Đổi mật khẩu thành công!');
			} catch (Exception $e) {
				return redirect()->route('change_password')->with('error',$error_message);
			}
		} else {
			return redirect()->route('change_password')->with('error',$error_message);
		}
	}

	public function logout(Request $rq)
	{
		$rq->session()->flush();
		return redirect()->route('view_login')->with('logout','Đăng xuất thành công');
	}
}