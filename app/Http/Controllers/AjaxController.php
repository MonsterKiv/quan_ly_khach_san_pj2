<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DichVuModel;
use App\Models\HoaDonModel;
use App\Models\HoaDonPhongModel;
use App\Models\HoaDonDichVuModel;
use App\Models\PhongModel;
use App\Http\Requests\DateOrderRequest;
use DB;

class AjaxController extends Controller
{
    public function getDichVu(Request $rq){
		$ma_dich_vu				= $rq->get('ma_dich_vu');
		$array_dich_vu_receive	= DichVuModel::find($ma_dich_vu);

    	return $array_dich_vu_receive;
    }

    public function getPhong(DateOrderRequest $rq)
    {
        /* Get variable from request & change to date type */
		$thoi_gian_den_stt = strtotime($rq->get('thoi_gian_den'));
		$thoi_gian_di_stt = strtotime($rq->get('thoi_gian_di'));
		$thoi_gian_den	= date('Y-m-d H:i:s', $thoi_gian_den_stt);
		$thoi_gian_di	= date('Y-m-d H:i:s', $thoi_gian_di_stt);

        /* Select */
    	$array_phong_ordered = HoaDonPhongModel::getArrayPhongOrdered($thoi_gian_den,$thoi_gian_di)
    		->select('ma_phong')
    		->get()
    		->toArray();

    	$array_phong_receive = PhongModel::where('ma_trang_thai_phong',2)
			->whereNotIn('ma_phong',$array_phong_ordered)
			->get();

		return $array_phong_receive;
    }

    public function getHoaDonPhong(Request $rq)
    {
        $ma_phong                    = $rq->get('ma_phong');
        $array_hoa_don_phong_receive = HoaDonPhongModel::join('hoa_don','hoa_don_phong.ma_hoa_don','hoa_don.ma_hoa_don')
            ->join('khach_hang','hoa_don.ma_kh','khach_hang.ma_kh')
            ->where('ma_phong',$ma_phong)
            ->select('khach_hang.ten_kh as title','hoa_don_phong.thoi_gian_den as start','hoa_don_phong.thoi_gian_di as end','gia_dat_phong','so_luong_khach')
            ->get();

        return $array_hoa_don_phong_receive;
    }

    public function getThongKe(Request $rq)
    {
        /* Get year and month from request and calculate next month */
        $selected_nam   = $rq->selected_nam;
        $selected_thang = $rq->selected_thang;
        $next_thang = $selected_thang + 1;
        $next_nam = $selected_nam;
        if ($selected_thang < 9) {
            $next_thang = '0' . $next_thang;
        } elseif ($selected_thang = 12) {
            $next_thang = '01';
            $next_nam++;
        }

        /* Parse variables to make SQL */
        $str_date_like     = $selected_nam . '-' . $selected_thang . '%';
        $str_date_full     = $selected_nam . '-' . $selected_thang . '-01 00:00:00';
        $str_nextdate_full = $next_nam . '-' . $next_thang . '-01 00:00:00';

        $array_hoa_don_phong = HoaDonPhongModel::where('thoi_gian_den','LIKE',$str_date_like)
            ->orWhere('thoi_gian_di','LIKE',$str_date_like)
            ->orWhere(function($query) use ($str_date_full){
                return $query->where('thoi_gian_den','<',$str_date_full)
                    ->where('thoi_gian_di','>',$str_date_full);
            })
            ->get();

        /* Calculate service and room statistics */
        $tien_dich_vu = HoaDonDichVuModel::where('thoi_gian_sd_dich_vu','LIKE',$str_date_like)
            ->sum(DB::raw('gia_sd_dich_vu * so_luong_dich_vu'));

        $tien_phong = 0;
        foreach ($array_hoa_don_phong as $each) {
            if ($each->thoi_gian_den >= $str_date_full && $each->thoi_gian_di <= $str_nextdate_full) {
                $stt = strtotime($each->thoi_gian_di) - strtotime($each->thoi_gian_den);
            } elseif ($each->thoi_gian_den < $str_date_full && $each->thoi_gian_di > $str_nextdate_full) {
                $stt = strtotime($str_nextdate_full) - strtotime($str_date_full);
            } elseif ($each->thoi_gian_den < $str_date_full) {
                $stt = strtotime($each->thoi_gian_di) - strtotime($str_date_full);
            } else {
                $stt = strtotime($str_nextdate_full) - strtotime($each->thoi_gian_den);
            }
            $tien_phong += round($each->gia_dat_phong / 3 * ceil($stt / 28800), -2);
        }

        /* Push the value */
        $array_tien_thang = [];
        array_push($array_tien_thang, $tien_phong, intval($tien_dich_vu));

        return $array_tien_thang;
    }

    public function getThongKeNoiBat(Request $rq)
    {
        /* Get year and month from request and calculate next month */
        $selected_nam   = $rq->selected_nam;
        $selected_thang = $rq->selected_thang;
        $next_thang = $selected_thang + 1;
        $next_nam = $selected_nam;
        if ($selected_thang < 9) {
            $next_thang = '0' . $next_thang;
        } elseif ($selected_thang = 12) {
            $next_thang = '01';
            $next_nam++;
        }

        /* Parse variables to make SQL */
        $str_date_like     = $selected_nam . '-' . $selected_thang . '%';
        $str_date_full     = $selected_nam . '-' . $selected_thang . '-01 00:00:00';
        $str_nextdate_full = $next_nam . '-' . $next_thang . '-01 00:00:00';

        /* Phòng có được đặt nhiều thời gian nhất */
        // return: $tgdp, $phong
        $array_hoa_don_phong = HoaDonPhongModel::where('thoi_gian_den','LIKE',$str_date_like)
            ->orWhere('thoi_gian_di','LIKE',$str_date_like)
            ->orWhere(function($query) use ($str_date_full){
                return $query->where('thoi_gian_den','<',$str_date_full)
                    ->where('thoi_gian_di','>',$str_date_full);
            })
            ->get();

        $last_stt = 0;
        $thoi_gian_dung_phong = [];
        foreach ($array_hoa_don_phong as $each) {
            if ($each->thoi_gian_den >= $str_date_full && $each->thoi_gian_di <= $str_nextdate_full) {
                $stt = strtotime($each->thoi_gian_di) - strtotime($each->thoi_gian_den);
            } elseif ($each->thoi_gian_den < $str_date_full && $each->thoi_gian_di > $str_nextdate_full) {
                $stt = strtotime($str_nextdate_full) - strtotime($str_date_full);
            } elseif ($each->thoi_gian_den < $str_date_full) {
                $stt = strtotime($each->thoi_gian_di) - strtotime($str_date_full);
            } else {
                $stt = strtotime($str_nextdate_full) - strtotime($each->thoi_gian_den);
            }

            if (isset($thoi_gian_dung_phong[$each->ma_phong])) {
                $old_tgdp = $thoi_gian_dung_phong[$each->ma_phong];
                $thoi_gian_dung_phong[$each->ma_phong] = $old_tgdp + $stt;
            } else {
                $thoi_gian_dung_phong[$each->ma_phong] = $stt;
            }
        }

        $check_phong = 0;
        foreach ($thoi_gian_dung_phong as $key => $value) {
            if ($value >= $check_phong) {
                $check_phong = $value;
                $ma_phong = $key;
            }
        }
        $tgdp_day = floor($check_phong/86400);
        $tgdp_hour = floor(($check_phong - $tgdp_day * 86400) / 3600);
        if ($tgdp_day == 0) {
            $tgdp = $tgdp_hour . ' giờ';
        } else {
            $tgdp = $tgdp_day . ' ngày ' . $tgdp_hour . ' giờ';
        }

        $phong = PhongModel::find($ma_phong)->ten_phong;

        /* Dịch vụ được sử dụng nhiều lần nhất */
        // return: $so_luong_dich_vu, $ten_dich_vu
        $array_dich_vu = HoaDonDichVuModel::leftJoin('dich_vu','dich_vu.ma_dich_vu','hoa_don_dich_vu.ma_dich_vu')
            ->where('thoi_gian_sd_dich_vu','LIKE',$str_date_like)
            ->select(DB::raw('sum(hoa_don_dich_vu.so_luong_dich_vu) as so_luong_dich_vu'),'dich_vu.ten_dich_vu')
            ->groupBy('hoa_don_dich_vu.ma_dich_vu')
            ->get();

        $so_luong_dich_vu = 0;
        $ten_dich_vu = 'Không có dịch vụ';
        foreach ($array_dich_vu as $each) {
            if ($each->so_luong_dich_vu >= $so_luong_dich_vu) {
                $so_luong_dich_vu = $each->so_luong_dich_vu;
                $ten_dich_vu = $each->ten_dich_vu;
            }
        }

        /* Lượt hóa đơn trong tháng */
        // return: $so_hoa_don
        $array_hoa_don_thang = HoaDonModel::where('thoi_gian_lap_hoa_don','LIKE',$str_date_like)->select('ma_kh')->get();
        $so_hoa_don = $array_hoa_don_thang->count();

        /* Số khách hàng đặt lại phòng */
        // return: $so_hoa_don_dat_lai
        $array_hoa_don_thang_arr = $array_hoa_don_thang->toArray();
        $so_hoa_don_dat_lai = HoaDonModel::where('thoi_gian_lap_hoa_don','<',$str_date_full)
            ->whereIn('ma_kh',$array_hoa_don_thang_arr)
            ->get()
            ->count();

        $array_return = [
            'tgdp'               => $tgdp,
            'phong'              => $phong,
            'so_luong_dich_vu'   => $so_luong_dich_vu,
            'ten_dich_vu'        => $ten_dich_vu,
            'so_hoa_don'         => $so_hoa_don,
            'so_hoa_don_dat_lai' => $so_hoa_don_dat_lai
        ];

        return $array_return;
    }
}
