<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\NumberMinZero;
use App\Rules\NumberMaxTwo;

class DichVuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ten_dich_vu'        => 'required',
            'gia_dich_vu'        => 'bail|required|digits_between:4,9',
            'don_vi_dich_vu'     => 'required',
            'trang_thai_dich_vu' => [
                'required',
                new NumberMinZero($this->trang_thai_dich_vu),
                new NumberMaxTwo($this->trang_thai_dich_vu)
            ]
        ];
    }

    public function messages()
    {
        return [
            'required'                    => ':attribute không được để trống',
            'digits_between'              => ':attribute phải nằm trong khoảng 1.000 → 999.999.999',

            'trang_thai_dich_vu.required' => ':attribute không hợp lệ!'
        ];
    }
    public function attributes()
    {
        return [
            'ten_dich_vu'        => 'Tên dịch vụ',
            'gia_dich_vu'        => 'Giá dịch vụ',
            'don_vi_dich_vu'     => 'Đơn vị dịch vụ',
            'trang_thai_dich_vu' => 'Trạng thái dịch vụ',
        ];
    }
}
