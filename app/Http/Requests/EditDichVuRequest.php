<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditDichVuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ten_dich_vu'        => 'required',
            'gia_dich_vu'        => 'bail|required|digits_between:4,9',
            'don_vi_dich_vu'     => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required'       => ':attribute không được để trống',
            'digits_between' => ':attribute phải nằm trong khoảng 1.000 → 999.999.999',
        ];
    }
    public function attributes()
    {
        return [
            'ten_dich_vu'        => 'Tên dịch vụ',
            'gia_dich_vu'        => 'Giá dịch vụ',
            'don_vi_dich_vu'     => 'Đơn vị dịch vụ',
        ];
    }
}
