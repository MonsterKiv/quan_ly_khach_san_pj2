<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\DatPhongMinZero;

class OrderPhongRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ma_hoa_don'     => 'bail|required|integer',
            'so_luong_khach' => [
                'bail',
                'required',
                'integer',
                new DatPhongMinZero($this->so_luong_khach),
            ],
            'thoi_gian_den'  => 'bail|required|date',
            'thoi_gian_di'   => 'bail|required|date|after:thoi_gian_den',
            'ma_phong'       => 'bail|required|integer',
        ];
    }
    public function messages()
    {
        return [
            'required'           => ':attribute không được để trống',
            'date'               => ':attribute không hợp lệ',
            'integer'            => ':attribute không hợp lệ',

            'thoi_gian_di.after' => ':attribute phải sau thời gian đến',
        ];
    }
    public function attributes()
    {
        return [
            'ma_hoa_don'     => 'Hóa đơn',
            'so_luong_khach' => 'Số lượng khách',
            'thoi_gian_den'  => 'Thời gian đến',
            'thoi_gian_di'   => 'Thời gian đi',
            'ma_phong'       => 'Phòng',
        ];
    }
}
