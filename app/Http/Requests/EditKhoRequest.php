<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\EditKhoMinZero;
use App\Rules\EditKhoHongMinZero;
use App\Rules\CompareEditKho;

class EditKhoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tong_so_luong_vat_tu' => [
                'required',
                new EditKhoMinZero($this->tong_so_luong_vat_tu),
            ],
            'so_luong_vat_tu_hong' => [
                'required',
                new CompareEditKho($this->tong_so_luong_vat_tu, $this->so_luong_vat_tu_hong),
                new EditKhoHongMinZero($this->so_luong_vat_tu_hong),
            ],
        ];
    }
    public function messages()
    {
        return [
            'required' => ':attribute không được để trống',
        ];
    }
    public function attributes()
    {
        return [
            'tong_so_luong_vat_tu' => 'Tổng số lượng vật tư',
            'so_luong_vat_tu_hong' => 'Số lượng vật tư hỏng',
        ];
    }
}
