<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\DatDichVuMinZero;

class OrderDichVuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ma_hoa_don'   => 'bail|required|integer',
            'ma_dich_vu' => 'bail|required|array',
            'so_luong_dich_vu' => 'required','array',
            'so_luong_dich_vu.*' => [
                new DatDichVuMinZero($this->so_luong_dich_vu),
            ],
        ];
    }
    public function messages()
    {
        return [
            'required' => ':attribute không được để trống',
            'integer'  => ':attribute không hợp lệ!',
            'array'    => ':attribute không hợp lệ!',
        ];
    }
    public function attributes()
    {
        return [
            'ma_hoa_don'       => 'Hóa đơn',
            'ma_dich_vu'       => 'Dịch vụ',
            'so_luong_dich_vu' => 'Số lượng dịch vụ',
        ];
    }
}
