<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VatTuPhongRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'so_luong_vat_tu' => 'bail|required|numeric',
        ];
    }
    public function messages()
    {
        return [
            'required' => ':attribute không được để trống',
            'numeric'  => ':attribute phải là số',
        ];
    }
    public function attributes()
    {
        return [
            'so_luong_vat_tu' => 'Số lượng vật tư thực tế',
        ];
    }
}
