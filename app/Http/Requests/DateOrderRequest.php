<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class DateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    protected function failedValidation(Validator $validator): void
    {
        $jsonResponse = response()->json(['errors' => $validator->errors()], 422);

        throw new HttpResponseException($jsonResponse);
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'thoi_gian_den' => 'required|date|after:now',
            'thoi_gian_di' => 'required|date|after:thoi_gian_den',
        ];
    }
    public function messages()
    {
        return [
            'required' => ':attribute phải điền',
            'date' => ':attribute phải là thời gian hợp lệ',
            'thoi_gian_den.after' => ':attribute phải sau thời gian hiện tại',
            'thoi_gian_di.after' => ':attribute phải sau thời gian đến',
        ];
    }
    public function attributes()
    {
        return [
            'thoi_gian_den' => 'Thời gian đến',
            'thoi_gian_di' => 'Thời gian đi',
        ];
    }
}
