<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VatTuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ten_vat_tu' => 'required',
            'gia_vat_tu' => 'bail|required|digits_between:4,9'
        ];
    }
    public function messages()
    {
        return [
            'required'       => ':attribute không được để trống',
            'digits_between' => ':attribute phải nằm trong khoảng 1.000 → 999.999.999'
        ];
    }
    public function attributes()
    {
        return [
            'ten_vat_tu' => 'Tên vật tư',
            'gia_vat_tu' => 'Giá vật tư'
        ];
    }
}
