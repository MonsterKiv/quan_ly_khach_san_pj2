<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoaiPhongRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ten_loai_phong' => 'required',
            'gia_phong'      => 'bail|required|between:4,9'
        ];
    }
    public function messages()
    {
        return [
            'required' => ':attribute không được để trống',
            'between'  => ':attribute phải nằm trong khoảng từ 1.000 → 99.999.999'
        ];
    }
    public function attributes()
    {
        return [
            'ten_loai_phong' => 'Tên loại phòng',
            'gia_phong'      => 'Giá phòng'
        ];
    }
}
