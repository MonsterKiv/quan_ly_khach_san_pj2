<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KhachHangRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ten_kh'       => 'required',
            'ngay_sinh_kh' => 'bail|required|date',
            'gioi_tinh_kh' => 'bail|required|digits:1',
            'quoc_tich_kh' => 'required',
            'cmt_hc_kh'    => 'required',
            'email_kh'     => 'bail|required|email',
            'sdt_kh'       => 'required',
        ];
    }
    public function messages()
    {
        return[
            'required' => ':attribute không được để trống',
            'date'     => ':attribute không hợp lệ',
            'digits'   => ':attribute không hợp lệ',
            'email'    => ':attribute không hợp lệ',
        ];
    }
    public function attributes()
    {
        return [
            'ten_kh'       => 'Tên khách hàng',
            'ngay_sinh_kh' => 'Ngày sinh',
            'gioi_tinh_kh' => 'Giới tính',
            'quoc_tich_kh' => 'Quốc tịch',
            'cmt_hc_kh'    => 'Chứng minh thư / hộ chiếu',
            'email'        => 'Email',
            'sdt_kh'       => 'Số điện thoại',
        ];
    }
}
