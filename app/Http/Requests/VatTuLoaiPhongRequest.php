<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VatTuLoaiPhongRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ma_vat_tu'       => 'required',
            'so_luong_vat_tu' => 'bail|required|digits_between:1,3'
        ];
    }
    public function messages()
    {
        return [
            'required'       => ':attribute không được để trống',
            'digits_between' => ':attribute phải nằm trong khoảng từ 1 → 999'
        ];
    }
    public function attributes()
    {
        return [
            'ma_vat_tu'       => 'Vật tư',
            'so_luong_vat_tu' => 'Số lượng vật tư'
        ];
    }
}
