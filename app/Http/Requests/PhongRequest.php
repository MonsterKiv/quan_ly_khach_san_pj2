<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\PhongMinZero;

class PhongRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ma_loai_phong'       => 'required',
            'ten_phong'           => 'required',
            'tang'                => [
                'bail',
                'required',
                new PhongMinZero($this->tang),
            ],
            'ma_view'             => 'bail|required|digits:1',
            'ma_trang_thai_phong' => 'bail|required|digits:1'
        ];
    }
    public function messages()
    {
        return [
            'required'                  => ':attribute không được để trống',
            'numeric'                   => ':attribute phải là một số',

            'ma_view.required'          => ':attribute không hợp lệ!',
            'ma_view.digits'            => ':attribute không hợp lệ!',
            
            'trang_thai_phong.required' => ':attribute không hợp lệ!',
            'trang_thai_phong.digits'   => ':attribute không hợp lệ!'
        ];
    }
    public function attributes()
    {
        return [
            'ma_loai_phong'       => 'Loại phòng',
            'ten_phong'           => 'Tên phòng',
            'tang'                => 'Tầng',
            'ma_view'             => 'View',
            'ma_trang_thai_phong' => 'Trạng thái phòng'
        ];
    }
}
