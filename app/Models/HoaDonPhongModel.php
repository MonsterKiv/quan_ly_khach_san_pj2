<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class HoaDonPhongModel extends Model
{
    protected $table      = 'hoa_don_phong';
    protected $primaryKey = ['ma_hoa_don','ma_phong','thoi_gian_den','thoi_gian_di'];
    protected $fillable   = ['ma_hoa_don','ma_phong','gia_dat_phong','so_luong_khach','thoi_gian_den','thoi_gian_di'];

	public $timestamps		= false;
	public $incrementing	= false;

	public function hoaDon()
    {
        return $this->belongsTo('App\Models\HoaDonModel','ma_hoa_don','ma_hoa_don');
    }
	public function getThoiGianDen()
    {
        $date_time     = $this->thoi_gian_den;
        $new_date_time = date('d/m/Y - H:i:s', strtotime($date_time));
        return $new_date_time;
    }

    public function getThoiGianDi()
    {
        $date_time     = $this->thoi_gian_di;
        $new_date_time = date('d/m/Y - H:i:s', strtotime($date_time));
        return $new_date_time;
    }

    public function getThoiGianThuePhong()
    {
        $date_time = $this->tong_thoi_gian_thue_phong;
        $num_date = floor($date_time/86400);
        $num_hour = floor(($date_time - $num_date * 86400) / 3600);
        if ($num_date == 0) {
        	$new_time_display = $num_hour . ' giờ';
        } else {
        	$new_time_display = $num_date . ' ngày ' . $num_hour . ' giờ';
        }
        
        return $new_time_display;
    }


    protected function setKeysForSaveQuery(Builder $query)
	{
	    $keys = $this->getKeyName();
	    if(!is_array($keys)){
	        return parent::setKeysForSaveQuery($query);
	    }

	    foreach($keys as $keyName){
	        $query->where($keyName, '=', $this->getKeyForSaveQuery($keyName));
	    }

	    return $query;
	}

	/**
	 * Get the primary key value for a save query.
	 *
	 * @param mixed $keyName
	 * @return mixed
	 */
	protected function getKeyForSaveQuery($keyName = null)
	{
	    if(is_null($keyName)){
	        $keyName = $this->getKeyName();
	    }

	    if (isset($this->original[$keyName])) {
	        return $this->original[$keyName];
	    }

	    return $this->getAttribute($keyName);
	}
	public function scopeGetArrayPhongOrdered($query,$thoi_gian_den,$thoi_gian_di)
	{
		$query->whereBetween('thoi_gian_den', [$thoi_gian_den,$thoi_gian_di])
		->orWhereBetween('thoi_gian_di', [$thoi_gian_den,$thoi_gian_di]);
	}
}