<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class VatTuLoaiPhongModel extends Model
{
	protected $table		= 'vat_tu_loai_phong';
	protected $primaryKey	= ['ma_vat_tu','ma_loai_phong'];
	protected $fillable		= ['ma_vat_tu','so_luong_vat_tu','ma_loai_phong'];
	
	public $timestamps		= false;
	public $incrementing	= false;



	protected function setKeysForSaveQuery(Builder $query)
	{
	    $keys = $this->getKeyName();
	    if(!is_array($keys)){
	        return parent::setKeysForSaveQuery($query);
	    }

	    foreach($keys as $keyName){
	        $query->where($keyName, '=', $this->getKeyForSaveQuery($keyName));
	    }

	    return $query;
	}

	/**
	 * Get the primary key value for a save query.
	 *
	 * @param mixed $keyName
	 * @return mixed
	 */
	protected function getKeyForSaveQuery($keyName = null)
	{
	    if(is_null($keyName)){
	        $keyName = $this->getKeyName();
	    }

	    if (isset($this->original[$keyName])) {
	        return $this->original[$keyName];
	    }

	    return $this->getAttribute($keyName);
	}
}