<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class VatTuPhongModel extends Model
{
    protected $table      = 'vat_tu_phong';
    protected $primaryKey = ['ma_vat_tu','ma_phong'];
    protected $fillable   = ['ma_vat_tu','so_luong_vat_tu','trang_thai_vat_tu','ma_phong'];

	public $timestamps		= false;
	public $incrementing	= false;

	public function getTrangThaiVatTu()
	{
		switch ($this->trang_thai_vat_tu) {
			case '0':
				return "Thiếu";
				break;

			case '1':
				return "Hỏng";
				break;
			
			case '2':
				return "Mất";
				break;
			
			default:
				return "Sai";
				break;
		}
	}

	public function getTrangThaiVatTuForm($i)
	{
		switch ($i) {
			case '0':
				return "Thiếu";
				break;

			case '1':
				return "Hỏng";
				break;
			
			case '2':
				return "Mất";
				break;
			
			default:
				return "Sai";
				break;
		}
	}



    protected function setKeysForSaveQuery(Builder $query)
	{
	    $keys = $this->getKeyName();
	    if(!is_array($keys)){
	        return parent::setKeysForSaveQuery($query);
	    }

	    foreach($keys as $keyName){
	        $query->where($keyName, '=', $this->getKeyForSaveQuery($keyName));
	    }

	    return $query;
	}

	/**
	 * Get the primary key value for a save query.
	 *
	 * @param mixed $keyName
	 * @return mixed
	 */
	protected function getKeyForSaveQuery($keyName = null)
	{
	    if(is_null($keyName)){
	        $keyName = $this->getKeyName();
	    }

	    if (isset($this->original[$keyName])) {
	        return $this->original[$keyName];
	    }

	    return $this->getAttribute($keyName);
	}
}