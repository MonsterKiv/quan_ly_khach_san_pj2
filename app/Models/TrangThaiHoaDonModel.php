<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrangThaiHoaDonModel extends Model
{
	protected $table		= 'trang_thai_hoa_don';
	protected $primaryKey	= 'ma_trang_thai_hoa_don';
	protected $fillable		= ['ma_trang_thai_hoa_don','ten_trang_thai_hoa_don'];

    public $timestamps = false;
}