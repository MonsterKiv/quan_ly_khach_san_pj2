<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class HoaDonDichVuModel extends Model
{
	protected $table		= 'hoa_don_dich_vu';
	protected $primaryKey	= ['ma_hoa_don_dich_vu','ma_sd_dich_vu'];
	protected $fillable		= ['ma_hoa_don_dich_vu','ma_sd_dich_vu'];

	public $timestamps		= false;
	public $incrementing	= false;



	public function hoaDon()
    {
        return $this->belongsTo('App\Models\HoaDonModel','ma_hoa_don','ma_hoa_don');
    }
    protected function setKeysForSaveQuery(Builder $query)
	{
	    $keys = $this->getKeyName();
	    if(!is_array($keys)){
	        return parent::setKeysForSaveQuery($query);
	    }

	    foreach($keys as $keyName){
	        $query->where($keyName, '=', $this->getKeyForSaveQuery($keyName));
	    }

	    return $query;
	}

	/**
	 * Get the primary key value for a save query.
	 *
	 * @param mixed $keyName
	 * @return mixed
	 */
	protected function getKeyForSaveQuery($keyName = null)
	{
	    if(is_null($keyName)){
	        $keyName = $this->getKeyName();
	    }

	    if (isset($this->original[$keyName])) {
	        return $this->original[$keyName];
	    }

	    return $this->getAttribute($keyName);
	}

	public function getThoiGianSuDungDichVu()
    {
        $date_time     = $this->thoi_gian_sd_dich_vu;
        $new_date_time = date('d/m/Y - H:i:s', strtotime($date_time));
        return $new_date_time;
    }
}