<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoaiPhongModel extends Model
{
    protected $table      = 'loai_phong';
    protected $primaryKey = 'ma_loai_phong';
    protected $fillable   = ['ma_loai_phong','ten_loai_phong','gia_phong'];
    
    public $timestamps    = false;
}