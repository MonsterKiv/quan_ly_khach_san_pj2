<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TestModel extends Model
{
	protected $table		= 'phong';
	protected $primaryKey	= 'ma_phong';

    public $timestamps = false;
}