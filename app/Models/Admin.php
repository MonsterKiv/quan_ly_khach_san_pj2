<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 
 */
class Admin extends Model
{
	protected $table		= 'admin';
	protected $primaryKey	= 'ma_admin';
	protected $guard		= [];

	public $timestamps = false;
}