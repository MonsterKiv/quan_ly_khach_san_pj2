<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhongModel extends Model
{
    protected $table      = 'phong';
    protected $primaryKey = 'ma_phong';
    protected $fillable   = ['ma_phong','ma_loai_phong','ten_phong','tang','ma_view','ma_trang_thai_phong'];

    public $timestamps = false;

    public function view()
    {
        return $this->belongsTo('App\Models\ViewModel','ma_view','ma_view');
    }

    public function trang_thai_phong()
    {
        return $this->belongsTo('App\Models\TrangThaiPhongModel','ma_trang_thai_phong','ma_trang_thai_phong');
    }

    public function scopeKhongView($query)
    {
        return $query->whereHas('view',function($query){
            $query->where('ten_view','Không view');
        });
    }

    public function scopeViewNui($query)
    {
        return $query->whereHas('view',function($query){
            $query->where('ten_view','View núi');
        });
    }

    public function scopeViewBien($query)
    {
        return $query->whereHas('view',function($query){
            $query->where('ten_view','View biển');
        });
    }

    public function scopeNgungHoatDong($query)
    {
        return $query->whereHas('trang_thai_phong',function($query){
            $query->where('ma_trang_thai_phong','Ngưng hoạt động');
        });
    }

    public function scopeSanSang($query)
    {
        return $query->whereHas('trang_thai_phong',function($query){
            $query->where('ma_trang_thai_phong','Sẵn sàng');
        });
    }

    public function getTrangThaiPhongForm($i)
    {
        switch ($i) {
            case '1':
    			return "Tạm ngưng";
    			break;

    		case '2':
    			return "Sẵn sàng";
    			break;
    		
    		default:
    			return "Sai";
    			break;
        }
    }
}