<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KhoModel extends Model
{
    protected $table      = 'kho';
    protected $primaryKey = 'ma_vat_tu';
    protected $fillable   = ['ma_vat_tu','tong_so_luong_vat_tu','so_luong_vat_tu_hong'];
    
    public $timestamps    = false;
    public $incrementing  = false;
}