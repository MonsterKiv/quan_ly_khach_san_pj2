<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DichVuModel extends Model
{
    protected $table      = 'dich_vu';
    protected $primaryKey = 'ma_dich_vu';
    protected $fillable   = ['ma_dich_vu','ten_dich_vu','gia_dich_vu','don_vi_dich_vu','ma_trang_thai_dich_vu'];

    public $timestamps = false;

    public function trang_thai_dich_vu()
    {
        return $this->belongsTo('App\Models\TrangThaiDichVuModel','ma_trang_thai_dich_vu','ma_trang_thai_dich_vu');
    }

    public function scopeSanSang($query)
    {
        return $query->whereHas('trang_thai_dich_vu',function($query){
            $query->where('ten_trang_thai_dich_vu','Sẵn sàng');
        });
    }

    public function scopeTamNgung($query)
    {
        return $query->whereHas('trang_thai_dich_vu',function($query){
            $query->where('ten_trang_thai_dich_vu','Tạm ngưng');
        });
    }

    public function scopeHetDichVu($query)
    {
        return $query->whereHas('trang_thai_dich_vu',function($query){
            $query->where('ten_trang_thai_dich_vu','Hết dịch vụ');
        });
    }

    public function getTrangThaiDichVuForm($i)
    {
        switch ($i) {
            case '1':
                return "Sẵn sàng";
                break;

            case '2':
                return "Tạm ngưng";
                break;
            
            case '3':
                return "Hết dịch vụ";
                break;

            default:
                return "Lỗi";
                break;
        }
    }
}