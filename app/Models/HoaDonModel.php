<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HoaDonModel extends Model
{
    protected $table      = 'hoa_don';
    protected $primaryKey = 'ma_hoa_don';
    protected $fillable   = ['ma_hoa_don','ma_kh','thoi_gian_lap_hoa_don','ma_trang_thai_hoa_don','ghi_chu'];

    public $timestamps = false;
    
    public function array_hoa_don_dich_vu()
    {
        return $this->hasMany('App\Models\HoaDonDichVuModel','ma_hoa_don','ma_hoa_don');
    }
    public function array_hoa_don_phong()
    {
        return $this->hasMany('App\Models\HoaDonPhongModel','ma_hoa_don','ma_hoa_don');
    }
    public function trang_thai_hoa_don()
    {
        return $this->belongsTo('App\Models\TrangThaiHoaDonModel','ma_trang_thai_hoa_don','ma_trang_thai_hoa_don');
    }

    public function scopeChuaThanhToan($query)
    {
        return $query->whereHas('trang_thai_hoa_don',function($query){
            $query->where('ten_trang_thai_hoa_don','Chưa thanh toán');
        });
    }

    public function scopeDaThanhToan($query)
    {
        return $query->whereHas('trang_thai_hoa_don',function($query){
            $query->where('ten_trang_thai_hoa_don','Đã thanh toán');
        });
    }

    public function scopeDaHuy($query)
    {
        return $query->whereHas('trang_thai_hoa_don',function($query){
            $query->where('ten_trang_thai_hoa_don','Đã hủy');
        });
    }

    public function getThoiGianLapHoaDon()
    {
        $date_time     = $this->thoi_gian_lap_hoa_don;
        $new_date_time = date('d/m/Y - H:i', strtotime($date_time));
        return $new_date_time;
    }
}