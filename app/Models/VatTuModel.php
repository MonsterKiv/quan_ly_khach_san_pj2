<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VatTuModel extends Model
{
	protected $table		= 'vat_tu';
	protected $primaryKey	= 'ma_vat_tu';
	protected $fillable		= ['ma_vat_tu','ten_vat_tu','gia_vat_tu'];

    public $timestamps = false;
}