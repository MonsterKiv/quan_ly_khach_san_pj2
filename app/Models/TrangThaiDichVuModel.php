<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrangThaiDichVuModel extends Model
{
	protected $table		= 'trang_thai_dich_vu';
	protected $primaryKey	= 'ma_trang_thai_dich_vu';
	protected $fillable		= ['ma_trang_thai_dich_vu','ten_trang_thai_dich_vu'];

    public $timestamps = false;
}