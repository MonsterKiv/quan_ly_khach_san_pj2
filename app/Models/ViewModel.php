<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ViewModel extends Model
{
	protected $table		= 'view';
	protected $primaryKey	= 'ma_view';
	protected $fillable		= ['ma_view','ten_view'];

    public $timestamps = false;
}