<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KhachHangModel extends Model
{
    protected $table      = 'khach_hang';
    protected $primaryKey = 'ma_kh';
    protected $fillable   = ['ma_kh','ten_kh','ngay_sinh_kh','gioi_tinh_kh','quoc_tich_kh','cmt_hc_kh','email_kh','sdt_kh'];

    public $timestamps = false;

    public function getGioiTinh()
    {
    	if ($this->gioi_tinh_kh == 0) {
    		return "Nam";
    	} else {
    		return "Nữ";
    	}
    }

    public function ngaySinhFormat()
    {
    	$date = $this->ngay_sinh_kh;
    	$new_date = date('d/m/Y', strtotime($date));
    	return $new_date;
    }
}