<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrangThaiPhongModel extends Model
{
	protected $table		= 'trang_thai_phong';
	protected $primaryKey	= 'ma_trang_thai_phong';
	protected $fillable		= ['ma_trang_thai_phong','ten_trang_thai_phong'];

    public $timestamps = false;
}