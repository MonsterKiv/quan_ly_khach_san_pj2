$(document).ready(function() {
	/* Toggle open/close sub-title of left menu */
	$('.collapsible').click(function(event) {
		$(this).next().toggleClass('show');
		$(this).toggleClass('menu_active');
	});

	/* Display "Go to top" and "Bottom admin modal opener" */
	$(window).scroll(function(event) {
		var buttonGoToTop = $('.go_to_top');
		var buttonOpenProfileBottom = $('.toggle_admin_modal');
		if ($('body,html').scrollTop() > 70 || $(document).scrollTop() > 70) {
			buttonGoToTop.css('display', 'block');
			buttonOpenProfileBottom.css('display', 'block');
		} else {
			buttonGoToTop.css('display', 'none');
			buttonOpenProfileBottom.css('display', 'none');
		}
	});

	/* Execute "Go to top" */
	$('.go_to_top').click(function(event) {
		$('body,html').scrollTop(0);
		$(document).scrollTop(0);
	});

	/* Toggle Admin Modal */
	$('.header_username').click(openAdminModal);
	$('.toggle_admin_modal').click(openAdminModal);
	$('.overlay').click(closeAdminModal);

	/* Mobile - Toggle Left Menu */
	$('.header_menu_toggle').click(openLeftMenu);
	$('.overlay_menu').click(closeLeftMenu);

	/* Display (Hide) Left Menu when resize to Mobile (Desktop) client */
	$(window).resize(function(event) {
		if ($('.header_menu_toggle').css('display') == 'none') {
			$('.menu').css('left', '0');
		} else {
			$('.menu').css('left', '-250px');
		}
	});

	/* Open Form Modal */
	$('.open_popup_form').click(openFormModal);

	/* Close Form Modal */
	$('.overlay_form').click(closeFormModal);
	$('.form_popup_close_button').click(closeFormModal);

	/* Are you sure display (when delete only) */
    $('.button_trigger').click(function(event) {
    	$(this).hide().siblings().show();
    });
    $('.button_return').click(function(event) {
    	event.preventDefault();
    	$(this).parent().parent().hide().siblings().show();
    });
});

function openAdminModal() {
	$('.overlay').show();
	$('.modal_admin').css('right', '0');
	$('body,html').css('overflow', 'hidden');
	var OffsetY = $(window).scrollTop();
	$('.contentBig').css({
		top: -OffsetY,
		paddingRight: '17px'
	});
}

function closeAdminModal() {
	$('.overlay').hide();
	$('.modal_admin').css('right', '-300px');
	$('body,html').css('overflow', '');
	$('.contentBig').css({
		top: '',
		paddingRight: ''
	});
}

function openLeftMenu() {
	$('.overlay_menu').show();
	$('.menu').css('left', '0');
	var OffsetY = $(window).scrollTop();
	$('body,html').css('overflow', 'hidden');
	$('.contentBig').css('top', -OffsetY);
}

function closeLeftMenu() {
	$('.overlay_menu').hide();
	$('.menu').css('left', '-250px');
	$('body,html').css('overflow', '');
	$('.contentBig').css({
		position: '',
		top: ''
	});
}

function openFormModal() {
	$('.overlay_form').show();
	$('body,html').css('overflow', 'hidden');
	var OffsetY = $(window).scrollTop();
	$('.contentBig').css({
		top: -OffsetY,
		paddingRight: '17px'
	});
}

function closeFormModal() {
	$('.overlay_form').hide();
	$('.form_popup').hide();
	$('body,html').css('overflow', '');
	$('.contentBig').css({
		top: '',
		paddingRight: ''
	});
}

/* Non-reload validate form and return validation errors */
function ajaxForm(url){
	$("#button_submit").click(function(event){
		event.preventDefault();
		var form = $("#formId");
		form.find('span.validate_msg').empty();
		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'json',
			data: form.serialize(),
			success: function(response) {
				if (response == "Sửa thành công") {
					$("#noti").append(`
						<div class="success">
							<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
							${response}
						</div>
					`);
				} else {
					$("#noti").append(`
						<div class="alert">
							<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
							${response}
						</div>
					`);
				}
			},
		    error: function (err) {
		    	$("#noti").html(``);
		        if (err.status == 422) {
		            var array_error = err.responseJSON.errors;
					$.each(array_error, function(index,val) {
						$(`[name='${index}']`).after(`
						<span class="validate_msg">
							${val.join(' | ')}
						</span>`);
					});
		        }
		    }
		});
	});
}

/* Datepicker Vietnam Region */
$.datepicker.regional["vi-VN"] =
	{
		closeText: "Đóng",
		prevText: "Trước",
		nextText: "Sau",
		currentText: "Hôm nay",
		monthNames: [
			"Tháng một",
			"Tháng hai",
			"Tháng ba",
			"Tháng tư",
			"Tháng năm",
			"Tháng sáu",
			"Tháng bảy",
			"Tháng tám",
			"Tháng chín",
			"Tháng mười",
			"Tháng mười một",
			"Tháng mười hai"
			],
		monthNamesShort: [
			"Một",
			"Hai",
			"Ba",
			"Bốn",
			"Năm",
			"Sáu",
			"Bảy",
			"Tám",
			"Chín",
			"Mười",
			"Mười một",
			"Mười hai"
			],
		dayNames: [
			"Chủ nhật",
			"Thứ hai",
			"Thứ ba",
			"Thứ tư",
			"Thứ năm",
			"Thứ sáu",
			"Thứ bảy"
			],
		dayNamesShort: [
			"CN",
			"Hai",
			"Ba",
			"Tư",
			"Năm",
			"Sáu",
			"Bảy"
			],
		dayNamesMin: [
			"CN",
			"T2",
			"T3",
			"T4",
			"T5",
			"T6",
			"T7"
			],
		weekHeader: "Tuần",
		dateFormat: "dd/mm/yy",
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ""
	};
$.datepicker.setDefaults($.datepicker.regional["vi-VN"]);